<?php

  // Get environment settings
  require_once( __DIR__ . '/local-config.inc.php' );

  // Get PDO db file because mysqi sucks
  require_once(realpath(__DIR__ . '/assets/php/_pdo.inc.php'));
  $settings = getWebsiteSettings();
  $enabled_languages = json_decode($settings[8]['value']);

  // Get global Settings
  require_once($GLOBALS['dir'].'assets/php/globals.php');

  // get stripe plans for modal
  $stripe_plans = getStripePlans();

  /**
   * Set Cookie to toggle homepage CTA.
   * Expires in 1 year.
  **/
  if( isset( $_SESSION['secure'] ) && !empty( $_SESSION['secure'] ) ) {
    if( !$_COOKIE['returningVisit'] ) {
      setcookie( 'returningVisit', true, '2147483647', '/' );
    }
  }

?>
<!DOCTYPE html>
<html class=""
  ng-app="App"
  lang="<?php echo $_SESSION['lang']; ?>-CA"
  >
  <head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible"
      content="IE=edge"
      />
    <meta name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
      />
    <meta name="description"
      content="<?php echo $settings[6]['value']; ?>"
      />
    <meta name="author"
      content="<?php echo $settings[0]['value']; ?>"
      />
    <meta name="msapplication-TileImage"
      content="build/img/touch/apple-touch-icon-144x144-precomposed.png"
      />
    <meta name="msapplication-TileColor"
      content="#222222"
      />

    <?php
      if ($_REQUEST['videoId']) {
        $videoId = filter_var(trim($_REQUEST['videoId']), FILTER_VALIDATE_INT) ?
          trim($_REQUEST['videoId']) :
          filter_var(trim($_REQUEST['videoId']), FILTER_SANITIZE_NUMBER_INT);

        $db = dbconnect();

        // Prepare statements
        $stmt = $db->prepare('
          SELECT *
            FROM videos
           WHERE id = :videoId
           LIMIT 1
        ');

        $stmt->execute(array(':videoId' => $videoId,));
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(isset($results[0]) && !empty($results[0])) {
          $video = $results[0];
          ?>
          <meta
            property="og:image"
            content="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . '/uploads/images/' . $video['poster'] ?>"
            />

          <meta
            property="og:type"
            content="video.movie"
            />

          <meta
            property="og:url"
            content="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>"
            />

          <meta
            property="og:title"
            content="<?php echo $video['name'] ?>"
            />

          <meta
            property="og:description"
            content="<?php echo $video['description'] ?>"
            />
        <?php
        }
      }
    ?>

    <title>
      <?php echo $settings[0]['value']; ?>
    </title>

    <link rel="shortcut icon"
      href="<?php echo $GLOBALS['dir']; ?>build/img/icons/favicon.ico"
      />

    <!-- ICONS -->
    <link rel="apple-touch-icon-precomposed"
      sizes="180x180"
      href="build/img/touch/apple-touch-icon-180x180-precomposed.png"
      />
    <link rel="apple-touch-icon-precomposed"
      sizes="152x152"
      href="build/img/touch/apple-touch-icon-152x152-precomposed.png"
      />
    <link rel="apple-touch-icon-precomposed"
      sizes="144x144"
      href="build/img/touch/apple-touch-icon-144x144-precomposed.png"
      />
    <link rel="apple-touch-icon-precomposed"
      sizes="120x120"
      href="build/img/touch/apple-touch-icon-120x120-precomposed.png"
      />
    <link rel="apple-touch-icon-precomposed"
      sizes="114x114"
      href="build/img/touch/apple-touch-icon-114x114-precomposed.png"
      />
    <link rel="apple-touch-icon-precomposed"
      sizes="76x76"
      href="build/img/touch/apple-touch-icon-76x76-precomposed.png"
      />
    <link rel="apple-touch-icon-precomposed"
      sizes="72x72"
      href="build/img/touch/apple-touch-icon-72x72-precomposed.png"
      />
    <link rel="apple-touch-icon-precomposed"
      href="build/img/touch/apple-touch-icon-57x57-precomposed.png"
      />
    <link rel="shortcut icon"
      href="build/img/touch/apple-touch-icon.png"
      />
    <link rel="icon"
      sizes="192x192"
      href="build/img/touch/touch-icon-192x192.png"
      />
    <link rel="icon"
      sizes="128x128"
      href="build/img/touch/touch-icon-128x128.png"
      />

    <style>
      [ng-cloak] {
        display: none !important;
      }
    </style>
    <!-- END ICONS -->

    <base href="/" />

    <!-- polyfill for multi-browser support --> 
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>

    <!-- modernizr -->
    <script src="<?php echo $GLOBALS['dir']; ?>build/js/modernizr.min.js"></script>

    <!-- css -->
    <link rel="stylesheet"
      href="//fonts.googleapis.com/css?family=Open+Sans:400,700"
      type="text/css"
      />
    <?php if(getenv('ENVIRONMENT') === 'production' || getenv('ENVIRONMENT') === 'staging'): ?>
    <link rel="stylesheet"
      href="<?php echo $GLOBALS['dir']; ?>build/css/app.min.css"
      type="text/css"
      />
    <?php else: ?>
    <link rel="stylesheet"
      href="<?php echo $GLOBALS['dir']; ?>build/css/app.css"
      type="text/css"
      />
    <?php endif; ?>
    <!--[if lt IE 10]>
    <link rel="stylesheet" href="<?php echo $GLOBALS['dir']; ?>build/css/ie.css" type="text/css" />
    <![endif]-->
    <!--[if gte IE 9]>
    <style type="text/css">
      .gradient, .btn, .video-js .vjs-control-bar { filter: none; }
    </style>
    <![endif]-->

    <script
      type="text/javascript"
      id="st_insights_js"
      src="https://ws.sharethis.com/button/buttons.js?publisher=1cb66cd6-c2de-4ac1-87d1-d7cc632dc9f0"
      >
    </script>

    <!-- Load Playr and HLS package if in IE browser --> 
    <?php if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false): ?>       
      <script src="<?php echo $GLOBALS['dir']; ?>assets/lib/hls.js-master/dist/hls.js"></script>       
      <link rel="stylesheet" href="<?php echo $GLOBALS['dir']; ?>assets/lib/plyr-master/dist/plyr.css">       
      <script src="<?php echo $GLOBALS['dir']; ?>assets/lib/plyr-master/dist/plyr.js"></script>   
      <!-- Ad support for Plyr --> 
      <script type="text/javascript" src="https://imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
      <script src="<?php echo $GLOBALS['dir']; ?>assets/lib/plyr-ads/dist/plyr-ads.min.js"></script>      
      <link src="<?php echo $GLOBALS['dir']; ?>assets/lib/plyr-ads/dist/plyr-ads.min.css"></link>     
    <?php endif; ?>

  </head>
<body
  ng-cloak>