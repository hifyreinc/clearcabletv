<?php session_start(); ?>

<?php $GLOBALS['dir'] = '../'; ?>
<?php include($GLOBALS['dir'].'assets/php/config.php'); ?>

<?php

	$query = "SELECT id FROM categories WHERE EXISTS(
      	  SELECT id
      	  FROM questions
      	  WHERE category = categories.id
      	) ORDER BY position ASC LIMIT 1";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);
	header('Location: /faq/category/?categoryId='.$row['id']);

?>