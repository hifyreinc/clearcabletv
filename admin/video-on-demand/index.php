<?php session_start(); ?>
<?php $GLOBALS['restricted'] = true; ?>
<?php $GLOBALS['restricted-admin'] = true; ?>

<?php $GLOBALS['dir'] = '../../'; ?>
<?php $GLOBALS['pagetype'] = 'admin'; ?>
<?php $GLOBALS['pagesubtype'] = 'video-on-demand'; ?>
<?php $GLOBALS['pagename'] = 'admin-video-on-demand-all'; ?>

<?php include($GLOBALS['dir'].'_header.php'); ?>

<?php include($GLOBALS['dir'].'views/_includes/_header.php'); ?>
<?php include($GLOBALS['dir'].'views/_includes/_nav.php'); ?>

	<?php include($GLOBALS['dir'].'views/'.$GLOBALS['pagetype'].'/'.$GLOBALS['pagename'].'.php'); ?>

<?php include($GLOBALS['dir'].'views/_includes/_footer.php'); ?>

<?php include($GLOBALS['dir'].'_footer.php'); ?>