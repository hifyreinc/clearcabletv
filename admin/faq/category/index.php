<?php session_start(); ?>
<?php $GLOBALS['restricted'] = true; ?>
<?php $GLOBALS['restricted-admin'] = true; ?>
<?php $GLOBALS['required'] = array("categoryId"); ?>

<?php $GLOBALS['dir'] = '../../../'; ?>
<?php $GLOBALS['pagetype'] = 'admin'; ?>
<?php $GLOBALS['pagesubtype'] = 'faq'; ?>
<?php $GLOBALS['pagename'] = 'admin-faq-category'; ?>

<?php include($GLOBALS['dir'].'_header.php'); ?>

<?php include($GLOBALS['dir'].'views/_includes/_header.php'); ?>
<?php include($GLOBALS['dir'].'views/_includes/_nav.php'); ?>

	<?php include($GLOBALS['dir'].'views/'.$GLOBALS['pagetype'].'/'.$GLOBALS['pagename'].'.php'); ?>

<?php include($GLOBALS['dir'].'views/_includes/_footer.php'); ?>

<?php include($GLOBALS['dir'].'_footer.php'); ?>