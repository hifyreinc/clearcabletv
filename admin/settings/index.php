<?php

session_start();

$GLOBALS['restricted'] = true; 
$GLOBALS['restricted-admin'] = true;
$GLOBALS['pagetype'] = 'admin';
$GLOBALS['pagesubtype'] = 'settings';
$GLOBALS['pagename'] = 'admin-settings';
$GLOBALS['dir'] = '../../';

require_once(realpath(__DIR__ . '/../../_header.php'));
require_once(realpath(__DIR__ . '/../../views/_includes/_header.php'));
require_once(realpath(__DIR__ . '/../../views/_includes/_nav.php'));
require_once(realpath(__DIR__ . '/../../views/'.$GLOBALS['pagetype'].'/'.$GLOBALS['pagename'].'.php'));
require_once(realpath(__DIR__ . '/../../views/_includes/_footer.php'));
require_once(realpath(__DIR__ . '/../../_footer.php'));

?>