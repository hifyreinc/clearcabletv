<?php

session_start();
$GLOBALS['restricted'] = true;
$GLOBALS['restricted-admin'] = true;
$GLOBALS['dir'] = '../../../';
$GLOBALS['pagetype'] = 'admin';
$GLOBALS['pagesubtype'] = 'go-services';
$GLOBALS['pagename'] = 'admin-go-services-add-new';
include($GLOBALS['dir'].'_header.php');
include($GLOBALS['dir'].'views/_includes/_header.php');
include($GLOBALS['dir'].'views/_includes/_nav.php');
include($GLOBALS['dir'].'views/'.$GLOBALS['pagetype'].'/'.$GLOBALS['pagename'].'.php');
include($GLOBALS['dir'].'views/_includes/_footer.php');
include($GLOBALS['dir'].'_footer.php');

?>