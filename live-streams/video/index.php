<?php

session_start();

$GLOBALS['dir'] = '../../';
$GLOBALS['pagetype'] = 'live-streams';
$GLOBALS['pagename'] = 'live-streams-video';
$GLOBALS['required'] = array("videoId");

include($GLOBALS['dir'].'_header.php');
include($GLOBALS['dir'].'views/_includes/_header.php');
include($GLOBALS['dir'].'views/_includes/_nav.php');
include($GLOBALS['dir'].'views/'.$GLOBALS['pagetype'].'/'.$GLOBALS['pagename'].'.php');

include($GLOBALS['dir'].'views/_includes/_footer.php');
include($GLOBALS['dir'].'_footer.php');

?>
