// Set Defaults?
var scrollhorzswiper;
var swipersidenav;

/**
 * @name loadScrollers
 *
 * @description
 * Loads swiper objects to do ... something.
**/
var loadScrollers = function () {
  // Get all instances of swiper elements
  var filmstrips = $('.swiper-scroll-horz:not([ng-cloak])');
  filmstrips.each(function (index) {
    // console.log($(this).attr('rel'));
    // console.log(index);
    // Add loaded class
    $(this).addClass('horz-scroll-loaded')
    // Add identified class
    $(this).addClass('swiper-' + index);
    // Add identifier
    $(this).attr('data-swiper_id', 'swiper-' + index);
    $(this).attr('id', 'swiper-' + index);
    // Assign swiper object
    window['swiper-'+index] = new Swiper($(this), {
      direction: 'horizontal',
      freeMode: true,
      slidesPerView: 'auto',
      onSetTransition: function(swiper){
        window.clearInterval(scrollhorzswiper);
        swiper.onResize();
        // scrollArrows(swiper,'swiper-'+index);
      },
      onInit: function(swiper){
        scrollArrows(swiper,'swiper-'+index);
      }
    });
  });

  // Set a swiper instance for the nav
  var nav = $('.swiper-side-nav:not([ng-cloak])');
  // Add loaded class
  $(nav).addClass('nav-scroll-loaded');
  // Assign instance
  swipersidenav = new Swiper($('.swiper-side-nav'), {
    direction: 'vertical',
    freeMode: true,
    slidesPerView: 'auto',
    mousewheelControl: true
  });
  swipersidenavcover = new Swiper($('.swiper-side-nav-cover'), {
    direction: 'vertical',
    freeMode: true,
    slidesPerView: 'auto'
  });
};

/**
 * @name reloadScrollers
 *
 * @description
 * Reloads Swiper objects?
**/
var reloadScrollers = function () {
  var elems = $('.swiper-scroll-horz');
  if (elems.length) {
    elems.each(function () {

      var parentscrollerid = $(this).attr('data-swiper_id');
      var swiper = window[parentscrollerid];

      if (swiper) {
        swiper.slideTo(0, 0);
        swiper.updateSlidesSize();
        swiper.onResize();
        scrollArrows(swiper, parentscrollerid);
      }
      
    });
  }
};

/**
 * @name scrollArrows
 *
 * @description
 * Toggle visibility of swiper controls.
 * Abandoning checking the plugin's isBeginning and isEnd methods
 * because in some instances both are returning true and cancelling everything out.
 *
 * Do a manual check on the wrapper's translate property instead.
**/
var scrollArrows = function(swiper, index) {
  swiper.updateProgress();

  // Hide both arrows by default, we no longer want them to show
  $('#' + index).addClass('hide-scroll-left');
  $('#' + index).addClass('hide-scroll-right');
};

/**
 * @name getURLParameter
 *
 * @description
 * Gets some kind of url parameter
**/
function getURLParameter(sParam){

  // Check query string
  var sPageURL = window.location.search.substring(1);

  // If not empty
  if (sPageURL) {
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == sParam) {
        return sParameterName[1];
      }
    }
  } else {
    return null;
  }
}

/**
 * @name setURLParameter
 *
 * @description
 * Sets some kind of url parameter
**/
function setURLParameter(key, value) {
  var key = escape(key); value = escape(value);
  var kvp = document.location.search.substr(1).split('&');

  if (kvp == '') {
    document.location.search = '?' + key + '=' + value;
  } else {
    var i = kvp.length; var x;
    while (i--) {
      x = kvp[i].split('=');
      if (x[0] == key) {
        x[1] = value;
        kvp[i] = x.join('=');
        break;
      }
    }
    if (i < 0) {
      kvp[kvp.length] = [key, value].join('=');
    }
    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&');
  }
}

/**
 * @name throttle
 *
 * @description
 * ??
**/
function throttle(fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  var last;
  var deferTimer;

  return function () {
    var context = scope || this;
    var now = +new Date,
      args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}

/**
 * @name _ajax
 *
 * @description
 * Ajax call to update some setting in the DB.
 *
 * @param func: php switch-case for ajax file to execute
 * @param data: all the data we need to send to the switch-case
 * @param callback: optional callback function to execute for live updating.
**/
function _ajax(func, info, callback) {
  // Call ajax file.
  var aj = $.post(
    "assets/php/_ajax.php",
    {
      case: func,
      info: JSON.stringify(info)
    }
  );

  // After Submit
  aj.done(function(data) {
    var parsedData = $.parseJSON(data);
    // Optional Callback
    if(callback) {
      setTimeout(function(){
        callback(parsedData);
      }, 50);
    }
  });
}

/**
 * Event Listeners
 * --------------------------------------------------
**/
$(document).ready(function() {

  // Load Swipers
  loadScrollers();

  // ??
  $('body').on('contextmenu', 'video,object', function(e) {
    e.preventDefault();
  });

  // Horizontal scroll arrows?
  $('body').on('mouseenter', '.hover-div', function(){

    var parentscroller = $(this).closest('.swiper-scroll-horz');
    var parentscrollerid = parentscroller.attr('data-swiper_id');
    var parentwrapper = parentscroller.children().children('.swiper-slide');

    if(window[parentscrollerid]){

      var swiper = window[parentscrollerid];
      var scrolldir;
      var scrolldist;
      var maxscrolldist;

      if($(this).hasClass('scroll-left')){

        scrolldir = 'left';
        scrolldist = 5;
        maxscrolldist = 0;

      } else {

        scrolldir = 'right';
        scrolldist = -5;
        maxscrolldist = parentwrapper.width() - parentscroller.width();

      }

      scrollhorzswiper = setInterval(function(){

        var currentx = swiper.translate;
        var scrolleroffset = 5 * Math.round(currentx/5);
        if((scrolldir == 'left') && (currentx*-1) > maxscrolldist){
          swiper.setWrapperTranslate(scrolleroffset + scrolldist);
        } else if((scrolldir == 'right') && (currentx*-1) < maxscrolldist){
          swiper.setWrapperTranslate(scrolleroffset + scrolldist);
        } else {
          window.clearInterval(scrollhorzswiper);
        }
        swiper.updateProgress();
        scrollArrows(swiper,parentscrollerid);

      }, 18);

    }
  });
  $('body').on('mouseleave', '.hover-div', function(){
    window.clearInterval(scrollhorzswiper);
  });

  // Label Wizardry
  $('body').on('focus', '.form-control', function(){
    var parentContainer = $(this).closest('.form-group');
    parentContainer.addClass('focused');
  });
  $('body').on('blur', '.form-control', function(){
    var parentContainer = $(this).closest('.form-group');
    parentContainer.removeClass('focused');
    if ($(this).val()) {
      parentContainer.addClass('populated');
    } else {
      parentContainer.removeClass('populated');
    }
  });
  $('body').on('click', '.clear-input', function(e){
    e.preventDefault();
    $(this).siblings('.form-control').val('').blur();
  });

  // Nav
  $('body').on('click', '.bars-menu-button,.side-nav-cover', function (e) {
    e.preventDefault();
  });
  $('body').on('mousedown', '.bars-menu-button,.side-nav-cover', function () {
    if ($('html').hasClass('open-nav')) {
      $('html').removeClass('open-nav');
      $('input,textarea,select').blur();
      $('html').addClass('nav-closing');
      setTimeout(function(){
        $('html').removeClass('nav-closing');
      }, 300);
    } else {
      $('html').addClass('open-nav');
      $('input,textarea,select').blur();
      $('html').addClass('nav-opening');
      setTimeout(function(){
        $('html').removeClass('nav-opening');
      }, 300);
    }
    return false;
  });
  $('body').on('click', 'nav .dropdown-toggle', function(){
    //console.log('click');
    $(this).closest('.dropdown').toggleClass('open');
    if(swipersidenav){
      var swiper = swipersidenav;
      swiper.updateContainerSize();
      swiper.updateSlidesSize();
      swiper.updateProgress();
    }
    return false;
  });
  $('body').on('click', '[data-swapmodal]', function(e){
    e.preventDefault();
    var href = $(this).attr('href');
    $('.modal').removeClass('fade').modal('hide');
    $(href).modal('show');
    $('.modal').addClass('fade');
  });

  // Like Videos
  $('body').on('click', '.likeable', function(e) {
    e.preventDefault();
    _ajax(
      'likeVideo',
      [
        $('#video-wrapper').attr('data-video'),
        $(this).attr('data-action')
      ],
      function(data) {
        // Toggle the visibility of the like
        if (data[0] === true) {
          $('#like').addClass('liked');
          $('#dislike').removeClass('disliked');
        } else {
          $('#like').removeClass('liked');
          $('#dislike').addClass('disliked');
        }

        // Set new totals.
        $('#likeCount').html(data[1][0]);
        $('#dislikeCount').html(data[1][1]);
      }
    );
  });

  // Track Videos
  $('body').on('click', '.vjs-big-play-button', function() {
    _ajax(
      'trackVideo',
      [
        $('#video-wrapper').attr('data-video'),
        $('#video-wrapper').attr('data-user')
      ],
      function(data) {
        var vid = $('#the_video');
        if (!vid.hasClass('vjs-waiting')) {
          console.log('add loading class');
          $(vid).addClass('vjs-waiting');
        }
        $('#viewCount').html(data);
        console.log('hide button');
        $('.vjs-big-play-button').hide();
      }
    );
  });

  // Update Admin Settings
  $('body').on('click', '#adminSettings', function(e) {
    e.preventDefault();

    var keys = [
      $('#websiteLabel').attr('rel'),
      $('#websiteURL').attr('rel'),
      $('.gateway-radio:checked').attr('rel'),
      $('#homepageMsg').attr('rel'),
      $('#twitterMsg').attr('rel'),
      $('#facebookMsg').attr('rel'),
      $('#languages').attr('rel')
    ];
    var values = [
      $('#websiteLabel').val(),
      $('#websiteURL').val(),
      $('.gateway-radio:checked').val(),
      $('#homepageMsg').val(),
      $('#twitterMsg').val(),
      $('#facebookMsg').val(),
      JSON.stringify($('#languages').val())
    ];

    // If set to stripe
    if($('.gateway-radio:checked').val() == 2) {
      // public key
      keys.push($('#stripeAPI').attr('rel'));
      values.push($('#stripeAPI').val());

      // private key
      keys.push($('#privateAPI').attr('rel'));
      values.push($('#privateAPI').val());
    }

    // Ajax Call
    _ajax(
      'updateWebsiteSettings',
      [
        keys,
        values
      ],
      function() {
        location.reload();
      }
    );
  });

  // Payment Gateway
  $('body').on('click', '.gateway-radio', function() {

    // store the value
    var payment = $(this).val();
    var wrapper = '#' + $(this).attr('data-key');

    // only toggle if it's stripe selected
    if(payment == 2) {
      $(wrapper).fadeIn(250);
    }
    if(payment != 2) {
      $(wrapper).fadeOut(250);
    }

  });

});
