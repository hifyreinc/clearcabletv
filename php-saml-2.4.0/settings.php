<?php

$settings = array (
    // If 'strict' is True, then the PHP Toolkit will reject unsigned
    // or unencrypted messages if it expects them signed or encrypted
    // Also will reject the messages if not strictly follow the SAML
    // standard: Destination, NameId, Conditions ... are validated too.
    'strict' => false,

    // Enable debug mode (to print errors)
    'debug' => true,

    // Service Provider Data that we are deploying
    'sp' => array (
        // Identifier of the SP entity  (must be a URI)
        'entityId' => 'urn:idp:clearcable:net:sp:cp:flow:prod',
        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array (
            // URL Location where the <Response> from the IdP will be returned
            'url' => 'https://ccapgo.com/saml2/acs.php',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        'singleLogoutService' => array (
            // URL Location where the <Response> from the IdP will be returned
            'url' => 'https://ccapgo.com/saml2/sls.php',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',

        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters
        'x509cert' => '',
        'privateKey' > '',
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array (
        // Identifier of the IdP entity  (must be a URI)
        'entityId' => 'urn:idp:clearcable:net:idp:federation',
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array (
            // URL Target of the IdP where the SP will send the Authentication Request Message
            'url' => 'https://idp.clearcable.net/saml/saml2/idp/SSOService.php',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-POST binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array (
            // URL Location of the IdP where the SP will send the SLO Request
            'url' => 'https://idp.clearcable.net/saml/saml2/idp/SingleLogoutService.php',
            // SAML protocol binding to be used when returning the <Response>
            // message.  Onelogin Toolkit supports for this endpoint the
            // HTTP-Redirect binding only
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        // Public x509 certificate of the IdP
        'x509cert' => 'MIIFTTCCBDWgAwIBAgIJALwClTCzrDRCMA0GCSqGSIb3DQEBCwUAMIG0MQswCQYDVQQGEwJVUzEQMA4GA1UECBMHQXJpem9uYTETMBEGA1UEBxMKU2NvdHRzZGFsZTEaMBgGA1UEChMRR29EYWRkeS5jb20sIEluYy4xLTArBgNVBAsTJGh0dHA6Ly9jZXJ0cy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5LzEzMDEGA1UEAxMqR28gRGFkZHkgU2VjdXJlIENlcnRpZmljYXRlIEF1dGhvcml0eSAtIEcyMB4XDTE1MDcwNzE5MDgzOFoXDTE4MDcwNzE5MDgzOFowQjEhMB8GA1UECxMYRG9tYWluIENvbnRyb2wgVmFsaWRhdGVkMR0wGwYDVQQDDBQqLmlkcC5jbGVhcmNhYmxlLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALhbAQtxB8HGhepYaLOGQQqbUB9b48QEnRjKOsIZe0ZnDuQu9cmXlLmD8zeMUujrYEQTqJ+YtLr2WZW13Yd8+Bie/CVQ6KY7+4JFbcdtJ9E65cX5MndwW6js74RwoYMWt0nmw1+3nlqCsVDYfQESNyieYrfE5cLAQeLiXmI7bVnlOCGcCS24/F09AC17bLMiA3Pu5tnbJRgLXbfWjYqtimiZzrch3CbdUZHQMgZUjEbzBKyk3L9eLkz4X2YKfMEEW2XAC//ZUh3QaT/pGEYsn11AzIVvvZZiMTdfnuRf+BB+7aORIqfam0IGPrPBKRJbgk6HDtx8KcHOOxkoTMFgnRUCAwEAAaOCAdEwggHNMAwGA1UdEwEB/wQCMAAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMA4GA1UdDwEB/wQEAwIFoDA2BgNVHR8ELzAtMCugKaAnhiVodHRwOi8vY3JsLmdvZGFkZHkuY29tL2dkaWcyczEtODkuY3JsMFMGA1UdIARMMEowSAYLYIZIAYb9bQEHFwEwOTA3BggrBgEFBQcCARYraHR0cDovL2NlcnRpZmljYXRlcy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5LzB2BggrBgEFBQcBAQRqMGgwJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmdvZGFkZHkuY29tLzBABggrBgEFBQcwAoY0aHR0cDovL2NlcnRpZmljYXRlcy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5L2dkaWcyLmNydDAfBgNVHSMEGDAWgBRAwr0njsw0gzCiM9f7bLPwtCyAzjBJBgNVHREEQjBAghQqLmlkcC5jbGVhcmNhYmxlLm5ldIISaWRwLmNsZWFyY2FibGUubmV0ghQqLmlkcC5jbGVhcmNhYmxlLm5ldDAdBgNVHQ4EFgQUyzrHj2F6CDngi9SZ9f/nOgr9PUUwDQYJKoZIhvcNAQELBQADggEBABOLCcsu9bT1P8b807ytWh08znG9zjMwjxy/9Oy9thyn5ExUrJ/i6vtttGt5/3TYDgnnuLt4DRaiK2JTzJGNCZ0EKx3jv7yiHQn3b9liFIZz/LFg2FpIlk0JScMwP3/XEJD/VmX6ffjzUZydR3urznz2dSVVckMXcSRUpQlmrhOX83nqmgHjKDxwUO5jpLcIYnPK7+JzEOuS0YqA2xcqwdQRZPIDkEWLKSCthQIWzKAD+VpoIn+WlvXz9ttSQBPjvR1VEW8i3gVpD+uah1/cdOhm/Cx1Hk7Wg2jp8e2N2B4O2Tuj2FkVRitFHdKt5m8M2yi+q4EIv0X9eX3MqpzwZjc=',
        /*
         *  Instead of use the whole x509cert you can use a fingerprint
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it)
         */
        // 'certFingerprint' => '',
    ),
);
