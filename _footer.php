      <?php include($GLOBALS['dir'].'views/_includes/_modal-login.php'); ?>
      <?php include($GLOBALS['dir'].'views/_includes/_modal-signup.php'); ?>

      <div class="javascript-translations" hidden>
        <p class="confirm-delete-video">
          <?php echo $tr->__('Are you sure you want to delete this video?'); ?>
        </p>
        <p class="confirm-delete-section">
          <?php echo $tr->__('Are you sure you want to delete this section?'); ?>
        </p>
        <p class="alert-video-update-success">
          <?php echo $tr->__('Video updated successfully!'); ?>
        </p>
        <p class="item-text">
          <?php echo $tr->__('item'); ?>
        </p>
        <p class="items-text">
          <?php echo $tr->__('items'); ?>
        </p>
        <p class="text-thumbnail">
          <?php echo $tr->__('Thumbnail'); ?>
        </p>
      </div>

      <?php if($settings[2]['value'] == '2'): ?>
      <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
      <?php endif; ?>
      <script type="text/javascript">
        // Set session language for stripe?
        function getCurrentSessionLang(){
          var sessionlang = '<?php echo $_SESSION['lang'] ?>';
          return sessionlang;
        }
        <?php if($settings[2]['value'] == '2'): ?>
        Stripe.setPublishableKey( '<?php echo $settings[3]['value']; ?>' );
        <?php endif; ?>
      </script>

      <!-- Twitter api -->
      <script>
        window.twttr = (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
          if (d.getElementById(id)) return t;
          js = d.createElement(s);
          js.id = id;
          js.src = "https://platform.twitter.com/widgets.js";
          fjs.parentNode.insertBefore(js, fjs);

          t._e = [];
          t.ready = function(f) {
            t._e.push(f);
          };

          return t;
        }(document, "script", "twitter-wjs"));
      </script>

      <!-- Facebook api -->
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            appId      : '1701515266787551',
            xfbml      : true,
            version    : 'v2.6'
          });
        };

        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "//connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script>

      <?php if(getenv('ENVIRONMENT') === 'staging'): ?>
        <script type='text/javascript'>
        (function (d, t) {
         var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
         bh.type = 'text/javascript';
         bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=lhcezhxuzxo74ul1ujzrra';
         s.parentNode.insertBefore(bh, s);
         })(document, 'script');
        </script>
      <?php endif; ?>

      <?php if(getenv('ENVIRONMENT') === 'production'): ?>
      <!-- Analytics -->
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-63595687-4', 'auto');
        ga('send', 'pageview');

      </script>

      <?php endif; ?>

      
      <!-- TVE Bundle -->
      <script src="<?php echo $GLOBALS['dir']; ?>build/js/angular.js"></script>
      <script src="<?php echo $GLOBALS['dir']; ?>build/js/vendor.js"></script>
      <script src="<?php echo $GLOBALS['dir']; ?>build/js/app.js"></script>


      <!-- VideoJS HLS with GOAL_BUFFER_LENGTH set to 60 ** --> 
      <script src="hls/node_modules/videojs-contrib-hls/dist/videojs-contrib-hls.js"/>



  </body>
</html>
