<?php session_start(); ?>

<?php $GLOBALS['dir'] = '../../'; ?>
<?php $GLOBALS['pagetype'] = 'video-on-demand'; ?>
<?php $GLOBALS['pagename'] = 'video-on-demand-category'; ?>
<?php $GLOBALS['required'] = array("categoryId"); ?>

<?php include($GLOBALS['dir'].'_header.php'); ?>

<?php include($GLOBALS['dir'].'views/_includes/_header.php'); ?>
<?php include($GLOBALS['dir'].'views/_includes/_nav.php'); ?>

	<?php include($GLOBALS['dir'].'views/'.$GLOBALS['pagetype'].'/'.$GLOBALS['pagename'].'.php'); ?>

<?php include($GLOBALS['dir'].'views/_includes/_footer.php'); ?>

<?php include($GLOBALS['dir'].'_footer.php'); ?>