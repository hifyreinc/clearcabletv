<?php

require('../php/config.php');

session_start();

define("TOOLKIT_PATH", '/var/www/flowportal/php-saml-2.4.0/');
require(TOOLKIT_PATH . '_toolkit_loader.php');

$auth = new OneLogin_Saml2_Auth();

$auth->processResponse();

$errors = $auth->getErrors();

if (!empty($errors)) {
    print_r('<p>'.implode(', ', $errors).'</p>');
    exit();
}

if (!$auth->isAuthenticated()) {
    echo "<p>Not authenticated</p>";
    exit();
}

$_SESSION['samlUserdata'] = $auth->getAttributes();
$_SESSION['samlNameId'] = $auth->getNameId();
$_SESSION['samlSessionIndex'] = $auth->getSessionIndex();

$_SESSION['username'] = $_SESSION['samlUserdata']['username'][0];
$_SESSION['userid'] = $_SESSION['samlUserdata']['Id'][0];
$_SESSION['secure'] = md5($salt.$_SESSION['userid']);

$query = 'SELECT * FROM users WHERE username = "'.$_SESSION['username'].'"';
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_assoc($result);
$numRows = mysqli_num_rows($result);

if ($numRows == 1) {
  $_SESSION['userid'] = $row['id'];
  $_SESSION['favorites'] = $row['favorites'];
  $_SESSION['email'] = $row['email'];
  $_SESSION['role'] = $row['role'];
  $_SESSION['settings'] = unserialize($row['settings']);
  $_SESSION['secure'] = md5($salt.$_SESSION['userid']);
}

if (isset($_POST['RelayState']) && OneLogin_Saml2_Utils::getSelfURL() != $_POST['RelayState']) {
    $auth->redirectTo($_POST['RelayState']);
}

if (!empty($_SESSION)) {
  print_r($_SESSION);
} else {
    echo _('Attributes not found');
}

?>
