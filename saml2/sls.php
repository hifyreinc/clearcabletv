<?php

session_start();

define("TOOLKIT_PATH", '/var/www/flowportal/php-saml-2.4.0/');
require(TOOLKIT_PATH . '_toolkit_loader.php');

$auth = new OneLogin_Saml2_Auth();

$auth->processSLO();            // Process the Logout Request & Logout Response
$errors = $auth->getErrors(); // Retrieves possible validation errors
  session_destroy();
  header('Location: /index.php');
  print_r('<p>'.implode(', ', $errors).'</p>');
