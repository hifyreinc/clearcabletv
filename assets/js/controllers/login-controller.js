// SIGNUP CONTROLLER
var errs = [];

App.controller('LoginController', function ($rootScope, $scope, Service) {

  $scope.loginAccount = {
    success: null,
    error: null,
    redirect: null
  };

  $('.modal').on('show.bs.modal', function(e){
    var videoid = $(e.relatedTarget).data('govideo');
    if(videoid){
      var currentlocation = window.location.href.split("?")[0].split('#')[0];
      currentlocation = currentlocation.replace('category/','');
      currentlocation = currentlocation.replace('video/','');
      currentlocation = currentlocation.replace(/\/$/, "") + '/video/?videoId=' + videoid;
      $scope.loginAccount.redirect = currentlocation;
      $('[data-swapmodal]').attr('govideo',videoid);
    }
  });

  $scope.accountLogin = function ($event){
    $scope.loginAccount.success = null;
    $scope.loginAccount.error = null;
    var btn = $($event.target);
    btn.prop('disabled', true);
    Service.accountLogin($scope.loginAccount, function (response) {
      if(response.success){
        $scope.loginAccount.success = response.response;
        btn.prop('disabled', false);
        $('.modal.in').modal('hide');
        window.location.reload();
      } else {
        $scope.loginAccount.error = response.response;
        btn.prop('disabled', false);
      }
      $scope.$digest();
    });
  };

});