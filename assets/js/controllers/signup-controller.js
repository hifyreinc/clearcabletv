// SIGNUP CONTROLLER
var errs = [];

App.controller('SignupController', function ($rootScope, $scope, Service) {

  $scope.createAccount = {
    step: 1,
    plan: {
      id: 'daily_subscription'
    },
    success: null,
    error: null,
    redirect: null
  };

  $('.modal').on('show.bs.modal', function(e){
    var videoid = $(e.relatedTarget).data('govideo');
    if(videoid){
      var currentlocation = window.location.href.split("?")[0].split('#')[0];
      currentlocation = currentlocation.replace('category/','');
      currentlocation = currentlocation.replace('video/','');
      currentlocation = currentlocation.replace(/\/$/, "") + '/video/?videoId=' + videoid;
      $scope.createAccount.redirect = currentlocation;
      $('[data-swapmodal]').attr('govideo',videoid);
    }
  });

  $scope.accountStep = function (num,showError){
    $scope.createAccount.step = num;
    //console.log($scope);
    if(!showError){
      $scope.createAccount.success = null;
      $scope.createAccount.error = null;
    }
  };

  $scope.accountCreate = function ($event){
    $scope.createAccount.success = null;
    $scope.createAccount.error = null;
    var btn = $($event.target);
    btn.prop('disabled', true);
    Service.accountCreate($scope.createAccount, function (response) {
      if(response.success){
        $scope.createAccount.success = response.response;
        btn.prop('disabled', false);
        $('.modal.in').modal('hide');
        window.location.reload();
      } else {
        if(response.response == 'invalid-stripe-token'){
          var $form = $('.signup-fields');
          Stripe.card.createToken($form, function(status, response){

            if (response.error) {
              $scope.createAccount.error = response.error.message;
              btn.prop('disabled', false);
              $scope.$digest();
            } else {
              $scope.createAccount.stripeToken = response.id;
              Service.accountCreate($scope.createAccount, function (response) {
                if(response.success){
                  $scope.createAccount.success = response.response;
                  btn.prop('disabled', false);
                  $('.modal.in').modal('hide');
                  window.location.reload();
                } else {
                  $scope.createAccount.error = response.response;
                  $scope.accountStep(response.error,true);
                  btn.prop('disabled', false);
                }
              });
            }

          });
        } else {
          $scope.createAccount.error = response.response;
          $scope.accountStep(response.error,true);
          btn.prop('disabled', false);
        }
      }
      $scope.$digest();
    });
  };

});