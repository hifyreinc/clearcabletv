<?php

/**
 * Accept $_POST paramaters and execute.
 *
 * @param $case: Switch case to execute
 * @param $data: object array of data we need to run our function
**/

// Make sure someone actually submitted a request through the proper channels
if(isset($_POST) && isset($_POST['case'])) {

  // Get PDO db file because mysqi sucks
  require_once(realpath(__DIR__ . '/_pdo.inc.php'));

  // Run a switch so we can fun a variety of different calls
  switch($_POST['case']) {

    /**
     * @name updateWebsiteSettings()
     *
     * @description
     * Accept arrays of setting keys and values. Combine them into a singgle array
     * to be looped over by an update SQL statement.
     *
     * @param $keys: array of WHERE keys to use in the update command.
     * @param $values: array of values we need to update
    **/
    case 'updateWebsiteSettings':

      // It was easier to pass keys and values as 2 separate arrays.
      $info = json_decode($_POST['info']);
      $data = array_combine($info[0], $info[1]);

      // Upsate Settings
      $update = updateWebsiteSettings($data);
      echo json_encode($update);
      break;

    /**
     * @name trackVideo()
     *
     * @description
     * Track how many times a Stream or Video-on-Demand has been played.
     *
     * @param $video: ID of video to update.
     * @param $user: Optional ID of user watching.
    **/
    case 'trackVideo':

      // Set up our data
      $info = json_decode($_POST['info']);
      $video = filter_var($info[0], FILTER_VALIDATE_INT);
      $user  = filter_var($info[1], FILTER_VALIDATE_INT);
      $now = date("Y-m-d H:i:s");

      // Update our View Count if we have good data
      if($video && $now) {
        trackVideo($user, $video, $now);

        // Get new count total
        $newTotal = getVideoViewCount($video);
        echo json_encode($newTotal);
      }
      break;

    /**
     * @name likeVideo()
     *
     * @description
     * Set a like/dislike for a video if the user hasn't currently taken that action.
     *
     * @param $video: ID of video to update.
     * @param $user: User ID to compare like against
     * @param $action: like or dislike.
    **/
    case 'likeVideo':

      // Set up our data
      $info = json_decode($_POST['info']);
      $video = filter_var($info[0], FILTER_VALIDATE_INT);
      $like  = filter_var($info[1], FILTER_VALIDATE_BOOLEAN);

      // Update our View Count if we have good data
      if($video) {
        $newTotals = setVideoLike($video, $like);

        // We need to package the action taken into the callback function
        $return = array($like, $newTotals);
        echo json_encode($return);
      }

      break;

  }

}

?>