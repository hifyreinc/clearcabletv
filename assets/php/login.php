<?php

session_start();

include('config.php');
require_once('class.translation.php');
$tr = new Translator($_SESSION['lang']);

unset($_SESSION['loginerror']);

$username = mysqli_real_escape_string($conn, $_POST['username']);
$password = mysqli_real_escape_string($conn, $_POST['password']);
$videoid = mysqli_real_escape_string($conn, $_POST['videoid']);
$pageurl = mysqli_real_escape_string($conn, $_POST['pageurl']);
$provider = 'rogers';

$saltpass = md5($salt.$password);
$query = 'SELECT * FROM users WHERE username = "'.$username.'" AND password = "'.$saltpass.'"';
$result = mysqli_query($conn, $query);
$row = mysqli_fetch_assoc($result);
$numRows = mysqli_num_rows($result);

if ($numRows == 1) {
  // Put stuff in session
  $_SESSION['userid'] = $row['id'];
  $_SESSION['username'] = $row['username'];
  $_SESSION['favorites'] = $row['favorites'];
  $_SESSION['email'] = $row['email'];
  $_SESSION['role'] = $row['role'];
  $_SESSION['settings'] = unserialize($row['settings']);
  $_SESSION['secure'] = md5($salt.$row['id']);
  $_SESSION['provider'] = $provider;
  // Go to main page

  if($videoid){
    $pageurl = strtok($pageurl, '?');
    $pageurl = str_replace("category/", "", $pageurl);
    $pageurl = str_replace("video/", "", $pageurl);
    if (strpos($pageurl,'#') !== false) {
      $pageurl = substr($pageurl, 0, strpos($pageurl, "#"));
    }
    header('Location: '.rtrim($pageurl, '/').'/video/?videoId='.$videoid);
  } else {
	  header('Location: '.$pageurl);
  }

} else {

  $_SESSION['loginerror'] = $tr->__('Invalid Email Address/Password.');
  header('Location: '.$pageurl);

}
