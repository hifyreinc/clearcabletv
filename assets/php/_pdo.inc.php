<?php

// Get environment settings
require_once( realpath( __DIR__ . '/../../local-config.inc.php' ) );

// Get Stripe API
require_once(__DIR__ . '/vendor/autoload.php');

/**
 * Set base timezone for server
**/
date_default_timezone_set('America/Toronto');

/**
 * @name dbConnect
 *
 * @description
 * Reusable function to return a PDO object.
 * Storing an open connecting is stupid, return it and close it when we need it.
**/
function dbConnect() {
  $dsn = 'mysql:dbname='.getenv('DBNAME').';host='.getenv('DBHOST').';charset=utf8;port='.getenv('DBPORT');
  $db  = new PDO($dsn, getenv('DBUSER'), getenv('DBPASS'), array (
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
  ));

  return $db;
}

/**
 * @name getVideoLike
 *
 * @description
 * Reusable function that returns the like status of a user/video combo.
**/
function getVideoLike($user, $video) {

  // Establish a database connetion.
  $db = dbconnect();

  $checkUser = $db->prepare(
    'SELECT *
     FROM `video_likes`
     WHERE `userID` = :user
      AND `videoID` = :video'
  );
  $checkUser->execute(array(
    ':user'  => $user,
    ':video' => $video
  ));

  return $checkUser->fetchAll(PDO::FETCH_ASSOC);

}

/**
 * @name getVideoLikeCount
 *
 * @description
 * Reusable function that returns the like status of a user/video combo.
**/
function getVideoLikeCount($video, $status) {

  // Establish a database connetion.
  $db = dbconnect();

  // PRepare Statements
  $stmt = $db->prepare(
    'SELECT count(*)
     FROM `video_likes`
     WHERE `videoID` = :video
      AND `liked` = :status'
  );
  $stmt->execute(array(
    ':video'   => $video,
    ':status'  => $status
  ));

  // Return Statement
  return $stmt ? $stmt->fetchColumn() : false;

}


/**
 * @name setVideoLike
 *
 * @description
 * Reusable function that returns the like status of a user/video combo.
**/
function setVideoLike($video, $liked) {

 // Establish a database connetion.
  $db = dbconnect();

  $stmt = $db->prepare(
    'INSERT INTO `video_likes` (videoID, liked)
     VALUES (:video, :liked)'
  );
  $stmt->execute(array(
    ':video'  => $video,
    ':liked'  => $liked
  ));

  // Now get our new totals.
  $totals = array();

  // Likes
  $likeCount = getVideoLikeCount($video, 1);
  if(isset($likeCount)) {
    array_push($totals, $likeCount);
  }

  // Dislikes
  $dislikeCount = getVideoLikeCount($video, 0);
  if(isset($dislikeCount)) {
    array_push($totals, $dislikeCount);
  }

  return $totals;

}

/**
 * @name trackVideo
 *
 * @description
 * Inserts a new row into video_views table.
**/

function trackVideo($video, $user, $time) {

  // Establish a database connetion.
  $db = dbconnect();

  // Prepare statements
  $stmt = $db->prepare(
    'INSERT INTO `video_views` (videoID, userID, viewTS)
     VALUES (:user, :video, :ts)'
  );
  $stmt->execute(array(
    ':user'   => $user,
    ':video'  => $video,
    ':ts'  => $time
  ));

  // Return statement
  return $stmt ? true : false;

}

/**
 * @name getVideoViewCount
 *
 * @description
 * Inserts a new row into video_views table.
**/

function getVideoViewCount($video) {

  // Establish a database connetion.
  $db = dbconnect();

  // Prepare statements
  $stmt = $db->prepare(
    'SELECT count(*)
     FROM `video_views`
     WHERE `videoID` = :video'
  );
  $stmt->execute(array(
    ':video' => $video,
  ));

  // Return statement
  return $stmt ? $stmt->fetchColumn() : false;
}

/**
 * @name getWebsiteSetting
 *
 * @description
 * Get the website settings for use throughout the website.
**/
function getWebsiteSettings() {

  // Establish a database connetion.
  $db = dbconnect();

  // Prepare statement
  $stmt = $db->prepare(
    'SELECT *
     FROM `settings`'
  );
  $stmt->execute();

  // Return statement
  return $stmt ? $stmt->fetchAll(PDO::FETCH_ASSOC) : false;

}

/**
 * @name getWebsiteSetting
 *
 * @description
 * Get the website settings for use throughout the website.
**/
function updateWebsiteSettings($data) {

  // Establish a database connetion.
  $db = dbconnect();

  // Prepare statements
  foreach($data as $k => $v) {
    $stmt = $db->prepare(
      'UPDATE `settings`
       SET `value` = :value
       WHERE `ID` = :id'
    );
    $stmt->execute(array(
      ':id'     => $k,
      ':value'  => $v
    ));
  }

  // Return statement
  return $stmt ? true : false;

}

/**
 * @name getStripePlans
 *
 * @description
 * Fetch all Stripe subscriptions from the API.
**/

function getStripePlans($api_args = null) {
  \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));
  $plans = \Stripe\Plan::all($api_args);

  return $plans->data;
}

/**
 * @name getPrivateAPIKey
 *
 * @description
 * Get's Stripe APIkey from DB.
**/
function getPrivateAPIKey() {
  // Establish a database connetion.
  $db = dbconnect();

  // Prepare statement
  $stmt = $db->prepare(
    'SELECT `value`
     FROM `settings`
     WHERE id = 5'
  );
  $stmt->execute();

  $key = $stmt->fetchAll(PDO::FETCH_ASSOC);;

  // Close our db connection
  $db = null;

  return $key[0]['value'];
}


?>