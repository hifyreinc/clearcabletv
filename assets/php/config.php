<?php

// Get environment settings
require_once( realpath( __DIR__ . '/../../local-config.inc.php' ) );

$salt = getenv( 'SALT' );

$host     = getenv( 'DBHOST' );
$username = getenv( 'DBUSER' );
$password = getenv( 'DBPASS' );
$table    = getenv( 'DBNAME' );

$conn = mysqli_connect( $host, $username, $password, $table );

if( mysqli_connect_errno() ) {
  printf(
    'Connect Error (%d): %s',
    mysqli_connect_errno(),
    mysqli_connect_error()
  );
  exit();
}

mysqli_set_charset($conn, 'utf8');

/**
 * Set category constants
 */
$goServiceCategory = 112;
$adCategory = 113;