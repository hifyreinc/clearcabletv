<?php

header('P3P:CP="NOI COR NID CUR ADM DEV CUS PSA OUR DEL IND UNI STA"');

if(isset($GLOBALS['restricted'])) {
  if(!isset($_SESSION['secure'])) {
    header('Location: /');
  }
  if(isset($GLOBALS['admin-only'])) {
    if($_SESSION['role'] != 'admin'){
      header('Location: /');
    }
  }
}

if(isset($_SESSION['subscription_error']) && $GLOBALS['pagename'] != 'account-all'){
  header('Location: /account/');
}

include('config.php');
require_once('class.translation.php');

if(isset($_GET['lang'])){
  if(!in_array($_GET['lang'], $enabled_languages)){
    $_SESSION['lang'] = 'en';
  } else {
    $_SESSION['lang'] = $_GET['lang'];
  }
  $tr = new Translator($_SESSION['lang']);
} else {
  if(!isset($_SESSION['lang'])){
    $_SESSION['lang'] = 'en';
  }
  $tr = new Translator($_SESSION['lang']);
}

function switchlangurl($switchlang){
  $url_domain = $_SERVER['HTTP_HOST'];
  $url_path = $_SERVER['SCRIPT_NAME'];

  $qs = '';

  if(empty($_GET) || !isset($_GET['lang'])){

    $key = 'lang';
      $qs .= $key  . '=' . $switchlang . '&';

  }

  foreach ($_GET as $key => $value){
      if ($key  == 'lang'){
          $qs .= $key  . '=' . $switchlang . '&';
      } else {
          $qs .= $key  . '=' . urlencode($value) . '&';
      }
  }

  return "http://" . $url_domain . $url_path . "?" . $qs;
}

$GLOBALS['noimage'] = '//placehold.it/480x270/000000&text=Image+Not+Found';

if(isset($GLOBALS['required'])){
if(in_array('categoryId', $GLOBALS['required'])) {
  if (is_null($_GET['categoryId'])) { header('Location: /'); }
  if(isset( $GLOBALS['pagesubtype'] ) && $GLOBALS['pagesubtype'] == 'advertisements'){
    if($_GET['categoryId'] != '-1'){
      header('Location: /');
    }
  } else if(isset( $GLOBALS['pagesubtype'] ) && $GLOBALS['pagesubtype'] == 'go-services'){
    if($_GET['categoryId'] != '70') {
      header('Location: /');
    }
  } else if( isset( $GLOBALS['pagetype'] ) && $GLOBALS['pagetype'] == 'faq' || isset( $GLOBALS['pagesubtype'] ) && $GLOBALS['pagesubtype'] == 'faq'){
    $sectionId = mysqli_real_escape_string($conn, $_GET['categoryId']);
    $query = "SELECT * FROM categories WHERE id = ". $sectionId;
    $result = mysqli_query($conn, $query);
    $numRows = mysqli_num_rows($result);
    if ($numRows != 1) { header('Location: /'); }
  } else {
    $sectionId = mysqli_real_escape_string($conn, $_GET['categoryId']);
    $query = "SELECT * FROM sections WHERE id = ". $sectionId;
    $result = mysqli_query($conn, $query);
    $numRows = mysqli_num_rows($result);
    if ($numRows != 1) { header('Location: /'); }
  }
}

// if(in_array('videoId', $GLOBALS['required'])) {
//   if (is_null($_GET['videoId'])) { header('Location: /'); }
//   $videoId = mysqli_real_escape_string($conn, $_GET['videoId']);
//   $query = "SELECT * FROM videos WHERE id = " . $videoId;
//   $result = mysqli_query($conn, $query);
//   $numRows = mysqli_num_rows($result);
//   if ($numRows != 1) { header('Location: /'); }
// }
}

?>