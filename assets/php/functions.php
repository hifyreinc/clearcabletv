<?php

/**
 * Define some basic Settings
 * Include external function files.
 * Set request type.
 * Initiate translate object
**/


if (!isset($requesttype)) {
  $requesttype = $_GET['type'];
}
session_start();
require_once('config.php');
require_once('class.translation.php');
$tr = new Translator($_SESSION['lang']);
define('DEFAULT_POSTER', "//placehold.it/384x216/000000&text=Image+Not+Found");
define('PUBLISH', "(publishstart <= NOW() AND publishend > NOW() OR publishstart IS NULL OR publishend IS NULL)");
$publish = isset($_SESSION['role']) && $_SESSION['role'] == 'admin' ? '' : " AND (publishstart <= NOW() AND publishend > NOW() OR publishstart IS NULL OR publishend IS NULL)";
$settings = getWebsiteSettings();
$enabled_languages = json_decode($settings[8]['value']);
$has_en = in_array('en', $enabled_languages)
  ? true : false;
$has_fr = in_array('fr', $enabled_languages)
  ? true : false;
$has_es = in_array('es', $enabled_languages)
  ? true : false;

// Check payment gateway settings
$paymentGateway = getPaymentGateway();

// Setup the Facebook SDK
// if (getenv('FB_APP_ID') && getenv('FB_APP_SECRET')) {
//   $fb = new Facebook\Facebook([
//     'app_id' => getenv('FB_APP_ID'),
//     'app_secret' => getenv('FB_APP_SECRET'),
//     'default_graph_version' => 'v2.8'
//   ]);
// }


// Require composer files
require_once __DIR__.'/vendor/autoload.php';

/**
 * @name getSources
 *
 * @description
 * Assembles <source> parameters for HTML5 Video embed.
 * Sets parameters to the correct formats if it's a live-stream on an on-demand video.
 *
 * @param $video: video id being requested
**/
/**
 * @name getSources
 *
 * @description
 * Assembles <source> parameters for HTML5 Video embed.
 * Sets parameters to the correct formats if it's a live-stream on an on-demand video.
 *
 * @param $video: video id being requested
**/
function getSources($video) {

  /**
   * Set up video sources.
   * Source patterns varry if it is a live stream or an uploaded video.
  **/

  // For Live Streams
  if (!empty($video['url']) || !empty($video['m3u8'])) {
    $sources = array(
      'm3u8'         => '<source src="%s" type="application/x-mpegURL">',
      'smoothStream' => '<source src="%s" type="application/vnd.ms-sstr+xml">',
      'rtmp'         => '<source src="%s" type="rtmp/mp4">'
    );
  }
  // For Uploaded Videos
  else {
    $sources = array(
      'mp4'   => '<source src="/uploads/videos/%s" type="video/mp4" />',
      'webm'  => '<source src="/uploads/videos/%s" type="video/webm" />',
      'ogg'   => '<source src="/uploads/videos/%s" type="video/ogg" />'
    );

    $html = '';
    foreach($sources as $type => $source) {
      $url = sprintf($source, $video[$type]);
      $html .= $url;
    }
    return $html;
  }

  // if go service
  if (empty($video['m3u8'])){
    return ''; 
  }     

  /**
   * Live stream urls are tokened.
   *
   * We need to generate a string that follows a specific pattern, hash it
   * on the fly and then append it to the end of the stream request url.
  **/

  // Assemble Stream request URL.
  $html = '';
  foreach ($sources as $type => $source) {

     // HLS
    if ($type == 'm3u8') {
      // Compile stream url
      $stream = $video['m3u8'] . '://'
        . $video['hostname'] . ':'
        . $video['port'] . '/'
        . $video['application'] . '/smil:'
        . $video['streamname'] . '.smil/playlist.m3u8';
    }

    // Surface Manifest
    if ($type == 'smoothStream') {
      // Compile stream url
      $stream = $video['m3u8'] . '://'
        . $video['hostname'] . ':'
        . $video['port'] . '/'
        . $video['application'] . '/'
        . $video['streamname'] . '.smil/Manifest';
      }

    // RTMP
    if ($type == 'rtmp') {
      // Compile stream url
      $stream = $video['url'] . '://'
        . $video['hostname'] . ':'
        . $video['port'] . '/'
        . $video['application'] . '/'
        . $video['streamname'] . '_480p.stream';
      }

    // Tokenize for Wowza
    $wowzaToken = new \remiheens\WowzaSecureToken\WowzaSecureToken('iimae0UC2a','076719eb414e424a');
    $wowzaToken->setClientIP($_SERVER['REMOTE_ADDR']);
    $wowzaToken->setURL($stream);
    $wowzaToken->setHashMethod(\remiheens\WowzaSecureToken\WowzaSecureToken::SHA256);
    $starttime = time();
    $endtime = strtotime('+4 HOURS');
    $params = array(
      'endtime' => $endtime,
      // 'starttime' => $starttime   testing without starttime ** 
    );
    $wowzaToken->setExtraParams($params);
    $hash = $wowzaToken->getHash();
    $url = $wowzaToken->getFullURL();

    // Add to source list
    $html .= sprintf($source, $url);
  }

  return $html;
};



/**
 * @name getPoster
 *
 * @description
 * Sets the Video's poster image.
 *
 * @param $video: array of all video data.
**/
function getPoster($video) {
  $poster = isset($video['poster']) ? $video['poster'] : '';

  // If we have a poster
  if ($poster) {
    $poster = '/uploads/images/' . $poster;
    if (!file_exists('../../' . $poster)) {
      $poster = DEFAULT_POSTER;
    }
  } else {
    $poster = DEFAULT_POSTER;
  }
  return $poster;
};


/**
 * @name getVideosFromCategory
 *
 * @description
 * Reusable fetch query that retrives all videos associated to a given category ID.
**/
function getVideosFromCategory($id, $publish) {
  // Establish a database connetion.
  $db = dbconnect();

  // Get Category Info
  $cat = $db->prepare(
    'SELECT *
     FROM sections
     WHERE id = :id'
  );
  $cat->execute(array(
    ':id' => $id,
  ));
  $catResults = $cat->fetchAll(PDO::FETCH_ASSOC);
  // Check for no-results
  $catResults[0] = !empty($catResults[0]) ? $catResults[0] : '';

  // Get the videos
  $vids = $db->prepare(
    'SELECT *
     FROM videos
     WHERE
      EXISTS(
        SELECT * FROM video_category
          WHERE videoID = videos.id
            AND categoryID = :id
      )'
      . $publish
      . 'ORDER BY position ASC'
  );
  $vids->execute(array(
    ':id' => $id,
  ));
  $vidCount = $vids->rowCount();
  $vidResults = $vids->fetchAll(PDO::FETCH_ASSOC);

  // Assemble back into an array for us to break down.
  $data = array(
    'category' => $catResults[0],
    'videos' => $vidResults,
    'count' => $vidCount
  );

  // Close our db connection
  $db = null;

  // Return data
  return $data;
}


/**
 * @name getRelatedVideos
 *
 * @description
 * Get videos from category, ignoring a given ID
**/
function getRelatedVideos($video, $categories, $publish) {
  // Establish a database connetion.
  $db = dbconnect();

  // Create an array of stored procedure variables
  $limit = count($categories) - 1;
  $inParams = '';
  for($i = 0; $i <= $limit; $i++){
    $inParams .= $i != $limit ? ':cat' . $i . ', ' : ':cat' . $i;
  }

  // Create an array of replacement values
  $paramReplace = array( ':video' => $video );

  // Populate category replacements
  for($i = 0; $i <= $limit; $i++) {
    $paramReplace[':cat' . $i] = $categories[$i];
  }

  // Get Category Info
  $stmt = $db->prepare(
    'SELECT *
      FROM videos
      WHERE !( videos.id = :video )
        AND EXISTS(
          SELECT * FROM video_category
            WHERE videoID = videos.id
            AND categoryID IN (' . $inParams . '))
        ORDER BY RAND()'
  );
  $stmt->execute($paramReplace);
  $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);

  // Close our db connection
  $db = null;

  // Return data
  return $videos;
}


/**
 * @name getSections
 *
 * @description
 * Get all the different Video Categories
**/
function getSections($id) {
  // Establish a database connetion.
  $db = dbconnect();

  // Get the Sections
  $stmt = $db->prepare(
    'SELECT sections.*
      FROM sections
        LEFT OUTER JOIN section_has_type
          ON sections.id = section_has_type.section_id
        LEFT OUTER JOIN section_types
          ON section_types.id = section_has_type.section_type_id
      WHERE section_types.id = :id
      ORDER BY sections.position ASC'
  );
  $stmt->execute(array(
    ':id' => $id
  ));
  $sections = $stmt->fetchAll(PDO::FETCH_ASSOC);

  // Close our db connection
  $db = null;

  return $sections;
}


/**
 * @name getVideoCount
 *
 * @description
 * Get the video count of a given category
**/
function getVideoCount($id, $publish) {
  // Establish a database connetion.
  $db = dbconnect();

  // Get the Sections
  $stmt = $db->prepare(
    'SELECT COUNT(*) as count
     FROM videos
     WHERE
      EXISTS(
        SELECT * FROM video_category
          WHERE videoID = videos.id
            AND categoryID = :id
      )'
      . $publish
  );
  $stmt->execute(array(
    ':id' => $id
  ));
  $count = $stmt->fetchAll(PDO::FETCH_ASSOC);;

  // Close our db connection
  $db = null;

  return $count;
}


/**
 * @name getPopularVideos
 *
 * @description
 * Get the most popular videos for the givenlimit and  date range.
**/
function getPopularVideos($start, $end, $limit, $type, $publish) {
  // Establish a database connetion.
  $db = dbconnect();

  // Get the Sections
  $stmt = $db->prepare(
    'SELECT *, (
       SELECT COUNT(*)
       FROM video_views
       WHERE video_views.videoId = videos.id
     ) as view_count
     FROM videos
     WHERE type = :type
     ' . $publish . '
     ORDER BY view_count DESC
     LIMIT ' . $limit
  );
  $stmt->execute(array(
    ':type'        => $type,
  ));
  $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);;

  // Close our db connection
  $db = null;

  return $videos;

}


/**
 * @name getFeaturedVideos
 *
 * @description
 * Get the featured videos
**/
// function getFeaturedVideos($start, $end, $limit, $type, $publish) {
//   // Establish a database connetion.
//   $db = dbconnect();

//   // Get the Sections
//   $stmt = $db->prepare(
//     'SELECT *, (
//        SELECT COUNT(*)
//        FROM video_views
//        WHERE video_views.videoId = videos.id
//      ) as view_count
//      FROM videos
//      WHERE type = :type
//      ' . $publish . '
//      ORDER BY view_count DESC
//      LIMIT ' . $limit
//   );
//   $stmt->execute(array(
//     ':type'        => $type,
//   ));
//   $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);;

//   // Close our db connection
//   $db = null;

//   return $videos;

// }


/**
 * @name getPaymentGateway
 *
 * @description
 * Checks the gateway setting to determine if we need to strip or not.
**/
function getPaymentGateway() {
  // Establish a database connetion.
  $db = dbconnect();

  // Prepare statement
  $stmt = $db->prepare(
    'SELECT `value`
     FROM `settings`
     WHERE id = 3'
  );
  $stmt->execute();

  $gateway = $stmt->fetchAll(PDO::FETCH_ASSOC);;

  // Close our db connection
  $db = null;

  return $gateway[0]['value'];
}


// Switch Request Types passed through Angular controller
switch ($requesttype) {


  /**
   * @name getPopularVideos
   *
   * @description
   * Get the 10 most popular videos for this week.
  **/
  case 'getPopularVideos':

    // Prep our parameters
    $limit = filter_var(trim($_GET['limit']), FILTER_VALIDATE_INT) ?
      trim($_GET['limit']) : filter_var(trim($_GET['limit']), FILTER_SANITIZE_NUMBER_INT);
    $type = filter_var(trim($_GET['videoType']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $start = new DateTime('7 days ago');
    $start->setTimezone(new DateTimeZone('America/Toronto')); // Set timezone
    $end = new DateTime('now');
    $end->setTimezone(new DateTimeZone('America/Toronto')); // Set timezone

    // Fetch our sections
    $videos = getPopularVideos($start->format('Y-m-d H:i:s'), $end->format('Y-m-d H:i:s'), $limit, $type, $publish);

    echo json_encode($videos);
    break;

  /**
   * @name getRelatedVideos
   *
   * @description
   * Get videos from category, ignoring a given ID
  **/
  case 'getRelatedVideos':

    // Parse our Data correctly
    $video = filter_var(trim($_GET['video']), FILTER_VALIDATE_INT) ?
      trim($_GET['video']) : filter_var(trim($_GET['video']), FILTER_SANITIZE_NUMBER_INT);
    $categories = $_GET['categories'];
    $catLimit = count($categories);
    for($i = 0; $i < $catLimit; $i++){
      $categories[$i] = filter_var(trim($categories[$i]['id']), FILTER_VALIDATE_INT) ?
        trim($categories[$i]['id']) : filter_var(trim($categories[$i]['id']), FILTER_SANITIZE_NUMBER_INT);
    }

    // Fetch our sections
    $videos = getRelatedVideos($video, $categories, $publish);

    echo json_encode($videos);
    //echo $videos;
    break;


  /**
   * @name getSections
   *
   * @description
   * Get all the different Video Categories
  **/
  case 'getSections':

    // Parse our Data correctly
    $id = filter_var(trim($_GET['sectionType']), FILTER_VALIDATE_INT) ?
      trim($_GET['sectionType']) : filter_var(trim($_GET['sectionType']), FILTER_SANITIZE_NUMBER_INT);
    $returnCount = isset($_GET['count']) && filter_var(trim($_GET['count']), FILTER_VALIDATE_BOOLEAN) ?
      trim($_GET['count']) : false;

    // Fetch our sections
    $sections = getSections($id);

    if($returnCount) {
      $limit = count($sections);
      for($i = 0; $i < $limit; $i++) {
        $count = getVideoCount($sections[$i]['id'], $publish);
        $sections[$i]['videoCount'] = $count[0];
      }
    }

    echo json_encode($sections);
    break;


  /**
   * @name getVideoCount
   *
   * @description
   * Returns a list of videos from a given category.
  **/
  case 'getVideoCount':

    // Parse our Data correctly
    $id = filter_var(trim($_GET['section']), FILTER_VALIDATE_INT) ?
      trim($_GET['section']) : filter_var(trim($_GET['section']), FILTER_SANITIZE_NUMBER_INT);

    // Fetch our data
    $data = getVideoCount($id, $publish);

    echo json_encode($data[0]);
    break;


  /**
   * @name getVideo
   *
   * @description
   * Get Video information and Related Category info.
   *
   * @note:
   * Trying to be rusable, this function is used both by the front-end AND the
   * admin dashboar. They work with different information, so be careful when using this function.
   *
   * @param $_POST['id']: Video id to request.
  **/
  case 'getVideo':

    // Parse our Data correctly
    $id = filter_var(trim($_POST['id']), FILTER_VALIDATE_INT) ?
      trim($_POST['id']) : filter_var(trim($_POST['id']), FILTER_SANITIZE_NUMBER_INT); 

    // Establish a database connetion.
    $db = dbconnect();

    // Prepare statements
    $stmt = $db->prepare(
      'SELECT
        videos.*,
        sections.id as `categoryId`,
        sections.name AS `categoryName`,
        sections.french_name AS `categoryNameFr`,
        sections.spanish_name AS `categoryNameEs`
       FROM videos
       LEFT JOIN video_category
        ON videos.id = video_category.videoID
       INNER JOIN sections
        ON video_category.categoryID = sections.id
       WHERE videos.id = :id'
        . $publish
    );
    $stmt->execute(array(
      ':id' => $id,
    ));
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if(isset($results[0]) && !empty($results[0])) {

      // Store video info in a separate array
      $video = $results[0];

      // Remove categories because they need to come back as their own array
      unset($video['categoryId']);
      unset($video['categoryName']);
      unset($video['categoryNameFr']);
      unset($video['categoryNameEs']);
    }

    // Get poster info.
    $poster = getPoster($video);
    $sources = getSources($video);

    // Push our video data into a container array
    $data = array('video' => $video, 'filemtime' => 0);

    // No idea why we're setting this
    if ($poster != DEFAULT_POSTER) {
      $data['filemtime'] = filemtime('../../' . $poster);
    }

    // Create an array of our related categories
    $categories = array();
    $limit = count($results);
    for($i = 0; $i < $limit; $i++) {
      $categories[$i]['id'] = $results[$i]['categoryId'];
      $categories[$i]['name'] = $results[$i]['categoryName'];
      $categories[$i]['nameFr'] = $results[$i]['categoryNameFr'];
      $categories[$i]['nameEs'] = $results[$i]['categoryNameEs'];
    }
    $data['categories'] = $categories;

    if ($video['private'] && $_SESSION['secure']!=md5($salt.$_SESSION['userid'])){
    // Generate the HTML5 Video Embed
       ob_start();
        ?>
    
        <div class="fake-poster" style="background:gray;"></div>
        <div class="fake-poster-text"><p><i class="fa fa-lock"></i> This video is private. Please
        <a href="#login-modal" data-toggle="modal"> sign in
        </a>to watch </p></div>



       <?php
     $data['html'] = ob_get_clean();
      
    }

    // video not private or user is authenticated 
    else {
      ob_start();
    ?>
      <video id="the_video"
        crossorigin=""
        class="video-js"
        controls
        preload="auto"
        poster="<?php echo $poster; ?>"
        >
        <?php echo $sources; ?>
        <p class="vjs-no-js">
          To view this video please enable JavaScript, and consider upgrading to a web browser that
          <a href="http://videojs.com/html5-video-support/"
            target="_blank"
            >
            supports HTML5 video
          </a>
        </p>
      </video>

    <?php
     $data['html'] = ob_get_clean();
    }
  
    // Close connection
    $b = null;

    // Return data
    echo json_encode($data);
    break;


  /**
   * @name getAdvertisement
   *
   * @description
   * Get Ad information?
   *
   * @param $_GET['id']: Ad id to request.
  **/
    case 'getAdvertisement':

  // Set our data
  $id = mysqli_real_escape_string($conn, $_GET['id']);
  $role = isset($_SESSION['role']) ? $_SESSION['role'] : null;
  $sqlPublish = $role == 'admin' ? "" : " AND " . PUBLISH;

  $query = "SELECT * FROM videos WHERE id = " . $id . $sqlPublish;
  $result = mysqli_query($conn, $query);
  $preroll = mysqli_fetch_assoc($result);
  $poster = getPoster($preroll);
  $data = null;

  if (isset($preroll['preroll'])) {
    $query = "SELECT * FROM videos WHERE id = " . $preroll['preroll'] . $sqlPublish;
    $result = mysqli_query($conn, $query);
    $preroll = mysqli_fetch_assoc($result);
    $data = is_null($preroll) ? null : $preroll;
  }

  echo json_encode($data);
  break;


  /**
   * @name getVideos
   *
   * @description
   * Get Video information?
   *
   * @param $_GET['id']: Ad id to request.
  **/
  case 'getVideos':

  // Set data
  $sqlPublish = $_SESSION['role'] == 'admin' ? "" : " WHERE ".PUBLISH;

  // Run Query
  $query = "SELECT * FROM videos" . $sqlPublish . " ORDER BY position ASC, publishstart DESC";
  $result = mysqli_query($conn, $query);
  $data = array('videos' => array());
  while ($row = mysqli_fetch_assoc($result)) {
    $poster = getPoster($row);
    if ($poster != DEFAULT_POSTER) {
      $row['filemtime'] = filemtime('../../' . $poster);
    }
    $row['poster'] = $poster;
    $data['videos'][] = $row;
  }

  echo json_encode($data);
  break;


  /**
   * @name getAdvertisements
   *
   * @description
   * Get All Ads?
  **/
  case 'getAdvertisements':

    // Parse our Data correctly
    $id = filter_var(trim($_POST['category']), FILTER_VALIDATE_INT) ?
      trim($_POST['category']) : filter_var(trim($_POST['category']), FILTER_SANITIZE_NUMBER_INT);

    // Establish a database connetion.
    $db = dbconnect();

    // Get the videos
    $vids = $db->prepare(
      'SELECT *
       FROM videos
       WHERE
        EXISTS(
          SELECT * FROM video_category
            WHERE videoID = videos.id
              AND categoryID = :id
        )'
        . $publish
    );
    $vids->execute(array(
      ':id' => $id,
    ));
    $vidCount = $vids->rowCount();
    $vidResults = $vids->fetchAll(PDO::FETCH_ASSOC);

    // Assemble back into an array for us to break down.
    $data = array(
      'ads'      => $vidResults,
      'count'       => $vidCount
    );

    // Close connection
    $db = null;

    // Return data
    echo json_encode($data);
    break;


  /**
   * @name getGoServices
   *
   * @description
   * Get all Go Services?
  **/
  case 'getGoServices':

    // Parse our Data correctly
    $id = filter_var(trim($_POST['category']), FILTER_VALIDATE_INT) ?
      trim($_POST['category']) : filter_var(trim($_POST['category']), FILTER_SANITIZE_NUMBER_INT);

    // Establish a database connetion.
    $db = dbconnect();

    // Get the videos
    $vids = $db->prepare(
      'SELECT *
       FROM videos
       WHERE
        EXISTS(
          SELECT * FROM video_category
            WHERE videoID = videos.id
              AND categoryID = :id
        )'
        . $publish
    );
    $vids->execute(array(
      ':id' => $id,
    ));
    $vidCount = $vids->rowCount();
    $vidResults = $vids->fetchAll(PDO::FETCH_ASSOC);

    // Assemble back into an array for us to break down.
    $data = array(
      'services'      => $vidResults,
      'count'       => $vidCount
    );

    // Close connection
    $db = null;

    // Return data
    echo json_encode($data);
    break;


  /**
   * @name getVideosInCategory
   *
   * @description
   * Returns a list of videos from a given category.
  **/
  case 'getVideosInCategory':

    // Parse our Data correctly
    $id = filter_var(trim($_POST['category']), FILTER_VALIDATE_INT) ?
      trim($_POST['category']) : filter_var(trim($_POST['category']), FILTER_SANITIZE_NUMBER_INT);

    // Fetch our data
    $data = getVideosFromCategory($id, $publish);

    echo json_encode($data);
    break;


  /**
   * @name addStream
   *
   * @description
   * Add a new video stream to the videos table.
  **/
  case 'addStream':

    // Sanatize Data
    $url = filter_var(trim($_POST['url']), FILTER_VALIDATE_URL) ? trim($_POST['url']) : filter_var(trim($_POST['url']), FILTER_SANITIZE_URL);
    $m3u8 = filter_var(trim($_POST['m3u8']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $hostname = filter_var(trim($_POST['hostname']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $port = filter_var(trim($_POST['port']), FILTER_VALIDATE_INT) ?
      trim($_POST['port']) : filter_var(trim($_POST['port']), FILTER_SANITIZE_NUMBER_INT);
    $application = filter_var(trim($_POST['application']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $streamname = filter_var(trim($_POST['streamname']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $section = filter_var(trim($_POST['section']), FILTER_VALIDATE_INT) ?
      trim($_POST['section']) : filter_var(trim($_POST['section']), FILTER_SANITIZE_NUMBER_INT);

    $errors = array();
    if ($url == "" || $m3u8 == "") $errors[] = $tr->__('Protocol is required');
    if ($hostname == "") $errors[] = $tr->__('Hostname is required');
    if ($port < 1) $errors[] = $tr->__('Port is required');
    if ($application == "") $errors[] = $tr->__('Application is required');
    if ($streamname == "") $errors[] = $tr->__('Streamname is required');
    if (!$section) $errors[] = 'Section id is required';

    if (count($errors) > 0) {
      echo json_encode(array('success' => FALSE, 'error' => "Error Adding Stream:\n\n" . implode("\n", $errors)));
      exit();
    }

    // Establish a database connetion.
    $db = dbconnect();

    // Get Position Info
    $pos = $db->prepare(
      'SELECT id, MAX(position) as newPos
        FROM videos
        WHERE EXISTS(
          SELECT * FROM video_category
          WHERE videoID = videos.id
          AND categoryID = :id)'
    );
    $pos->execute(array(
      ':id' => $section,
    ));
    $posResults = $pos->fetchAll(PDO::FETCH_ASSOC);
    $newPos = $posResults ? $posResults[0]['newPos'] : 0;
    $newPos += 1;

    // Insert Data
    $insert = $db->prepare(
        'INSERT INTO videos (
          type,
          position,
          featured,
          private,
          url,
          m3u8,
          hostname,
          port,
          application,
          streamname
        ) VALUES (
          :type,
          :position,
          :featured,
          :private,
          :url,
          :m3u8,
          :hostname,
          :port,
          :application,
          :streamname
        )'
      );
      $insert->execute(array(
        ':type'         => 'stream',
        ':position'     => $newPos,
        ':featured'     => 0,
        ':private'      => 1,
        ':url'          => $url,
        ':m3u8'         => $m3u8,
        ':hostname'     => $hostname,
        ':port'         => $port,
        ':application'  => $application,
        ':streamname'   => $streamname
      ));

      if (!$insert) {
        echo json_encode(array('success' => FALSE, 'error' => "Error Inserting Stream:\n\nFailed to insert data for stream with url \"" . $url . "\""));
        exit();
      }

      // Get new video id
      $videoID = $db->lastInsertId();

      // Inset category info
      $cat = $db->prepare(
        'INSERT INTO video_category (videoID, categoryID)
         VALUES (:video, :category)'
      );
      $cat->execute(array(
        ':video'      => $videoID,
        ':category'   => $section
      ));
      if(!$cat) {
        echo json_encode(array('success' => FALSE, 'error' => "Couldn't Add to Category Table."));
        exit();
      }

      // Close connection
      $db = null;

      // Return data
      echo json_encode(array('success' => TRUE, 'videoId' => $videoID));
      break;



case 'getLiveStream':

  $query = "SELECT * FROM videos WHERE type = 'stream' AND ".PUBLISH." ORDER BY position ASC LIMIT 1";
  $result = mysqli_query($conn, $query);
  $video = mysqli_fetch_assoc($result);

  $poster = getPoster($video);
  $sources = getSources($video);

  if ($poster != DEFAULT_POSTER) {
    $filemtime = filemtime('../../' . $poster);
  }

  $sectionQuery = "SELECT name, french_name, spanish_name FROM sections WHERE id = " .$video['section'];
  $sectionResult = mysqli_query($conn, $sectionQuery);
  $sectionName = mysqli_fetch_assoc($sectionResult);

  $data['sectionName'] = $sectionName['name'];
  $data['sectionFrenchName'] = $sectionName['french_name'];
  $data['sectionSpanishName'] = $sectionName['spanish_name'];

  $data = array('video' => $video);

  if ($video['private'] && $_SESSION['secure']!=md5($salt.$_SESSION['userid'])){
    // Generate the HTML5 Video Embed
       ob_start();
        ?>
    
        <div class="fake-poster" style="background:gray ;"></div>
        <div class="fake-poster-text"><p><i class="fa fa-lock"></i> This video is private. Please
        <a href="#login-modal" data-toggle="modal"> sign in
        </a>to watch </p></div>



       <?php
     $data['html'] = ob_get_clean();
      
    }

  else{ 


    ob_start();
      ?>
      <video id="the_video" class="video-js" controls preload="auto"
      poster="<?php echo $poster; ?>?m=<?php echo $filemtime; ?>"
      >
      <?php echo $sources; ?>
      <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
    </video>
      <?php
      $data['html'] = ob_get_clean();
    }

  echo json_encode($data);

break;


  /**
   * @name getLiveStreamCats
   *
   * @description
   * Return a list of all the live stream categories
  **/


  /**
   * @name getLiveStreams
   *
   * @description
   * Get all videos associated with a given video stream category
  **/
  case 'getLiveStreams':

    // Parse our Data correctly
    $id = filter_var(trim($_POST['section']), FILTER_VALIDATE_INT) ?
      trim($_POST['section']) : filter_var(trim($_POST['section']), FILTER_SANITIZE_NUMBER_INT);

    // Fetch our data
    $data = getVideosFromCategory($id, $publish);

    // Return data
    echo json_encode($data);
    break;



case 'getFeaturedVideo':

    $section = $_GET['section'];
    if(isset($_SESSION['role'])){
      $sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND " . PUBLISH;
    } else {
      $sqlPublish = " AND " . PUBLISH;
    }


  $query = "
    SELECT *
    FROM videos
    WHERE featured = '1' AND section IN (
      SELECT sections.id
      FROM (sections LEFT OUTER JOIN section_has_type ON
        sections.id = section_has_type.section_id) LEFT OUTER JOIN
        section_types ON section_types.id = section_has_type.section_type_id
      WHERE section_types.id = $section
    )
    ORDER BY position ASC LIMIT 1;
  ";

  $result = mysqli_query($conn, $query);
  $video = mysqli_fetch_assoc($result);

  $poster = getPoster($video);
  $sources = getSources($video);

  if ($poster != DEFAULT_POSTER) {
    $filemtime = filemtime('../../' . $poster);
  }

  echo json_encode($video);

break;



case 'getFeaturedVideos':

  $sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

  $query = "SELECT * FROM videos WHERE featured = '1'" . $sqlPublish . " ORDER BY position ASC, publishstart DESC";

  $result = mysqli_query($conn, $query);

  $data = array('videos' => array());

  while ($row = mysqli_fetch_assoc($result))
  {
    $poster = getPoster($row);
    if ($poster != DEFAULT_POSTER) {
      $row['filemtime'] = filemtime('../../' . $poster);
    }
    $row['poster'] = $poster;
    $data['videos'][] = $row;
  }

  echo json_encode($data);

break;



case 'getSectionTypes':

  $query = "SELECT * FROM section_types";
  $result = mysqli_query($conn, $query);
  $data = array('sectionTypes' => array());

  while ($row = mysqli_fetch_assoc($result))
  {
    $data['sectionTypes'][] = $row;
  }

  echo json_encode($data);

break;



case 'addSection':

  $name = mysqli_real_escape_string($conn, $_POST['name']);
  $french_name = mysqli_real_escape_string($conn, $_POST['french_name']);
  $spanish_name = mysqli_real_escape_string($conn, $_POST['spanish_name']);
  $type = mysqli_real_escape_string($conn, $_POST['type']);

  if (!$type) {
    echo json_encode(array('success' => FALSE, 'error' => "No type id provided."));
    exit();
  } else if ( (!$name && $has_en) || (!$french_name && $has_fr) || (!$spanish_name && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No name provided.'));
    exit();
  }

  $query = "
    SELECT MAX(position) as new_position
    FROM
      sections LEFT OUTER JOIN section_has_type
        ON sections.id = section_has_type.section_id

        LEFT OUTER JOIN section_types
          ON section_types.id = section_has_type.section_type_id
    WHERE section_types.id = $type
  ";

  $result = mysqli_query($conn, $query);
  $position = mysqli_fetch_assoc($result);

  $position = $position["new_position"];
  if ($position == NULL) $position = 1;
  else $position = intval($position) + 1;

  $addSection = "
    INSERT INTO sections (name, french_name, spanish_name, position)
    VALUES ('$name', '$french_name', '$spanish_name', $position);
  ";
  mysqli_query($conn, $addSection);

  $addSectionType = "
    INSERT INTO section_has_type (section_id, section_type_id)
    VALUES (" . mysqli_insert_id($conn) . ", $type);
  ";
  mysqli_query($conn, $addSectionType);

  echo json_encode(array('success' => TRUE));

break;



case 'saveSection':

  $id = mysqli_real_escape_string($conn, $_POST['id']);
  $name = mysqli_real_escape_string($conn, $_POST['name']);
  $french_name = mysqli_real_escape_string($conn, $_POST['french_name']);
  $spanish_name = mysqli_real_escape_string($conn, $_POST['spanish_name']);

  if (!$id) {
    echo json_encode(array('success' => FALSE, 'error' => "No id provided."));
    exit();
  } else if ( (!$name && $has_en) || (!$french_name && $has_fr) || (!$spanish_name && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No name provided.'));
    exit();
  }

  $query = "
    UPDATE sections
    SET name = '$name', french_name = '$french_name', spanish_name = '$spanish_name'
    WHERE id = $id;
  ";
  mysqli_query($conn, $query);

  echo json_encode(array('success' => TRUE));

break;



case 'deleteSection':

  $id = mysqli_real_escape_string($conn, $_POST['id']);

  $query = "
    SELECT mp4, webm, ogg, thumbnail
    FROM videos
    WHERE section = $id;
  ";
  $result = mysqli_query($conn, $query);

  // Delete all videos associated with the section
  while ($row = mysqli_fetch_assoc($result)) {
    @unlink("../../uploads/videos/" . $row['mp4']);
    @unlink("../../uploads/videos/" . $row['webm']);
    @unlink("../../uploads/videos/" . $row['ogg']);
    @unlink("../../uploads/images/" . $row['thumbnail']);
  }

  $deleteVideos = "
    DELETE FROM videos
      WHERE EXISTS (
        SELECT *
          FROM video_category
         WHERE video_category.videoID = videos.id AND video_category.categoryID = $id
      ) AND NOT EXISTS (
        SELECT *
          FROM video_category
         WHERE video_category.videoID = videos.id AND video_category.categoryID <> $id
      );
  ";
  mysqli_query($conn, $deleteVideos);

  $deleteSectionType = "
    DELETE FROM section_has_type
    WHERE section_id = $id;
  ";
  mysqli_query($conn, $deleteSectionType);

  $deleteSection = "
    DELETE FROM sections
    WHERE id = $id;
  ";
  mysqli_query($conn, $deleteSection);

  echo json_encode(array('success' => TRUE));

break;



case 'deleteVideo':

  $id = mysqli_real_escape_string($conn, $_POST['id']);

  $query = "SELECT * FROM videos WHERE id = " . $id;
  $result = mysqli_query($conn, $query);
  $item = mysqli_fetch_assoc($result);

  @unlink("../../uploads/videos/" . $item['mp4']);
  @unlink("../../uploads/videos/" . $item['webm']);
  @unlink("../../uploads/videos/" . $item['ogg']);
  @unlink("../../uploads/images/" . $item['thumbnail']);

  $query = "DELETE FROM videos WHERE id = " . $id;
  $result = mysqli_query($conn, $query);

  echo json_encode(array('success' => TRUE));

break;


case 'saveVideoOrder':

  $videos = $_POST['videos'];
  foreach ($videos as $key => $id) {
    $id = mysqli_real_escape_string($conn, $id);
    $key = mysqli_real_escape_string($conn, $key);

    // Set php.ini max_input_vars to real high
    $query = "UPDATE videos SET position = " . $key . " WHERE id = " . $id;
    $result = mysqli_query($conn, $query);
    var_dump($result);
  }

break;

case 'saveQuestionOrder':

  $videos = $_POST['questions'];

  foreach ($videos as $key => $id) {
    $id = mysqli_real_escape_string($conn, $id);
    $key = mysqli_real_escape_string($conn, $key);

    // Set php.ini max_input_vars to real high
    $query = "UPDATE questions SET position = " . $key . " WHERE id = " . $id;
    $result = mysqli_query($conn, $query);
    var_dump($result);
  }

break;

case 'saveSectionOrder':

  $sections = $_POST['sections'];
  foreach ($sections as $key => $id) {
    $id = mysqli_real_escape_string($conn, $id);
    $key = mysqli_real_escape_string($conn, $key);

    // Set php.ini max_input_vars to real high
    $query = "UPDATE sections SET position = " . $key . " WHERE id = " . $id;
    $result = mysqli_query($conn, $query);
    var_dump($result);
  }

break;

case 'saveCategoryOrder':

  $sections = $_POST['categories'];
  foreach ($sections as $key => $id) {
    $id = mysqli_real_escape_string($conn, $id);
    $key = mysqli_real_escape_string($conn, $key);

    // Set php.ini max_input_vars to real high
    $query = "UPDATE categories SET position = " . $key . " WHERE id = " . $id;
    $result = mysqli_query($conn, $query);
    var_dump($result);
  }

break;


// REDIRECTS
case 'saveAccount':

  unset($_SESSION['accounterror']);
  unset($_SESSION['accountsuccess']);

  $email = mysqli_real_escape_string($conn, $_POST['email']);

  if ($email == '') {
    $_SESSION['accounterror'] = $tr->__('Please enter an email address.');
    header('Location: /account/');
    break;
  } else {
    $query = 'UPDATE users SET email = "' . $email . '" WHERE id = ' . $_SESSION['userid'];

    $result = mysqli_query($conn, $query);

    if ($result) {
      $_SESSION['accountsuccess'] = $tr->__('Account details have been updated.');
      $_SESSION['email'] = $email;
    } else {
      $_SESSION['accounterror'] = $tr->__('Invalid Email Address.');
    }

    header('Location: /account/');
  }

break;



case 'saveAccountPassword':

  unset($_SESSION['accounterror']);
  unset($_SESSION['accountsuccess']);

  $password = mysqli_real_escape_string($conn, $_POST['password']);
  $passconf = mysqli_real_escape_string($conn, $_POST['passconf']);

  if ($password == '' OR $passconf == '') {
    $_SESSION['accounterror'] = $tr->__('Please enter your password in both fields.');
    header('Location: /account/password/');
    break;
  }

  if ($password !== $passconf) {
    $_SESSION['accounterror'] = $tr->__('Your passwords do not match.');
    header('Location: /account/password/');
    break;
  }

  $query = 'UPDATE users SET password = "' . md5($salt.$password) . '" WHERE id = ' . $_SESSION['userid'];

  $result = mysqli_query($conn, $query);

  if ($result) {
    $_SESSION['accountsuccess'] = $tr->__('Your account password has been updated.');
  } else {
    $_SESSION['accounterror'] = $tr->__('Your account password has not been updated.');
  }

  header('Location: /account/password/');

break;

  $adCategory = 113;


  /**
   * @name videoUpload
   *
   * @description
   * Adds a new On-Demand video to the videos table.
   * Accepts video file and moves to uploads directory.
  **/
  case 'videoUpload':

    // Parse our Data correctly
    $id = filter_var(trim($_POST['section']), FILTER_VALIDATE_INT) ?
      trim($_POST['section']) : filter_var(trim($_POST['section']), FILTER_SANITIZE_NUMBER_INT);
    $videoType = isset($_GET['ad']) && !empty($_GET['ad']) ? 'advertisement' : 'video';
    $section = isset($_GET['ad']) && !empty($_GET['ad']) ? $adCategory : $id;

    $AVCONV = "/usr/bin/avconv";
    $TOUCH = "/usr/bin/touch";
    $SSTIME = "00:00:02";
    $VIDEODIR = "../../uploads/videos/";
    $IMAGEDIR = "../../uploads/images/";

    // Upload File
    if (!isset($_FILES['file'])) {
      echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
      exit();
    }

    if (!is_array($_FILES['file'])) {
      echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
      exit();
    }

    $_FILENAME = $_FILES['file']['name'];
    $_TMPNAME = $_FILES['file']['tmp_name'];
    $FILENAME = stripslashes($_FILENAME);
    $FILENAME = preg_replace("/[^a-zA-Z0-9.]/", "", $FILENAME);
    $FILENAME = str_replace(array(' ', "'", '"', '/'), '', $FILENAME);

    // Since its a new upload, check if it exists or not
    if (file_exists($VIDEODIR.$FILENAME)) {
      // If it does, throw a timestamp on the file name
      $FILENAME = stripslashes(time().$FILENAME);
    }

    // Removing quotes and double quotes
    $FILENAME = str_replace(array('"', "'"), "", $FILENAME);

    // Set the basename and link to file
    $BASENAME = basename($FILENAME, ".mp4");
    $IMAGENAME = $BASENAME.".png";
    $OGGNAME = $BASENAME.".ogv";
    $WEBMNAME = $BASENAME.".webm";
    $PATH_TO_IMAGE = $IMAGEDIR.$IMAGENAME;
    $PATH_TO_VIDEO = $VIDEODIR.$FILENAME;
    $PATH_TO_OGG = $VIDEODIR.$OGGNAME;
    $PATH_TO_WEBM = $VIDEODIR.$WEBMNAME;

    // Move the file
    if (!move_uploaded_file($_TMPNAME, $PATH_TO_VIDEO)) {
      echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
      exit();
    }

    $command = $AVCONV." -i ".$PATH_TO_VIDEO." -r 1 -vframes 1 -ss ".$SSTIME." -s hd480 ".$PATH_TO_IMAGE;
    exec($command);

    // OGG
    //$command = $AVCONV." -n -v quiet -i ".$PATH_TO_VIDEO." -acodec libvorbis -vcodec libtheora ".$PATH_TO_OGG;
    $command = $TOUCH." ".$PATH_TO_OGG;
    exec($command);

    // WEBM
    //$command = $AVCONV." -n -v quiet -i ".$PATH_TO_VIDEO." -acodec libvorbis -vcodec libvpx ".$PATH_TO_WEBM;
    $command = $TOUCH." ".$PATH_TO_WEBM;
    exec($command);

    // Establish a database connetion.
    $db = dbconnect();

    // Get Position Info
    $pos = $db->prepare(
      'SELECT id, MAX(position) as newPos
        FROM videos
        WHERE EXISTS(
          SELECT * FROM video_category
          WHERE videoID = videos.id
          AND categoryID = :id)'
    );
    $pos->execute(array(
      ':id' => $section,
    ));
    $posResults = $pos->fetchAll(PDO::FETCH_ASSOC);
    $newPos = $posResults ? $posResults[0]['newPos'] : 0;
    $newPos += 1;

    // Insert into videos
    $insert = $db->prepare(
      'INSERT INTO videos (
          type,
          position,
          featured,
          private,
          thumbnail,
          poster,
          mp4,
          webm,
          ogg
      ) VALUES (
        :type,
        :position,
        :featured,
        :private,
        :thumbnail,
        :poster,
        :mp4,
        :webm,
        :ogg
      )'
    );
    $insert->execute(array(
      ':type'       => $videoType,
      ':position'   => $newPos,
      ':featured'   => 0,
      ':private'    => 1,
      ':thumbnail'  => $IMAGENAME,
      ':poster'     => $IMAGENAME,
      ':mp4'        => $FILENAME,
      ':webm'       => $WEBMNAME,
      ':ogg'        => $OGGNAME
    ));

    if (!$insert) {
      @unlink($PATH_TO_VIDEO);
      @unlink($PATH_TO_IMAGE);
      echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to insert data for file with name \"" . $FILENAME . "\""));
      exit();
    }

    // Get new video id
    $videoID = $db->lastInsertId();

    // Insert category info
    $cat = $db->prepare(
      'INSERT INTO video_category (videoID, categoryID)
       VALUES (:video, :category)'
    );
    $cat->execute(array(
      ':video'      => $videoID,
      ':category'   => $section
    ));
    if(!$cat) {
      echo json_encode(array('success' => FALSE, 'error' => "Couldn't Add to Category Table."));
      exit();
    }

    // Set video for something. Edit page I assume.
    $video = array(
      'thumbnail' => $IMAGENAME,
      //'filemtime' => filemtime($PATH_TO_IMAGE),
      'id'        => $videoID
    );

    echo json_encode(array('success' => TRUE, 'video' => $video));
    break;



case 'videoUploadUpdate':

  $videoId = mysqli_real_escape_string($conn, $_POST['id']);

  $VIDEODIR = "../../uploads/videos/";

  // Upload File
  if ( ! isset($_FILES['file'])) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
    exit();
  }

  if ( ! is_array($_FILES['file'])) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
    exit();
  }

  $_FILENAME = $_FILES['file']['name'];
  $_TMPNAME = $_FILES['file']['tmp_name'];

  $FILENAME = stripslashes($_FILENAME);
  $FILENAME = preg_replace("/[^a-zA-Z0-9.]/", "", $FILENAME);
  $FILENAME = str_replace(array('"', "'"), "", $FILENAME);

  // Since its a new upload, check if it exists or not
  if (file_exists($VIDEODIR . $FILENAME)) {
    // If it does, throw a timestamp on the file name
    $FILENAME = stripslashes(time() . $FILENAME);
  }

  // Set the basename and link to file
  $BASENAME = basename($FILENAME, ".mp4");
  $OGGNAME = $BASENAME.".ogv";
  $WEBMNAME = $BASENAME.".webm";
  $PATH_TO_VIDEO = $VIDEODIR . $FILENAME;

  $IMAGENAME = $BASENAME.".png";
  $PATH_TO_IMAGE = $IMAGEDIR.$IMAGENAME;

  // Move the file
  if ( ! move_uploaded_file($_TMPNAME, $PATH_TO_VIDEO)) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
    exit();
  }

  //Create new Thumbnail
  $command = $AVCONV." -i ".$PATH_TO_VIDEO." -r 1 -vframes 1 -ss ".$SSTIME." -s hd480 ".$PATH_TO_IMAGE;
  exec($command);

  $query = "UPDATE videos SET mp4 = '$FILENAME', ogg = '$OGGNAME', webm = '$WEBMNAME' WHERE id = $videoId";

  $result = mysqli_query($conn, $query);

  if ( ! $result) {
    @unlink($PATH_TO_VIDEO);
    @unlink($PATH_TO_IMAGE);
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to update data for file with name \"" . $FILENAME . "\""));
    exit();
  }

  $video = array(
    'id' => $videoId,
    'filename' => $FILENAME
  );

  echo json_encode(array('success' => TRUE, 'video' => $video));

break;



case 'thumbUpload':

  $FILENAME = mysqli_real_escape_string($conn, $_POST['name']);
  $videoId = mysqli_real_escape_string($conn, $_POST['videoId']);

  // Upload File
  if ( ! isset($_FILES['file'])) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
    exit();
  }

  if ( ! is_array($_FILES['file'])) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
    exit();
  }

  if ($FILENAME == "") {
    $FILENAME = $_FILES['file']['name'];
  }

  $_TMPNAME = $_FILES['file']['tmp_name'];
  $IMAGEDIR = "../../uploads/images/";
  $PATH_TO_IMAGE = $IMAGEDIR . $FILENAME;

  // Move the file
  if ( ! move_uploaded_file($_TMPNAME, $PATH_TO_IMAGE)) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
    exit();
  }

  $query = "UPDATE videos SET thumbnail = '$FILENAME', poster = '$FILENAME' WHERE id = " . $videoId;
  $result = mysqli_query($conn, $query);

  $thumb = array(
    'thumbnail' => $FILENAME,
    'filemtime' => filemtime($PATH_TO_IMAGE)
  );

  echo json_encode(array('success' => TRUE, 'thumb' => $thumb));

break;



case 'thumbUploadUpdate':

  $FILENAME = mysqli_real_escape_string($conn, $_POST['name']);
  $videoId = mysqli_real_escape_string($conn, $_POST['videoId']);

  // Upload File
  if ( ! isset($_FILES['file'])) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
    exit();
  }

  if ( ! is_array($_FILES['file'])) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
    exit();
  }

  if ($FILENAME == "") {
    $FILENAME = $_FILES['file']['name'];
  }

  $_TMPNAME = $_FILES['file']['tmp_name'];
  $IMAGEDIR = "../../uploads/images/";
  $PATH_TO_IMAGE = $IMAGEDIR . $FILENAME;

  // Move the file
  if ( ! move_uploaded_file($_TMPNAME, $PATH_TO_IMAGE)) {
    echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
    exit();
  }

  $query = "UPDATE videos SET thumbnail = '$FILENAME', poster = '$FILENAME' WHERE id = " . $videoId;
  $result = mysqli_query($conn, $query);

  $thumb = array(
    'thumbnail' => $FILENAME,
    'filemtime' => filemtime($PATH_TO_IMAGE)
  );

  echo json_encode(array('success' => TRUE, 'thumb' => $thumb));

break;


  /**
   * @name updateVideo
   *
   * @description
   * Save Video Attributes in the Admin Dashboard
  **/
  case 'updateVideo':

    // Sanatize our Data
    $videoId = filter_var(trim($_POST['video']['id']), FILTER_VALIDATE_INT) ?
      filter_var(trim($_POST['video']['id']), FILTER_SANITIZE_NUMBER_INT) : null;
    $title = filter_var(trim($_POST['video']['name']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $french_title = filter_var(trim($_POST['video']['french_name']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $spanish_title = filter_var(trim($_POST['video']['spanish_name']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $description = filter_var(trim($_POST['video']['description']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $french_description = filter_var(trim($_POST['video']['french_description']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $spanish_description = filter_var(trim($_POST['video']['spanish_description']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $location = filter_var(trim($_POST['video']['location']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $featured = filter_var(trim($_POST['video']['featured']), FILTER_VALIDATE_BOOLEAN) ?
      trim($_POST['video']['featured']) : false;
    $private = filter_var(trim($_POST['video']['private']), FILTER_VALIDATE_BOOLEAN) ?
      trim($_POST['video']['private']) : false;
    $type = filter_var(trim($_POST['video']['type']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $publishstart = filter_var(trim($_POST['video']['publishstart']), FILTER_SANITIZE_STRING);
    $publishend = filter_var(trim($_POST['video']['publishend']), FILTER_SANITIZE_STRING);
    $url = isset($_POST['video']['url']) && filter_var(trim($_POST['video']['url']), FILTER_VALIDATE_URL) ? trim($_POST['video']['url']) : filter_var(trim($_POST['video']['url']), FILTER_SANITIZE_URL);
    $m3u8 = filter_var(trim($_POST['video']['m3u8']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $hostname = filter_var(trim($_POST['video']['hostname']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $port = filter_var(trim($_POST['video']['port']), FILTER_VALIDATE_INT) ?
      filter_var(trim($_POST['video']['port']), FILTER_SANITIZE_NUMBER_INT) : null;
    $application = filter_var(trim($_POST['video']['application']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $streamname = filter_var(trim($_POST['video']['streamname']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $tags = filter_var(trim($_POST['video']['tags']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    $ad = filter_var(trim($_POST['video']['preroll']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
    if ($ad == "None" || intval($ad) == 0 || intval($ad) == -1) {
      $ad = -1;
    }

    // Set up empty error array
    $errors = array();

    if((!$title && $has_en) || (!$french_title && $has_fr) || (!$spanish_title && $has_es) ) {
      $errors[] = "Title is required";
    }
    if ( (!$description && $has_en) || (!$french_description && $has_fr) || (!$spanish_description && $has_es) ) {
      $errors[] = "Description is required";
    }
    if ($publishstart != "" AND $publishend == "") {
      $errors[] = "Publish end must be selected when publish start is selected.";
    }

    if($type == 'stream'){
      if ($hostname == "") $errors[] = $tr->__('Hostname is required');
      if ($port < 1) $errors[] = $tr->__('Port is required');
      if ($application == "") $errors[] = $tr->__('Application is required');
      if ($streamname == "") $errors[] = $tr->__('Streamname is required');
    }

    if ($publishstart == "") {
      $publishstart = "NULL";
    } else {
      $publishstart = "'$publishstart'";
    }

    if ($publishend == "") {
      $publishend = "NULL";
    } else {
      $publishend = "'$publishend'";
    }

    if(empty($type) || !$type) {
      $errors[] = 'Type is required';
    }

    if (count($errors) > 0) {
      echo json_encode(array('success' => FALSE, 'error' => "Error Updating Video:\n\n" . implode("\n", $errors)));
      exit();
    }

    $featured = $featured == 'true' ? '1' : '0';
    $private = $private == 'true' ? '1' : '0';

    $query = "UPDATE videos SET
      name = '$title',
      french_name = '$french_title',
      spanish_name = '$spanish_title',
      description = '$description',
      french_description = '$french_description',
      spanish_description = '$spanish_description',
      location = '$location',
      featured = '$featured',
      private = '$private',
      type = '$type',
      publishstart = $publishstart,
      publishend = $publishend,
      url = '$url',
      m3u8 = '$m3u8',
      hostname = '$hostname',
      port = '$port',
      application = '$application',
      streamname = '$streamname',
      tags = '$tags',
      preroll = $ad
    WHERE id = $videoId";
    //die(var_dump($query));
    $result = mysqli_query($conn, $query);

    // if($featured == '1'){
    //   $queryUnfeature = "UPDATE videos SET
    //     featured = '0'
    //   WHERE type = '$type' AND id != $videoId";
    //   $resultUnfeature = mysqli_query($conn, $queryUnfeature);
    // }

    if (!$result) {
      echo json_encode(array('success' => FALSE, 'error' => "Error Updating Video:\n\nFailed to update data for video with name \"" . $query . "\""));
      exit();
    }

    /**
     * Category Check.
     *
     * If no new categories came passed a long with the Video data,
     * Fall back onto the original category values.
    **/

    $categories = isset($_POST['video']['categories']) && !empty($_POST['video']['categories']) ?
      $_POST['video']['categories'] : $_POST['categories'];

    if (isset($_POST['video']['categories']) && !empty($_POST['video']['categories'])) {
      $categories = $_POST['video']['categories'];
    } else {
      $categories = array();
      foreach($_POST['categories'] as $pc) {
        array_push($categories, $pc['id']);
      }
    }

    // Trash current categories
    $deleteQuery = "DELETE FROM video_category WHERE videoID = $videoId";
    mysqli_query($conn, $deleteQuery);

    // Insert rows with the newly selected ones.
    foreach($categories as $cat) {
      $categoryQuery = "INSERT INTO video_category (videoID, categoryID) VALUES ('$videoId', '$cat')";
      if(!mysqli_query($conn, $categoryQuery)) {
        echo json_encode(array('success' => FALSE, 'error' => 'Category not deleted.'));
        exit();
      }
    }

    echo json_encode(array('success' => TRUE));
    break;


case 'getSearchVideos':

  $searchTerm = mysqli_real_escape_string($conn, $_REQUEST['search']);
  $searchTerm = urldecode(trim($searchTerm));

  $videos = array();

  if ($searchTerm == "") {
    echo json_encode($videos);
    exit();
  }

  $sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

  $query = "SELECT v.*, s.name as sectionName, s.french_name as sectionFrenchName, s.spanish_name as sectionSpanishName"
  ." FROM videos AS v LEFT JOIN sections AS s ON s.id = v.section"
  ." WHERE v.location"
  ." LIKE '%$searchTerm%' OR v.name"
  ." LIKE '%$searchTerm%' OR v.description"
  ." LIKE '%$searchTerm%' OR v.tags"
  ." LIKE '%$searchTerm%'"
  .$sqlPublish
  ." ORDER BY v.position";

  $result = mysqli_query($conn, $query);

  while ($row = mysqli_fetch_assoc($result))
  {
    $poster = getPoster($row);
    if ($poster != DEFAULT_POSTER) {
      $row['filemtime'] = filemtime('../../' . $poster);
    }
    $row['poster'] = $poster;
    $videos[] = $row;
  }

  echo json_encode(array('videos' => $videos));

break;

/**
 * Create a new Go Service
 *
 * Add a new row to the videos table, and create a corresdponding entry
 * within the video_category table.
 *
 * @name createGoButton
 * @param string $_POST['url'] given URL for stream location
 */
  case 'createGoButton':
    // Prep data
    $url = filter_var(trim($_POST['url']), FILTER_VALIDATE_URL) ?
      trim($_POST['url']) : filter_var(trim($_POST['url']), FILTER_SANITIZE_URL);
    $featured = '0';
    $private = '1';
    $category = $goServiceCategory;
    $type = 'go';

    // Check that we have valid info
    if(!$url) {
      echo json_encode(
        array(
          'success' => FALSE,
          'error'   => 'URL cannot be blank')
      );
      exit();
    }

    // Establish a database connetion.
    $db = dbconnect();
    // Insert Data
    $insert = $db->prepare(
        'INSERT INTO videos (
          name,
          description,
          section,
          location,
          featured,
          private,
          type,
          publishstart,
          publishend,
          url,
          m3u8,
          tags,
          preroll
        ) VALUES (
          :name,
          :description,
          :category,
          :location,
          :featured,
          :private,
          :type,
          :publishstart,
          :publishend,
          :url,
          :m3u8,
          :tags,
          :preroll
        )'
      );
      $insert->execute(array(
        ':name'         => '',
        ':description'  => '',
        ':category'     => $category,
        ':location'     => '',
        ':featured'     => $featured,
        ':private'      => $private,
        ':type'         => $type,
        ':publishstart' => NULL,
        ':publishend'   => NULL,
        ':url'          => $url,
        ':m3u8'         => '',
        ':tags'         => '',
        ':preroll'      => NULL
      ));

      // Check query
      if (!$insert) {
        echo json_encode(
          array(
            'success' => FALSE,
            'error' => "Error Inserting Stream:\n\nFailed to insert data for stream with url \"$url\""
          )
        );
        exit();
      }

      // Get new video id to insert into video_category table
      $videoID = $db->lastInsertId();

      // Inset category info
      $cat = $db->prepare(
        'INSERT INTO video_category (
          videoID,
          categoryID
        ) VALUES (
          :video,
          :category
        )'
      );
      $cat->execute(
        array(
          ':video'    => $videoID,
          ':category' => $category
        )
      );
      if(!$cat) {
        echo json_encode(
          array(
            'success' => FALSE,
            'error' => "Couldn't Add to Category Table."
          )
        );
        exit();
      }

      // Close connection
      $db = null;

      // Return data
      echo json_encode(
        array(
          'success' => TRUE,
          'videoId' => $videoID
        )
      );
    break;




case 'editGoButton':

  // Gets the post data from the request
  $id = mysqli_real_escape_string($conn, $_POST['id']);
  $name = mysqli_real_escape_string($conn, $_POST['name']);
  $french_name = mysqli_real_escape_string($conn, $_POST['french_name']);
  $spanish_name = mysqli_real_escape_string($conn, $_POST['spanish_name']);
  $description = mysqli_real_escape_string($conn, $_POST['description']);
  $french_description = mysqli_real_escape_string($conn, $_POST['french_description']);
  $spanish_description = mysqli_real_escape_string($conn, $_POST['spanish_description']);
  $url = mysqli_real_escape_string($conn, $_POST['url']);

  $error = NULL;

  // Checks for the required information
  if ($has_en && !$name) {
    $error = 'Title cannot be blank';
  } elseif ($has_fr && !$french_name) {
    $error = 'French Title cannot be blank';
  } elseif ($has_es && !$spanish_name) {
    $error = 'Spanish Title cannot be blank';
  } elseif (!$url) {
    $error = 'URL cannot be blank';
  } elseif (!$id) {
    $error = 'ID cannot be blank';
  }

  if (!$description) $description = '';
  if (!$french_description) $french_description = '';
  if (!$spanish_description) $spanish_description = '';

  $featured = '0';
  $private = '1';

  // Checks for any errors, returns them if they occur
  if (!is_null($error)) {
    echo json_encode(array('success' => FALSE, 'error' => $error));
    exit();
  }

  // Sets variables that are static for Go buttons
  $section = 112;
  $type = 'go';

  // Creates the query to update the Go button
  $query = "UPDATE videos
    SET name = '$name',
        french_name = '$french_name',
        spanish_name = '$spanish_name',
        description = '$description',
        french_description = '$french_description',
        spanish_description = '$spanish_description',
        section = '$section',
        location = '',
        featured = '$featured',
        private = '$private',
        url = '$url'
    WHERE id = $id";

  // Returns the result (or error if something goes wrong)
  $result = mysqli_query($conn, $query);

  if (!$result) {
    echo json_encode(array('success' => FALSE));
    exit();
  }
  echo json_encode(array('success' => TRUE));

  break;

case 'getGoButton':

  // Gets the id from the get parameters (if it exists)
  $id = mysqli_real_escape_string($conn, $_GET['id']);

  // Stops execution if no id is attached to the request
  if (!$id) {
    echo json_encode(array('success' => FALSE, 'error' => 'No ID was found in the request.'));
    exit();
  }

  // Performs the query and gets the result
  $query = "SELECT * FROM videos WHERE id = $id";
  $result = mysqli_query($conn, $query);
  $button = mysqli_fetch_assoc($result);

  // Returns the result (could be an error)
  if (is_null($button)) {
    echo json_encode(array('success' => FALSE, 'error' => "No button was found with id $id."));
    exit();
  }
  echo json_encode(array('success' => TRUE, 'data' => $button));

  break;


  /**
   * @name getGoButtons
   *
   * @description
   * Get all the GoService thumbnails for the public facing page.
  **/
  case 'getGoButtons':

    // Parse our Data correctly
    $id = 112;
    $publish = '';

    // Fetch our data
    $data = getVideosFromCategory($id, $publish);

    echo json_encode($data);
    break;



case 'getCategories':

  // Make the query for the categories
  $query = '';
  if (isset($_GET['filter']) && $_GET['filter'] == 'no_empty') {
      $query = "
        SELECT *
        FROM categories
        WHERE EXISTS(
          SELECT id
          FROM questions
          WHERE category = categories.id
        )
    ORDER BY position ASC;
      ";
  } else {
    $query = "SELECT * FROM categories ORDER BY position ASC";
  }
  $results = mysqli_query($conn, $query);

  // If no categories are found return an error
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => 'No categories were found.'));
    exit();
  }

  // Add all categories to the response
  $categories = array();
  while ($category = mysqli_fetch_assoc($results)) {
    $category['id'] = intval($category['id']);
    array_push($categories, $category);
  }

  // Return the categories in the response
  echo json_encode(array('success' => TRUE, 'data' => $categories));
  break;

case 'getCategoryCount':

  // Gets the category id from the request
  $id = mysqli_real_escape_string($conn, $_GET['category']);
  if (!$id) {
    echo json_encode(array('success' => FALSE, 'error' => 'No category id provided.'));
    exit();
  }

  // Executes the query
  $query = "SELECT COUNT(*) AS count FROM questions WHERE category = $id";
  $results = mysqli_query($conn, $query);

  // Returns an error if something went wrong
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => 'Something went wrong getting count.'));
    exit();
  }

  // Returns the count
  echo json_encode(array('success' => TRUE, 'count' => intval(mysqli_fetch_assoc($results)['count'])));
  break;

case 'createCategory':

  // Checks that the category is in the request
  $postdata = file_get_contents("php://input");
  $request = json_decode($postdata);
  $english = mysqli_real_escape_string($conn, $request->english_name);
  $french = mysqli_real_escape_string($conn, $request->french_name);
  $spanish = mysqli_real_escape_string($conn, $request->spanish_name);

  if ( (!$english && $has_en) || (!$french && $has_fr) || (!$spanish && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No name provided.'));
    exit();
  }

  // Attempts to add the category to the database
  $query = "
    INSERT INTO categories (english_category, french_category, spanish_category)
    VALUES ('$english', '$french', '$spanish');
  ";
  $results = mysqli_query($conn, $query);

  // If not successful, return an error
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => 'Category not created.'));
    exit();
  }

  // Return the newly created id
  echo json_encode(array('success' => TRUE, 'data' => mysqli_insert_id($conn)));

  break;

case 'editCategory':

  // Checks that the category is in the request
  $id = mysqli_real_escape_string($conn, $_GET['category_id']);
  $english = mysqli_real_escape_string($conn, $_GET['english_name']);
  $french = mysqli_real_escape_string($conn, $_GET['french_name']);
  $spanish = mysqli_real_escape_string($conn, $_GET['spanish_name']);

  // Checks that the required data is present
  if (!$id) {
    echo json_encode(array('success' => FALSE, 'error' => 'No category id provided.'));
    exit();
  } else if ( (!$english && $has_en) || (!$french && $has_fr) || (!$spanish && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No name provided.'));
    exit();
  }

  // Attempts to edit the category in the database
  $query = "UPDATE categories SET ";
  if ($english) $query = $query . "english_category = '$english',";
  if ($french) $query = $query . "french_category = '$french',";
  if ($spanish) $query = $query . "spanish_category = '$spanish'";
  if (substr($query, -1) == ',') $query = substr($query, 0, -1);
  $query = $query . " WHERE id = $id";
  $results = mysqli_query($conn, $query);

  // Update failed, return error
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => 'Category not updated.'));
    exit();
  }

  // Update successful, return data
  echo json_encode(array('success' => TRUE));
  break;

case 'deleteCategory':

  // Get category id from request
  $category = mysqli_real_escape_string($conn, $_GET['category_id']);
  if (!$category) {
    echo json_encode(array('success' => FALSE, 'error' => 'Category not provided.'));
    exit();
  }

  // Attempts to delete the category from the database
  $query = "DELETE FROM categories WHERE id = $category";
  $results = mysqli_query($conn, $query);

  // Deletion was not successful
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => 'Category not deleted.'));
    exit();
  }

  // Deletion was successful
  echo json_encode(array('success' => TRUE));
  break;

case 'getQuestions':

  // Gets category from request
  $category = mysqli_real_escape_string($conn, $_GET['category']);
  if (!$category) {
    echo json_encode(array('success' => FALSE, 'error' => 'No category provided'));
    exit();
  }

  // Make the query for the questions
  $query = "SELECT * FROM questions WHERE category = $category ORDER BY position ASC";
  $results = mysqli_query($conn, $query);

  // Add all questions to the response
  $questions = array();
  while ($question = mysqli_fetch_assoc($results)) {
    $question['id'] = intval($question['id']);
    $question['category'] = intval($question['category']);

    array_push($questions, $question);
  }

  // If no questions are found return an error
  if (empty($questions)) {
    echo json_encode(array('success' => FALSE, 'error' => 'No questions were found.'));
    exit();
  }

  // Return the categories in the response
  echo json_encode(array('success' => TRUE, 'data' => $questions));
  break;

case 'createQuestion':

  // Gather request parameters
  $postdata = file_get_contents("php://input");
  $request = json_decode($postdata);
  $category = $request->category_id;
  $eQuestion = addslashes($request->english_question);
  $fQuestion = addslashes($request->french_question);
  $sQuestion = addslashes($request->spanish_question);
  $english = addslashes($request->english);
  $french = addslashes($request->french);
  $spanish = addslashes($request->spanish);

  // Checks if the required values are present
  if (!$category) {
    echo json_encode(array('success' => FALSE, 'error' => 'No category provided.'));
    exit();
  } else if ( (!$eQuestion && $has_en) || (!$fQuestion && $has_fr) || (!$sQuestion && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No question provided.'));
    exit();
  } else if ( (!$english && $has_en) || (!$french && $has_fr) || (!$spanish && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No answer provided.'));
    exit();
  }

  // Attempts to add the question to the database
  $query = "
    INSERT INTO questions (category, english_question, french_question, spanish_question, english, french, spanish)
    VALUES ($category, '$eQuestion', '$fQuestion', '$sQuestion', '$english', '$french', '$spanish')
  ";
  $results = mysqli_query($conn, $query);

  // If not successful, return an error
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => 'Question not created.'));
    exit();
  }

  // Return the newly created id
  echo json_encode(array('success' => TRUE, 'data' => mysqli_insert_id($conn)));

  break;

case 'editQuestion':

  // Gather request parameters
  $id = mysqli_real_escape_string($conn, $_GET['question_id']);
  $category = mysqli_real_escape_string($conn, $_GET['category_id']);
  $eQuestion = mysqli_real_escape_string($conn, $_GET['english_question']);
  $fQuestion = mysqli_real_escape_string($conn, $_GET['french_question']);
  $sQuestion = mysqli_real_escape_string($conn, $_GET['spanish_question']);
  $english = mysqli_real_escape_string($conn, $_GET['english']);
  $french = mysqli_real_escape_string($conn, $_GET['french']);
  $spanish = mysqli_real_escape_string($conn, $_GET['spanish']);

  // Return an error if required parameters are missing
  if (!$id) {
    echo json_encode(array('success' => FALSE, 'error' => 'No question id provided.'));
    exit();
  } else if ( (!$eQuestion && $has_en) || (!$fQuestion && $has_fr) || (!$sQuestion && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No question provided.'));
    exit();
  } else if ( (!$english && $has_en) || (!$french && $has_fr) || (!$spanish && $has_es) ) {
    echo json_encode(array('success' => FALSE, 'error' => 'No answer provided.'));
    exit();
  }

  // Set up the query
  $query = "UPDATE questions SET ";
  if ($category) $query = $query . "category = $category,";
  if ($eQuestion) $query = $query . "english_question = '$eQuestion',";
  if ($fQuestion) $query = $query . "french_question = '$fQuestion',";
  if ($sQuestion) $query = $query . "spanish_question = '$sQuestion',";
  if ($english) $query = $query . "english = '$english',";
  if ($french) $query = $query . "french = '$french',";
  if ($spanish) $query = $query . "spanish = '$spanish'";
  if (substr($query, -1) == ',') $query = substr($query, 0, -1);
  $query = $query . " WHERE id = $id";

  // Run the query
  $results = mysqli_query($conn, $query);

  // Return an error if question not updated
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => $query));
    exit();
  }

  // If question was updated, return a success
  echo json_encode(array('success' => TRUE));

  break;

case 'deleteQuestion':

  // Get and validate the question id
  $question = mysqli_real_escape_string($conn, $_GET['question']);
  if (!$question) {
    echo json_encode(array('success' => FALSE, 'error' => 'No question id provided'));
    exit();
  }

  // Create and run query
  $query = "DELETE FROM questions WHERE id = $question";
  $results = mysqli_query($conn, $query);

  // If question is not deleted return an error
  if (!$results) {
    echo json_encode(array('success' => FALSE, 'error' => 'Question not deleted.'));
    exit();
  }

  // If question is deleted return a success
  echo json_encode(array('success' => TRUE));
  break;

case 'saveUserQueue':

  $userId = $_SESSION['userid'];
  $id = mysqli_real_escape_string($conn, $_POST['id']);
  $time = mysqli_real_escape_string($conn, $_POST['time']);

  $query = "INSERT INTO queue (
    userId,
    videoId,
    time
  ) VALUES (
    $userId,
    $id,
    '$time'
  ) ON DUPLICATE KEY UPDATE
    time = '$time'
  ";

  $result = mysqli_query($conn, $query);

  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'error' => 'Could not save queue.'));
    exit();
  }

  $queueId = mysqli_insert_id($conn);

  echo json_encode(array('success' => TRUE, 'queue' => 'Queue was saved.'));
  break;

case 'getUserQueue':

  $userId = $_SESSION['userid'];
  $id = mysqli_real_escape_string($conn, $_POST['id']);

  $query = "SELECT time FROM queue WHERE userId = " . $userId . " AND videoId = " . $id;
  $result = mysqli_query($conn, $query);

  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'error' => 'No queue found.'));
    exit();
  }

  $queue = mysqli_fetch_assoc($result);

  echo json_encode(array('success' => TRUE, 'queue' => $queue['time']));
  break;

case 'getAccount':

  $userId = $_SESSION['userid'];

  $query = "SELECT * FROM users WHERE id = " . $userId;
  $result = mysqli_query($conn, $query);

  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'error' => 'No user found.'));
    exit();
  }

  $userinfo = mysqli_fetch_assoc($result);

  \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));

  if($userinfo['stripeId']){
    $customer = \Stripe\Customer::retrieve( $userinfo['stripeId'] );

    $userinfo['stripe'] = $customer;

    if($customer['sources']['total_count'] < 1){
      if($customer['sources']['total_count'] < 1){
        $_SESSION['subscription_error'] = TRUE;
      } else if ($customer['subscriptions']['total_count'] < 1){
        $_SESSION['subscription_error'] = TRUE;
      } else {
        $_SESSION['subscription_error'] = null;
      }
    }
  }
  $selectquery = "SELECT * FROM users";
  $selectresult = mysqli_query($conn, $selectquery);

  $alluserinfo = [];

  if ($selectresult->num_rows > 0) {
      // output data of each row
      while($row = $selectresult->fetch_assoc()) {
        $thisuser = [];
          $alluserinfo[$row["id"]] = $row;
      }
  } else {
      //echo "0 results";
  }

  echo json_encode(array('success' => TRUE, 'user' => $userinfo, 'allusers' => $alluserinfo));
  break;


/**
 * @name accountChangeEmail
 *
 * @description
 * Update users email
**/
case 'accountChangeEmail':

  // Get user info
  $userId = $_SESSION['userid'];
  $query = "SELECT * FROM users WHERE id = " . $userId;
  $result = mysqli_query($conn, $query);
  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'response' => 'No user found.'));
    exit();
  }
  $userinfo = mysqli_fetch_assoc($result);

  // Get new email address
  $email = mysqli_real_escape_string($conn, $_POST['user']['email']);

  // Set email
  $responseMsg = null;
  if ($email == '' || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $responseMsg = $tr->__('Please enter a valid email address.');
    echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
    exit();
  } else {

    // Check for an email again
    if($userinfo['email'] != $email) {

      // Make sure they don't try to use someone else's email
      $check_query = "SELECT * FROM users WHERE email = '" . $email . "'";
      $check_result = mysqli_query($conn, $check_query);
      if (mysqli_num_rows($check_result) > 0) {
        $responseMsg = $tr->__('Sorry, that email address already exists.');
        echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
        exit();
      } else {

        // Update email in our system
        $email_query = 'UPDATE users SET email = "' . $email . '", username = "' . $email . '" WHERE id = ' . $_SESSION['userid'];
        $email_result = mysqli_query($conn, $email_query);

        // If stripe is our payment gateway, update the Stripe account
        if($paymentGateway == '2') {
          \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));
          $customer = \Stripe\Customer::retrieve( $userinfo['stripeId'] );
          $customer->email = "$email";
          $customer->save();
        }

        // Update session
        $_SESSION['username'] = $email;
        $_SESSION['email'] = $email;

        $responseMsg = $tr->__('Success! Email address has been updated.');
        echo json_encode(array('success' => TRUE, 'response' => $responseMsg));
        exit();

      }

    } else {

      $responseMsg = $tr->__('Nothing was updated.');
      echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
      exit();

    }

  }
  break;


/**
 * @name accountChangePassword
 *
 * @description
 * Update users password
**/

case 'accountChangePassword':

  // Get user info
  $userId = $_SESSION['userid'];
  $query = "SELECT * FROM users WHERE id = " . $userId;
  $result = mysqli_query($conn, $query);
  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'response' => 'No user found.'));
    exit();
  }
  $userinfo = mysqli_fetch_assoc($result);

  $password = mysqli_real_escape_string($conn, $_POST['user']['password']);
  $passwordConfirm = mysqli_real_escape_string($conn, $_POST['user']['passwordConfirm']);

  $responseMsg = null;

  if ($password == '' || strlen($password) < 5) {

    $responseMsg = $tr->__('Please enter a password at least 5 characters long.');
    echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
    exit();

  } else if ($password != $passwordConfirm) {

    $responseMsg = $tr->__('Passwords do not match.');
    echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
    exit();

  } else {

    $query = 'UPDATE users SET password = "' . md5($salt.$password) . '" WHERE id = ' . $userId;
    $result = mysqli_query($conn, $query);

    if ($result) {
      $responseMsg = $tr->__('Success! Password has been updated.');
      echo json_encode(array('success' => TRUE, 'response' => $responseMsg));
      exit();
    } else {
      $responseMsg = $tr->__('Failed to update password.');
      echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
      exit();
    }

  }
  break;

case 'accountChangeCard':

  $userId = $_SESSION['userid'];

  $query = "SELECT * FROM users WHERE id = " . $userId;
  $result = mysqli_query($conn, $query);

  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'response' => 'No user found.'));
    exit();
  }

  $userinfo = mysqli_fetch_assoc($result);

  $token = $_POST['user']['stripeToken'];

  $responseMsg = null;

  if ($token == '') {

    $responseMsg = $tr->__('Invalid Stripe token.');
    echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
    exit();

  } else {

    \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));
    $customer = \Stripe\Customer::retrieve( $userinfo['stripeId'] );
    $cardId = $customer['sources']['data'][0]['id'];
    $customer->sources->create(array("source" => $token));
    if($cardId){
      $customer->sources->retrieve($cardId)->delete();
    }
    $customer = \Stripe\Customer::retrieve( $userinfo['stripeId'] );
    $userinfo['stripe'] = $customer;

    if($customer['sources']['total_count'] < 1){
      $_SESSION['subscription_error'] = TRUE;
    } else if ($customer['subscriptions']['total_count'] < 1){
      $_SESSION['subscription_error'] = TRUE;
    } else {
      $_SESSION['subscription_error'] = null;
    }

    if ($customer) {
      $responseMsg = $tr->__('Success! Credit card has been updated.');
      echo json_encode(array('success' => TRUE, 'response' => $responseMsg, 'user' => $userinfo));
      exit();
    } else {
      $responseMsg = $tr->__('Failed to update credit card.');
      echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
      exit();
    }

  }
  break;

case 'accountChangePlan':

  $userId = $_SESSION['userid'];

  $query = "SELECT * FROM users WHERE id = " . $userId;
  $result = mysqli_query($conn, $query);

  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'response' => 'No user found.'));
    exit();
  }

  $userinfo = mysqli_fetch_assoc($result);

  $plan = $_POST['user']['plan'];

  $responseMsg = null;

  if ($plan == '') {

    $responseMsg = $tr->__('Please select a plan.');
    echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
    exit();

  } else {

    \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));
    $customer = \Stripe\Customer::retrieve( $userinfo['stripeId'] );
    $subscriptionId = $customer['subscriptions']['data'][0]['id'];
    $subscription = $customer->subscriptions->retrieve($subscriptionId);
    $subscription->plan = $plan['id'];
    $subscription->save();
    $customer = \Stripe\Customer::retrieve( $userinfo['stripeId'] );
    $userinfo['stripe'] = $customer;

    if ($customer) {
      $responseMsg = $tr->__('Success! Plan has been updated.');
      echo json_encode(array('success' => TRUE, 'response' => $responseMsg, 'user' => $userinfo));
      exit();
    } else {
      $responseMsg = $tr->__('Failed to update plan.');
      echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
      exit();
    }

  }
  break;

case 'deleteAccount':

  $userId = $_SESSION['userid'];

  $query = "SELECT * FROM users WHERE id = " . $userId;
  $result = mysqli_query($conn, $query);

  if ( ! $result) {
    echo json_encode(array('success' => FALSE, 'error' => 'No user found.'));
    exit();
  }

  $userinfo = mysqli_fetch_assoc($result);

  // Stripe check
  if($paymentGateway == '2') {
    \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));
    $customer = \Stripe\Customer::retrieve($userinfo['stripeId']);
    $customer->delete();
  }

  $deleteUser = "
    DELETE FROM users
    WHERE id = $userId;
  ";
  mysqli_query($conn, $deleteUser);

  echo json_encode(array('success' => TRUE));
  break;

  /**
   * @name accountCreate
   *
   * @description
   * Accept email and password and create account
   *
   * Additionally: If Stripe is set as te payment gateway, Set a subcription
   * and process a credit card.
  **/
  case 'accountCreate':

    $email = mysqli_real_escape_string($conn, $_POST['user']['email']);
    $password = mysqli_real_escape_string($conn, $_POST['user']['password']);
    $plan = $paymentGateway == '2' ? mysqli_real_escape_string($conn, $_POST['user']['plan']['id']) : '';
    $token = $paymentGateway == '2' ? $_POST['user']['stripeToken'] : '';

    $check_query = "SELECT * FROM users WHERE email = '" . $email . "'";
    $check_result = mysqli_query($conn, $check_query);

    $responseMsg = null;

    if ($email == '' || !filter_var($email, FILTER_VALIDATE_EMAIL)) {

      $responseMsg = $tr->__('Please enter a valid email address.');
      echo json_encode(array('success' => FALSE, 'response' => $responseMsg, 'error' => 1));
      exit();

    } else if (mysqli_num_rows($check_result) > 0) {

      $responseMsg = $tr->__('Sorry, that email address already exists.');
      echo json_encode(array('success' => FALSE, 'response' => $responseMsg, 'error' => 1));
      exit();

    } else if ($password == '' || strlen($password) < 5) {

      $responseMsg = $tr->__('Please enter a password at least 5 characters long.');
      echo json_encode(array('success' => FALSE, 'response' => $responseMsg, 'error' => 1));
      exit();

    } else {

      // If Stripe
      if($paymentGateway == '2') {
        if ($plan == '') {
          $responseMsg = $tr->__('Please select a plan.');
          echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
          exit();
        } else if ($token == '') {
          $responseMsg = $tr->__('invalid-stripe-token');
          echo json_encode(array('success' => FALSE, 'response' => $responseMsg, 'error' => 3));
          exit();
        } else {
          \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));
          $customer = \Stripe\Customer::create(array(
            "source" => $token,
            "plan" => "$plan",
            "email" => "$email")
          );

          $customer_stripeid = $customer['id'];
        }
      } else {
        $customer_stripeid = null;
      }

      $password = md5($salt.$password);

      $query = "INSERT INTO users (
        stripeId,
        username,
        password,
        email,
        role
      ) VALUES (
        '$customer_stripeid',
        '$email',
        '$password',
        '$email',
        'user'
      )";

      $result = mysqli_query($conn, $query);

      if ( !$result ) {
        echo json_encode(array('success' => FALSE, 'response' => $query));
        exit();
      } else {
        $lang = $_SESSION['lang'];
        session_destroy();
        session_start();
        $_SESSION['lang'] = $lang;
        $_SESSION['userid'] = mysqli_insert_id($conn);
        $_SESSION['username'] = $email;
        $_SESSION['email'] = $email;
        $_SESSION['role'] = 'user';
        $_SESSION['settings'] = '';
        $_SESSION['secure'] = md5($salt.$_SESSION['userid']);
        echo json_encode(array('success' => TRUE, 'response' => $customer));
        exit();
      }

    }
    break;

  /**
   * @name accountLogin
   *
   * @description
   * Verify users provided authentication against our database.
   *
   * @optional
   * If stripe is set as the payment gateway, verify the user has
   * a valid stripeID and a valid subscription from stripe.
   *
   * @param email: user provided email
   * @param password: user provided password
   * @param stripeId: optional stripe customer id.
  **/
  case 'accountLogin':
    $email = mysqli_real_escape_string($conn, $_POST['user']['email']);
    $password = mysqli_real_escape_string($conn, $_POST['user']['password']);
    $password = md5($salt.$password);

    $query = "SELECT * FROM users WHERE email = '" . $email . "' AND password = '".$password."'";
    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) > 0) {

      $userinfo = mysqli_fetch_assoc($result);

      // Stripe check
      if($paymentGateway == '2') {
        \Stripe\Stripe::setApiKey(getenv('STRIPEPRIVATEKEY'));
        if($userinfo['stripeId']){
          $customer = \Stripe\Customer::retrieve( $userinfo['stripeId'] );

          // Check that they have a valid stripe subscription
          if(
            !$customer->subscriptions->data[0]->status
              || $customer->subscriptions->data[0]->status != 'active'
          ) {
            $responseMsg = $tr->__('Your subscription is no longer active. Please contact a site administrator.');
            echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
            exit();
          } else {
            $userinfo['stripe'] = $customer;
          }

        } else {
          $responseMsg = $tr->__('No stripe plan is associated with your account.');
          echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
          exit();
        }
      }

    //login
    $lang = $_SESSION['lang'];
    session_destroy();
    session_start();
    $_SESSION['lang'] = $lang;
    $_SESSION['userid'] = $userinfo['id'];
    $_SESSION['username'] = $userinfo['email'];
    $_SESSION['email'] = $userinfo['email'];
    $_SESSION['role'] = $userinfo['role'];
    $_SESSION['settings'] = $userinfo['settings'];
    $_SESSION['secure'] = md5($salt.$_SESSION['userid']);
    if($paymentGateway == '2') {
      if($customer['sources']['total_count'] < 1){
        $_SESSION['subscription_error'] = TRUE;
      } else if ($customer['subscriptions']['total_count'] < 1){
        $_SESSION['subscription_error'] = TRUE;
      } else {
        $_SESSION['subscription_error'] = null;
      }
    }
    echo json_encode(array('success' => TRUE, 'response' => $userinfo));
    exit();

  } else {

    $responseMsg = $tr->__('Invalid Email Address/Password.');
    echo json_encode(array('success' => FALSE, 'response' => $responseMsg));
    exit();

  }
  break;

default: echo 'default'; break;
}