<?php

// Functions File

if ( ! isset($requesttype)) { $requesttype = $_GET['type']; }

include('security.php');
session_start();
require_once('class.translation.php');
$translate = new Translator($_SESSION['lang'],'../');

define('DEFAULT_POSTER', "//placehold.it/384x216/000000&text=Image+Not+Found");
define('PUBLISH', "(publishstart <= NOW() AND publishend > NOW() OR publishstart IS NULL OR publishend IS NULL)");

function getSources($video) {
	if ($video['url'] != '' OR $video['m3u8'] != '') {
		$array = array(
			'url' => '<source src="%s" type="rtmp/mp4">',
			'm3u8' => '<source src="%s" type="application/vnd.apple.mpegurl">'
		);
	} else {
		$array = array(
			'mp4' => '<source src="uploads/videos/%s" type="video/mp4" />',
			'webm' => '<source src="uploads/videos/%s" type="video/webm" />',
			'ogg' => '<source src="uploads/videos/%s" type="video/ogg" />'
		);
	}

	$html = '';

	foreach ($array as $type => $source) {
		$videoType = isset($video[$type]) ? $video[$type] : '';
		$html .= sprintf($source, $videoType);
	}

	return $html;
};

function getPoster($video) {
	$poster = isset($video['poster']) ? $video['poster'] : '';
	if ($poster != "") {
		$poster = "uploads/images/" . $poster;
		if ( ! file_exists("../" . $poster)) {
			$poster = DEFAULT_POSTER;
		}
	} else {
		$poster = DEFAULT_POSTER;
	}
	return $poster;
};

switch ($requesttype)
{
case 'getSections':

	// Query
	$query = "SELECT * FROM sections ORDER BY position ASC";

	// Run query
	$result = mysqli_query($conn, $query);

	// Init array
	$data = array('sections' => array());

	// Push values into array
	while ($row = mysqli_fetch_assoc($result))
	{
		$rowQuery = "SELECT COUNT(*) as count FROM videos WHERE type = 'video' AND section = " . $row['id'];
		if (isset($_GET['override'])) $rowQuery = $rowQuery . " AND " . PUBLISH;

		$rowResult = mysqli_query($conn, $rowQuery);
		$rowItem = mysqli_fetch_assoc($rowResult);
		$row['videoCount'] = $rowItem['count'];
		$data['sections'][] = $row;
	}

	echo json_encode($data);

break;



case 'getVideo':

	$id = mysqli_real_escape_string($conn, $_POST['id']);

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

	$query = "SELECT * FROM videos WHERE id = " . $id . $sqlPublish;
	$result = mysqli_query($conn, $query);
	$video = mysqli_fetch_assoc($result);
	$data = array('video' => $video, 'filemtime' => 0);

	$poster = getPoster($video);
	$sources = getSources($video);

	if ($poster != DEFAULT_POSTER) {
		$data['filemtime'] = filemtime('../' . $poster);
	}

	if ($video['private'] && $_SESSION['secure']!=md5($salt.$_SESSION['userid'])){
		$data['html'] = '<div class="fake-poster" style="background-image:url('.$poster.');"></div><div class="fake-poster-text"><p><i class="fa fa-lock"></i> '.$translate->__('This video is private. Please','../').' <a href="#login-modal" data-toggle="modal">'. $translate->__('sign in','../').'</a> '.$translate->__('to watch','../').'.</p></div>';
	} else {
		$data['html'] = '<video id="the_video" style="width:100%" class="video-js vjs-sublime-skin" controls preload="auto" width="auto" height="auto" poster="'.$poster.'">
	'.$sources.'
	<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video>';
	}

	echo json_encode($data);

break;

case 'getAdvertisement':

	$id = mysqli_real_escape_string($conn, $_GET['id']);

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

	$query = "SELECT * FROM videos WHERE id = " . $id . $sqlPublish;
	$result = mysqli_query($conn, $query);
	$preroll = mysqli_fetch_assoc($result);

	$poster = getPoster($preroll);

	$query = "SELECT * FROM videos WHERE id = " . $preroll['preroll'] . $sqlPublish;
	$result = mysqli_query($conn, $query);
	$preroll = mysqli_fetch_assoc($result);

	//$sources = getSources($preroll);

	if ($poster != DEFAULT_POSTER) {
		//$data['filemtime'] = filemtime('../' . $poster);
	}

	if (is_null($preroll)) $data = null;
	else $data = $preroll;

	echo json_encode($data);


break;

case 'getVideos':

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " WHERE ".PUBLISH;

	$query = "SELECT * FROM videos" . $sqlPublish . " ORDER BY position ASC, publishstart DESC";
	$result = mysqli_query($conn, $query);
	$data = array('videos' => array());

	while ($row = mysqli_fetch_assoc($result))
	{
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		$data['videos'][] = $row;
	}

	echo json_encode($data);

break;

case 'getAdvertisements':

	// Admins get all advertisements, users only get published advertisements
	$publish = $_SESSION['role'] == 'admin' ? ' WHERE ' : ' WHERE ' . PUBLISH . ' AND ';

	// Makes the query and gets the results for all advertisements
	$query = 'SELECT * FROM videos' . $publish . 'section=-1 ORDER BY position ASC, publishstart DESC';
	$results = mysqli_query($conn, $query);
	$data = array('advertisements' => array());

	// Adds all of the advertisements to the results array
	while ($row = mysqli_fetch_assoc($results))
	{
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		$data['advertisements'][] = $row;
	}

	echo json_encode($data);

break;

case 'getSectionsAndVideos':

	$sqlType = $sqlType2 = "";
	if (isset($_POST['type'])) {
		$type = mysqli_real_escape_string($conn, $_POST['type']);
		$sqlType = " WHERE type = '" . $type . "'";
		$sqlType2 = " AND type = '" . $type . "'";
	}

	$sqlPublish = ' AND ' . PUBLISH;
	if (!isset($_POST['override'])) {
		$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;
	}

	// Queries
	$sectionsQuery = "SELECT * FROM sections ORDER BY position ASC";
	$videosQuery = "SELECT * FROM videos" . $sqlType . $sqlPublish . " ORDER BY position ASC, publishstart DESC";

	// Run queries
	$sectionsResult = mysqli_query($conn, $sectionsQuery);
	$videosResult = mysqli_query($conn, $videosQuery);

	// Init array
	$data = array('sections' => array(), 'videos' => array());

	// Push values into array
	while ($row = mysqli_fetch_assoc($sectionsResult))
	{
		$rowQuery = "SELECT COUNT(*) as count FROM videos WHERE section = " . $row['id'] . $sqlType2;
		if (isset($_POST['override'])) $rowQuery = $rowQuery . $sqlPublish;

		$rowResult = mysqli_query($conn, $rowQuery);
		$rowItem = mysqli_fetch_assoc($rowResult);
		$row['videoCount'] = $rowItem['count'];

		$data['sections'][] = $row;
	}

	while ($row = mysqli_fetch_assoc($videosResult))
	{
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		$data['videos'][] = $row;
	}

	echo json_encode($data);

break;



case 'addStream':

	$url = mysqli_real_escape_string($conn, $_POST['url']);
	$m3u8 = mysqli_real_escape_string($conn, $_POST['m3u8']);

	$url = trim($url);
	$m3u8 = trim($m3u8);

	$errors = array();

	if ($url == "") { $errors[] = $translate->__('Stream Link is required','../'); }
	if ($m3u8 == "") { $errors[] = $translate->__('m3u8 Link is required'); }

	if (count($errors) > 0) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Adding Stream:\n\n" . implode("\n", $errors)));
		exit();
	}

	$query = "SELECT MAX(position) as newPos FROM videos WHERE type = 'stream'";
	$result = mysqli_query($conn, $query);
	$newPos = 0;

	if (mysqli_num_rows($result) > 0) {
		$item = mysqli_fetch_assoc($result);
		$newPos = $item['newPos'];
		$newPos += 1;
	}

	$query = "INSERT INTO videos (
		name,
		description,
		section,
		location,
		featured,
		private,
		type,
		position,
		publishstart, publishend,
		url, m3u8
	) VALUES (
		'',
		'',
		'0',
		'',
		'0',
		'1',
		'stream',
		'$newPos',
		NULL, NULL,
		'$url', '$m3u8'
	)";

	$result = mysqli_query($conn, $query);

	if ( ! $result) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Inserting Stream:\n\nFailed to insert data for stream with url \"" . $url . "\""));
		exit();
	}

	$videoId = mysqli_insert_id($conn);

	echo json_encode(array('success' => TRUE, 'videoId' => $videoId));

break;



case 'getLiveStream':

	$query = "SELECT * FROM videos WHERE type = 'stream' AND ".PUBLISH." ORDER BY position ASC LIMIT 1";
	$result = mysqli_query($conn, $query);
	$video = mysqli_fetch_assoc($result);

	$poster = getPoster($video);
	$sources = getSources($video);

	if ($poster != DEFAULT_POSTER) {
		$filemtime = filemtime('../' . $poster);
	}

	$data = array('video' => $video);

	$data['html'] = '<video id="the_video" style="width:100%" class="video-js vjs-sublime-skin" controls preload="auto" width="auto" height="auto" poster="'.$poster.'?m='.$filemtime.'">
	'.$sources.'
	<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>';

	echo json_encode($data);

break;



case 'getLiveStreams':

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

	$query = "SELECT * FROM videos WHERE type = 'stream'" . $sqlPublish . " ORDER BY position ASC, publishstart DESC";
	$result = mysqli_query($conn, $query);
	$data = array('videos' => array());

	while ($row = mysqli_fetch_assoc($result))
	{
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		$data['videos'][] = $row;
	}

	echo json_encode($data);

break;



case 'getFeaturedVideo':

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

	$query = "SELECT * FROM videos WHERE featured = '1'" . $sqlPublish . " ORDER BY position ASC LIMIT 1";
	$result = mysqli_query($conn, $query);
	$video = mysqli_fetch_assoc($result);

	$poster = getPoster($video);
	$sources = getSources($video);

	if ($poster != DEFAULT_POSTER) {
		$filemtime = filemtime('../' . $poster);
	}

	echo '<video id="the_video" style="width:100%" class="video-js vjs-sublime-skin" controls preload="auto" width="auto" height="auto" poster="'.$poster.'?m='.$filemtime.'">
	'.$sources.'
	<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video>';

break;



case 'getFeaturedVideos':

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

	$query = "SELECT * FROM videos WHERE featured = '1'" . $sqlPublish . " ORDER BY position ASC, publishstart DESC";

	$result = mysqli_query($conn, $query);

	$data = array('videos' => array());

	while ($row = mysqli_fetch_assoc($result))
	{
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		$data['videos'][] = $row;
	}

	echo json_encode($data);

break;



case 'getSectionTypes':

	$query = "SELECT * FROM sectionTypes";
	$result = mysqli_query($conn, $query);
	$data = array('sectionTypes' => array());

	while ($row = mysqli_fetch_assoc($result))
	{
		$data['sectionTypes'][] = $row;
	}

	echo json_encode($data);

break;



case 'addSection':

	$name = mysqli_real_escape_string($conn, $_POST['name']);
	// $type = mysqli_real_escape_string($conn, $_POST['type']);

	$query = "SELECT MAX(position) as newPos FROM sections";
	$result = mysqli_query($conn, $query);
	$item = mysqli_fetch_assoc($result);

	$newPos = $item['newPos'];
	$newPos += 1;

	// $query = "INSERT INTO sections (name, type, position) VALUES ('".$name."', ".$type.", ".$newPos.")";
	$query = "INSERT INTO sections (name, position) VALUES ('".$name."', ".$newPos.")";

	$result = mysqli_query($conn, $query);

	echo json_encode(array('success' => TRUE));

break;



case 'saveSection':

	$id = mysqli_real_escape_string($conn, $_POST['id']);
	$name = mysqli_real_escape_string($conn, $_POST['name']);
	// $type = mysqli_real_escape_string($conn, $_POST['type']);

	// $query = "UPDATE sections SET name = '".$name."', type = ".$type." WHERE id = ".$id;
	$query = "UPDATE sections SET name = '".$name."' WHERE id = ".$id;

	$result = mysqli_query($conn, $query);

	echo json_encode(array('success' => TRUE));

break;



case 'deleteSection':

	$id = mysqli_real_escape_string($conn, $_POST['id']);

	$query = "SELECT * FROM videos WHERE section = " . $id;
	$result = mysqli_query($conn, $query);
	while ($row = mysqli_fetch_assoc($result)) {
		@unlink("../uploads/videos/" . $row['mp4']);
		@unlink("../uploads/videos/" . $row['webm']);
		@unlink("../uploads/videos/" . $row['ogg']);
		@unlink("../uploads/images/" . $row['thumbnail']);
	}

	$query = "DELETE FROM videos WHERE section = " . $id;
	$result = mysqli_query($conn, $query);

	$query = "DELETE FROM sections WHERE id = " . $id;
	$result = mysqli_query($conn, $query);

	echo json_encode(array('success' => TRUE));

break;



case 'deleteVideo':

	$id = mysqli_real_escape_string($conn, $_POST['id']);

	$query = "SELECT * FROM videos WHERE id = " . $id;
	$result = mysqli_query($conn, $query);
	$item = mysqli_fetch_assoc($result);

	@unlink("../uploads/videos/" . $item['mp4']);
	@unlink("../uploads/videos/" . $item['webm']);
	@unlink("../uploads/videos/" . $item['ogg']);
	@unlink("../uploads/images/" . $item['thumbnail']);

	$query = "DELETE FROM videos WHERE id = " . $id;
	$result = mysqli_query($conn, $query);

	echo json_encode(array('success' => TRUE));

break;



case 'saveVideoOrder':

	$videos = $_POST['videos'];
	foreach ($videos as $key => $id) {
		$id = mysqli_real_escape_string($conn, $id);
		$key = mysqli_real_escape_string($conn, $key);

		// Set php.ini max_input_vars to real high
		$query = "UPDATE videos SET position = " . $key . " WHERE id = " . $id;
		$result = mysqli_query($conn, $query);
		var_dump($result);
	}

break;

case 'saveSectionOrder':

	$sections = $_POST['sections'];
	foreach ($sections as $key => $id) {
		$id = mysqli_real_escape_string($conn, $id);
		$key = mysqli_real_escape_string($conn, $key);

		// Set php.ini max_input_vars to real high
		$query = "UPDATE sections SET position = " . $key . " WHERE id = " . $id;
		$result = mysqli_query($conn, $query);
		var_dump($result);
	}

break;



// REDIRECTS
case 'saveAccount':

	unset($_SESSION['accounterror']);
	unset($_SESSION['accountsuccess']);

	$email = mysqli_real_escape_string($conn, $_POST['email']);

	if ($email == '') {
		$_SESSION['accounterror'] = $translate->__('Please enter an email address.');
		header('Location: ../account.php');
		break;
	} else {
		$query = 'UPDATE users SET email = "' . $email . '" WHERE id = ' . $_SESSION['userid'];

		$result = mysqli_query($conn, $query);

		if ($result) {
			$_SESSION['accountsuccess'] = $translate->__('Account details have been updated.');
			$_SESSION['email'] = $email;
		} else {
			$_SESSION['accounterror'] = $translate->__('Invalid Email Address.');
		}

		header('Location: ../account.php');
	}

break;



case 'saveAccountPassword':

	unset($_SESSION['accounterror']);
	unset($_SESSION['accountsuccess']);

	$password = mysqli_real_escape_string($conn, $_POST['password']);
	$passconf = mysqli_real_escape_string($conn, $_POST['passconf']);

	if ($password == '' OR $passconf == '') {
		$_SESSION['accounterror'] = $translate->__('Please enter your password in both fields.');
		header('Location: ../account-password.php');
		break;
	}

	if ($password !== $passconf) {
		$_SESSION['accounterror'] = $translate->__('Your passwords do not match.');
		header('Location: ../account-password.php');
		break;
	}

	$query = 'UPDATE users SET password = "' . md5($salt.$password) . '" WHERE id = ' . $_SESSION['userid'];

	$result = mysqli_query($conn, $query);

	if ($result) {
		$_SESSION['accountsuccess'] = $translate->__('Your account password has been updated.');
	} else {
		$_SESSION['accounterror'] = $translate->__('Your account password has not been updated.');
	}

	header('Location: ../account-password.php');

break;



case 'videoUpload':

	$section = mysqli_real_escape_string($conn, $_POST['section']);

	$videoType = 'video';
	if ($_GET['ad']) {
		$videoType = 'advertisement';
		$section = -1;
	}

	// $AVCONV = "/usr/local/bin/avconv";
	$AVCONV = "/usr/bin/avconv";
	$TOUCH = "/usr/bin/touch";
	$SSTIME = "00:00:02";
	$VIDEODIR = "../uploads/videos/";
	$IMAGEDIR = "../uploads/images/";

	// Upload File
	if ( ! isset($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
		exit();
	}

	if ( ! is_array($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
		exit();
	}

	$_FILENAME = $_FILES['file']['name'];
	$_TMPNAME = $_FILES['file']['tmp_name'];

	$FILENAME = stripslashes($_FILENAME);
	$FILENAME = str_replace(array(' ', "'", '"', '/'), '', $FILENAME);

	// Since its a new upload, check if it exists or not
	if (file_exists($VIDEODIR.$FILENAME)) {
		// If it does, throw a timestamp on the file name
		$FILENAME = stripslashes(time().$FILENAME);
	}

	// Removing quotes and double quotes
	$FILENAME = str_replace(array('"', "'"), "", $FILENAME);

	// Set the basename and link to file
	$BASENAME = basename($FILENAME, ".mp4");
	$IMAGENAME = $BASENAME.".png";
	$OGGNAME = $BASENAME.".ogv";
	$WEBMNAME = $BASENAME.".webm";
	$PATH_TO_IMAGE = $IMAGEDIR.$IMAGENAME;
	$PATH_TO_VIDEO = $VIDEODIR.$FILENAME;
	$PATH_TO_OGG = $VIDEODIR.$OGGNAME;
	$PATH_TO_WEBM = $VIDEODIR.$WEBMNAME;

	// Move the file
	if ( ! move_uploaded_file($_TMPNAME, $PATH_TO_VIDEO)) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
		exit();
	}

	$command = $AVCONV." -i ".$PATH_TO_VIDEO." -r 1 -vframes 1 -ss ".$SSTIME." -s hd480 ".$PATH_TO_IMAGE;
	exec($command);

	// OGG
	//$command = $AVCONV." -n -v quiet -i ".$PATH_TO_VIDEO." -acodec libvorbis -vcodec libtheora ".$PATH_TO_OGG;
	$command = $TOUCH." ".$PATH_TO_OGG;
	exec($command);

	// WEBM
	//$command = $AVCONV." -n -v quiet -i ".$PATH_TO_VIDEO." -acodec libvorbis -vcodec libvpx ".$PATH_TO_WEBM;
	$command = $TOUCH." ".$PATH_TO_WEBM;
	exec($command);

	//${AVCONV} -i ${file} -r 1 -vframes 1 -ss 00:02:00 -s vga ${POSTERSDIR}/${NAME}.png

	$query = "SELECT MAX(position) as newPos FROM videos WHERE section = " . $section;
	$result = mysqli_query($conn, $query);
	$newPos = 0;

	if (mysqli_num_rows($result) > 0) {
		$item = mysqli_fetch_assoc($result);
		$newPos = $item['newPos'];
		$newPos += 1;
	}

	$query = "INSERT INTO videos (
		type,
		position,
		featured,
		private,
		name,
		description,
		location,
		section,
		thumbnail, poster,
		publishstart, publishend,
		url, m3u8, mp4, webm, ogg, tags
	) VALUES (
		'$videoType',
		$newPos,
		0,
		1,
		'',
		'',
		'',
		$section,
		'$IMAGENAME', '$IMAGENAME',
		NULL, NULL,
		'', '', '$FILENAME', '$WEBMNAME', '$OGGNAME', ''
	)";

	$result = mysqli_query($conn, $query);

	if ( ! $result) {
		@unlink($PATH_TO_VIDEO);
		@unlink($PATH_TO_IMAGE);
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to insert data for file with name \"" . $FILENAME . "\""));
		exit();
	}

	$videoId = mysqli_insert_id($conn);

	$video = array(
		'thumbnail' => $IMAGENAME,
		'filemtime' => filemtime($PATH_TO_IMAGE),
		'id' => $videoId
	);

	echo json_encode(array('success' => TRUE, 'video' => $video));

break;



case 'videoUploadUpdate':

	$videoId = mysqli_real_escape_string($conn, $_POST['id']);

	$VIDEODIR = "../uploads/videos/";

	// Upload File
	if ( ! isset($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
		exit();
	}

	if ( ! is_array($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
		exit();
	}

	$_FILENAME = $_FILES['file']['name'];
	$_TMPNAME = $_FILES['file']['tmp_name'];

	$FILENAME = stripslashes($_FILENAME);

	// Since its a new upload, check if it exists or not
	if (file_exists($VIDEODIR . $FILENAME)) {
		// If it does, throw a timestamp on the file name
		$FILENAME = stripslashes(time() . $FILENAME);
	}

	// Removing quotes and double quotes
	$FILENAME = str_replace(array('"', "'"), "", $FILENAME);

	// Set the basename and link to file
	$BASENAME = basename($FILENAME, ".mp4");
	$OGGNAME = $BASENAME.".ogv";
	$WEBMNAME = $BASENAME.".webm";
	$PATH_TO_VIDEO = $VIDEODIR . $FILENAME;

  $IMAGENAME = $BASENAME.".png";
  $PATH_TO_IMAGE = $IMAGEDIR.$IMAGENAME;

	// Move the file
	if ( ! move_uploaded_file($_TMPNAME, $PATH_TO_VIDEO)) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
		exit();
	}

  //Create new Thumbnail
  $command = $AVCONV." -i ".$PATH_TO_VIDEO." -r 1 -vframes 1 -ss ".$SSTIME." -s hd480 ".$PATH_TO_IMAGE;
  exec($command);

	$query = "UPDATE videos SET mp4 = '$FILENAME', ogg = '$OGGNAME', webm = '$WEBMNAME', thumbnail='$IMAGENAME' WHERE id = $videoId";

	$result = mysqli_query($conn, $query);

	if ( ! $result) {
		@unlink($PATH_TO_VIDEO);
		@unlink($PATH_TO_IMAGE);
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to update data for file with name \"" . $FILENAME . "\""));
		exit();
	}

	$video = array(
		'id' => $videoId,
		'filename' => $FILENAME
	);

	echo json_encode(array('success' => TRUE, 'video' => $video));

break;



case 'thumbUpload':

	$FILENAME = mysqli_real_escape_string($conn, $_POST['name']);
	$videoId = mysqli_real_escape_string($conn, $_POST['videoId']);

	// Upload File
	if ( ! isset($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
		exit();
	}

	if ( ! is_array($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
		exit();
	}

	if ($FILENAME == "") {
		$FILENAME = $_FILES['file']['name'];
	}

	$_TMPNAME = $_FILES['file']['tmp_name'];
	$IMAGEDIR = "../uploads/images/";
	$PATH_TO_IMAGE = $IMAGEDIR . $FILENAME;

	// Move the file
	if ( ! move_uploaded_file($_TMPNAME, $PATH_TO_IMAGE)) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
		exit();
	}

	$query = "UPDATE videos SET thumbnail = '$FILENAME', poster = '$FILENAME' WHERE id = " . $videoId;
	$result = mysqli_query($conn, $query);

	$thumb = array(
		'thumbnail' => $FILENAME,
		'filemtime' => filemtime($PATH_TO_IMAGE)
	);

	echo json_encode(array('success' => TRUE, 'thumb' => $thumb));

break;



case 'thumbUploadUpdate':

	$FILENAME = mysqli_real_escape_string($conn, $_POST['name']);
	$videoId = mysqli_real_escape_string($conn, $_POST['videoId']);

	// Upload File
	if ( ! isset($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nNo file(s) detected."));
		exit();
	}

	if ( ! is_array($_FILES['file'])) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\n'file' is not an array."));
		exit();
	}

	if ($FILENAME == "") {
		$FILENAME = $_FILES['file']['name'];
	}

	$_TMPNAME = $_FILES['file']['tmp_name'];
	$IMAGEDIR = "../uploads/images/";
	$PATH_TO_IMAGE = $IMAGEDIR . $FILENAME;

	// Move the file
	if ( ! move_uploaded_file($_TMPNAME, $PATH_TO_IMAGE)) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Uploading File:\n\nFailed to save file with name \"" . $FILENAME . "\""));
		exit();
	}

	$query = "UPDATE videos SET thumbnail = '$FILENAME', poster = '$FILENAME' WHERE id = " . $videoId;
	$result = mysqli_query($conn, $query);

	$thumb = array(
		'thumbnail' => $FILENAME,
		'filemtime' => filemtime($PATH_TO_IMAGE)
	);

	echo json_encode(array('success' => TRUE, 'thumb' => $thumb));

break;



case 'updateVideo':

	$videoId = mysqli_real_escape_string($conn, $_POST['id']);
	$title = mysqli_real_escape_string($conn, $_POST['title']);
	$description = mysqli_real_escape_string($conn, $_POST['description']);
	$section = mysqli_real_escape_string($conn, $_POST['section']);
	$location = mysqli_real_escape_string($conn, $_POST['location']);
	$featured = mysqli_real_escape_string($conn, $_POST['featured']);
	$private = mysqli_real_escape_string($conn, $_POST['private']);
	$type = mysqli_real_escape_string($conn, $_POST['type']);
	$publishstart = mysqli_real_escape_string($conn, $_POST['publishstart']);
	$publishend = mysqli_real_escape_string($conn, $_POST['publishend']);
	$url = mysqli_real_escape_string($conn, $_POST['url']);
	$m3u8 = mysqli_real_escape_string($conn, $_POST['m3u8']);
  $tags = mysqli_real_escape_string($conn, $_POST['tags']);
  $ad = mysqli_real_escape_string($conn, $_POST['selectedAd']);

  if ($ad == "None") $ad = "NULL";

	$title = trim($title);
	$description = trim($description);
	$location = trim($location);
	$publishstart = trim($publishstart);
	$publishend = trim($publishend);

	$errors = array();

	if ($title == "") { $errors[] = "Title is required"; }
	if ($description == "") { $errors[] = "Description is required"; }

	if ($publishstart != "" AND $publishend == "") {
		$errors[] = "Publish end must be selected when publish start is selected.";
	}

	if ($publishstart == "") {
		$publishstart = "NULL";
	} else {
		$publishstart = "'$publishstart'";
	}

	if ($publishend == "") {
		$publishend = "NULL";
	} else {
		$publishend = "'$publishend'";
	}

	if ($type != 'video' AND $type != 'stream' AND $type != 'advertisement') { $errors[] = "Type is required"; }

	if (count($errors) > 0) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Updating Video:\n\n" . implode("\n", $errors)));
		exit();
	}

	if ($type == 'stream') {
		$section = '0';
	}

	$featured = $featured == 'true' ? '1' : '0';
	$private = $private == 'true' ? '1' : '0';

	$query = "UPDATE videos SET
		name = '$title',
		description = '$description',
		section = '$section',
		location = '$location',
		featured = '$featured',
		private = '$private',
		type = '$type',
		publishstart = $publishstart,
		publishend = $publishend,
		url = '$url',
		m3u8 = '$m3u8',
		tags = '$tags',
		preroll = $ad
	WHERE id = $videoId";
	// die(var_dump($query));
	$result = mysqli_query($conn, $query);

	if ( ! $result) {
		echo json_encode(array('success' => FALSE, 'error' => "Error Updating Video:\n\nFailed to update data for video with name \"" . $query . "\""));
		exit();
	}

	echo json_encode(array('success' => TRUE));

break;


case 'getSearchVideos':

	$searchTerm = mysqli_real_escape_string($conn, $_REQUEST['search']);
	$searchTerm = urldecode(trim($searchTerm));

	$videos = array();

	if ($searchTerm == "") {
		echo json_encode($videos);
		exit();
	}

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

	$query = "SELECT v.*, s.name as sectionName"
	." FROM videos v JOIN sections s ON s.id = v.section"
	." WHERE v.location"
	." LIKE '%$searchTerm%' OR v.name"
	." LIKE '%$searchTerm%' OR v.description"
	." LIKE '%$searchTerm%' OR v.tags"
	." LIKE '%$searchTerm%'"
	.$sqlPublish
	." ORDER BY v.position";

	$result = mysqli_query($conn, $query);

	while ($row = mysqli_fetch_assoc($result))
	{
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		$videos[] = $row;
	}

	echo json_encode(array('videos' => $videos));

break;


case 'getRelatedVideos':

	$categoryId = mysqli_real_escape_string($conn, $_REQUEST['categoryId']);
	$videoId = mysqli_real_escape_string($conn, $_REQUEST['videoId']);

	$query = "SELECT type FROM videos WHERE id = $videoId";
	$result = mysqli_query($conn, $query);
	$row = mysqli_fetch_assoc($result);

	$type = "video";
	if ($row['type'] == 'stream') {
		$type = "stream";
	}

	$categoryId = trim($categoryId);
	$videoId = trim($videoId);

	$videos = array();

	$sqlPublish = $_SESSION['role'] == 'admin' ? "" : " AND ".PUBLISH;

	if ($type == 'stream') {
		$query = "SELECT * FROM videos WHERE type = '$type' AND id != " . $videoId . $sqlPublish . " ORDER BY position, publishstart DESC";
	} else {
		$query = "SELECT v.*, s.name as sectionName FROM videos v LEFT JOIN sections s ON s.id = v.section WHERE v.type = '$type' AND v.section = $categoryId AND v.id != " . $videoId . $sqlPublish . " ORDER BY v.position, v.publishstart DESC";
	}

	$result = mysqli_query($conn, $query);

	while ($row = mysqli_fetch_assoc($result))
	{
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		$videos[] = $row;
	}

	echo json_encode(array('videos' => $videos));

break;

case 'createGoButton':

	// Gets the post data from the request
	$url = mysqli_real_escape_string($conn, $_POST['url']);

	$error = NULL;

	// Checks for the required information
	if (!$url) {
		$error = 'URL cannot be blank';
	}

	$featured = '0';
	$private = '1';

	// Checks for any errors, returns them if they occur
	if (!is_null($error)) {
		echo json_encode(array('success' => FALSE, 'error' => $error));
		exit();
	}

	// Sets variables that are static for Go buttons
	$section = -2;
	$type = 'go';

	// Creates the query to insert the Go button
	$query = "INSERT INTO videos
		(name, description, section, location, featured, private,
			type, publishstart, publishend, url, m3u8, tags, preroll)
		VALUES ('', '', '$section', '', '$featured',
			'$private', '$type', NULL, NULL, '$url', '', '', NULL)";

	// Returns the result (or error if something goes wrong)
	$result = mysqli_query($conn, $query);

	if (!$result) {
		echo json_encode(array('success' => FALSE));
		exit();
	}
	echo json_encode(array('success' => TRUE, 'button' => mysqli_insert_id($conn)));

	break;

case 'editGoButton':

	// Gets the post data from the request
	$id = mysqli_real_escape_string($conn, $_POST['id']);
	$name = mysqli_real_escape_string($conn, $_POST['name']);
	$description = mysqli_real_escape_string($conn, $_POST['description']);
	$url = mysqli_real_escape_string($conn, $_POST['url']);

	$error = NULL;

	// Checks for the required information
	if (!$name) $error = 'Name cannot be blank';
	else if (!$url) $error = 'URL cannot be blank';
	else if (!$id) $error = 'ID cannot be blank';

	if (!$description) $description = '';

	$featured = '0';
	$private = '1';

	// Checks for any errors, returns them if they occur
	if (!is_null($error)) {
		echo json_encode(array('success' => FALSE, 'error' => $error));
		exit();
	}

	// Sets variables that are static for Go buttons
	$section = -2;
	$type = 'go';

	// Creates the query to update the Go button
	$query = "UPDATE videos
		SET name = '$name',
				description = '$description',
				section = '$section',
				location = '',
				featured = '$featured',
				private = '$private',
				url = '$url'
		WHERE id = $id";

	// Returns the result (or error if something goes wrong)
	$result = mysqli_query($conn, $query);

	if (!$result) {
		echo json_encode(array('success' => FALSE));
		exit();
	}
	echo json_encode(array('success' => TRUE));

	break;

case 'getGoButton':

	// Gets the id from the get parameters (if it exists)
	$id = mysqli_real_escape_string($conn, $_GET['id']);

	// Stops execution if no id is attached to the request
	if (!$id) {
		echo json_encode(array('success' => FALSE, 'error' => 'No ID was found in the request.'));
		exit();
	}

	// Performs the query and gets the result
	$query = "SELECT * FROM videos WHERE id = $id";
	$result = mysqli_query($conn, $query);
	$button = mysqli_fetch_assoc($result);

	// Returns the result (could be an error)
	if (is_null($button)) {
		echo json_encode(array('success' => FALSE, 'error' => "No button was found with id $id."));
		exit();
	}
	echo json_encode(array('success' => TRUE, 'data' => $button));

	break;

case 'getGoButtons':
	$section = -2;

	// Performs the query and gets the results
	$query = "SELECT * FROM videos WHERE section = '$section'";
	$results = mysqli_query($conn, $query);

	// Returns if no GO buttons are found
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'No buttons were found.'));
		exit();
	}

	// Adds all of the buttons to the response
	$arrayOfButtons = array();
	while ($row = mysqli_fetch_assoc($results)) {
		$poster = getPoster($row);
		if ($poster != DEFAULT_POSTER) {
			$row['filemtime'] = filemtime('../' . $poster);
		}
		$row['poster'] = $poster;
		array_push($arrayOfButtons, $row);
	}

	echo json_encode(array('success' => TRUE, 'data' => $arrayOfButtons));

	break;

case 'getCategories':

	// Make the query for the categories
	$query = "SELECT * FROM categories";
	$results = mysqli_query($conn, $query);

	// If no categories are found return an error
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'No categories were found.'));
		exit();
	}

	// Add all categories to the response
	$categories = array();
	while ($category = mysqli_fetch_assoc($results)) {
		$category['id'] = intval($category['id']);
		array_push($categories, $category);
	}

	// Return the categories in the response
	echo json_encode(array('success' => TRUE, 'data' => $categories));
	break;

case 'getCategoryCount':

	// Gets the category id from the request
	$id = mysqli_real_escape_string($conn, $_GET['category']);
	if (!$id) {
		echo json_encode(array('success' => FALSE, 'error' => 'No category id provided.'));
		exit();
	}

	// Executes the query
	$query = "SELECT COUNT(*) AS count FROM questions WHERE category = $id";
	$results = mysqli_query($conn, $query);

	// Returns an error if something went wrong
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'Something went wrong getting count.'));
		exit();
	}

	// Returns the count
	echo json_encode(array('success' => TRUE, 'count' => intval(mysqli_fetch_assoc($results)['count'])));
	break;

case 'createCategory':

	// Checks that the category is in the request
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$english = $request->english_name;
	$french = $request->french_name;

	if (!$english) {
		echo json_encode(array('success' => FALSE, 'error' => 'No English name provided.'));
		exit();
	} else if (!$french) {
		echo json_encode(array('success' => FALSE, 'error' => 'No French name provided.'));
		exit();
	}

	// Attempts to add the category to the database
	$query = "
		INSERT INTO categories (english_category, french_category)
		VALUES ('$english', '$french');
	";
	$results = mysqli_query($conn, $query);

	// If not successful, return an error
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'Category not created.'));
		exit();
	}

	// Return the newly created id
	echo json_encode(array('success' => TRUE, 'data' => mysqli_insert_id($conn)));

	break;

case 'editCategory':

	// Checks that the category is in the request
	$id = mysqli_real_escape_string($conn, $_GET['category_id']);
	$english = mysqli_real_escape_string($conn, $_GET['english_name']);
	$french = mysqli_real_escape_string($conn, $_GET['french_name']);

	// Checks that the required data is present
	if (!$id) {
		echo json_encode(array('success' => FALSE, 'error' => 'No category id provided.'));
		exit();
	}

	// Attempts to edit the category in the database
	$query = "UPDATE categories SET ";
	if ($english) $query = $query . "english_category = '$english',";
	if ($french) $query = $query . "french_category = '$french'";
	if (substr($query, -1) == ',') $query = substr($query, 0, -1);
	$query = $query . " WHERE id = $id";
	$results = mysqli_query($conn, $query);

	// Update failed, return error
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'Category not updated.'));
		exit();
	}

	// Update successful, return data
	echo json_encode(array('success' => TRUE));
	break;

case 'deleteCategory':

	// Get category id from request
	$category = mysqli_real_escape_string($conn, $_GET['category_id']);
	if (!$category) {
		echo json_encode(array('success' => FALSE, 'error' => 'Category not provided.'));
		exit();
	}

	// Attempts to delete the category from the database
	$query = "DELETE FROM categories WHERE id = $category";
	$results = mysqli_query($conn, $query);

	// Deletion was not successful
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'Category not deleted.'));
		exit();
	}

	// Deletion was successful
	echo json_encode(array('success' => TRUE));
	break;

case 'getQuestions':

	// Gets category from request
	$category = mysqli_real_escape_string($conn, $_GET['category']);
	if (!$category) {
		echo json_encode(array('success' => FALSE, 'error' => 'No category provided'));
		exit();
	}

	// Make the query for the questions
	$query = "SELECT * FROM questions WHERE category = $category";
	$results = mysqli_query($conn, $query);

	// Add all questions to the response
	$questions = array();
	while ($question = mysqli_fetch_assoc($results)) {
		$question['id'] = intval($question['id']);
		$question['category'] = intval($question['category']);

		array_push($questions, $question);
	}

	// If no questions are found return an error
	if (empty($questions)) {
		echo json_encode(array('success' => FALSE, 'error' => 'No questions were found.'));
		exit();
	}

	// Return the categories in the response
	echo json_encode(array('success' => TRUE, 'data' => $questions));
	break;

case 'createQuestion':

	// Gather request parameters
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$category = $request->category_id;
	$eQuestion = $request->english_question;
	$fQuestion = $request->french_question;
	$english = $request->english;
	$french = $request->french;

	// Checks if the required values are present
	if (!$category) {
		echo json_encode(array('success' => FALSE, 'error' => 'No category provided.'));
		exit();
	} else if (!$eQuestion) {
		echo json_encode(array('success' => FALSE, 'error' => 'No English question provided.'));
		exit();
	} else if (!$fQuestion) {
		echo json_encode(array('success' => FALSE, 'error' => 'No French question provided.'));
		exit();
	} else if (!$english) {
		echo json_encode(array('success' => FALSE, 'error' => 'No English answer provided.'));
		exit();
	} else if (!$french) {
		echo json_encode(array('success' => FALSE, 'error' => 'No French answer provided.'));
		exit();
	}

	// Attempts to add the question to the database
	$query = "
		INSERT INTO questions (category, english_question, french_question, english, french)
		VALUES ($category, '$eQuestion', '$fQuestion', '$english', '$french')
	";
	$results = mysqli_query($conn, $query);

	// If not successful, return an error
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'Question not created.'));
		exit();
	}

	// Return the newly created id
	echo json_encode(array('success' => TRUE, 'data' => mysqli_insert_id($conn)));

	break;

case 'editQuestion':

	// Gather request parameters
	$id = mysqli_real_escape_string($conn, $_GET['question_id']);
	$category = mysqli_real_escape_string($conn, $_GET['category_id']);
	$eQuestion = mysqli_real_escape_string($conn, $_GET['english_question']);
	$fQuestion = mysqli_real_escape_string($conn, $_GET['french_question']);
	$english = mysqli_real_escape_string($conn, $_GET['english']);
	$french = mysqli_real_escape_string($conn, $_GET['french']);

	// Return an error if required parameters are missing
	if (!$id) {
		echo json_encode(array('success' => FALSE, 'error' => 'No question id provided.'));
		exit();
	}

	// Set up the query
	$query = "UPDATE questions SET ";
	if ($category) $query = $query . "category = $category,";
	if ($eQuestion) $query = $query . "english_question = '$eQuestion',";
	if ($fQuestion) $query = $query . "french_question = '$fQuestion',";
	if ($english) $query = $query . "english = '$english',";
	if ($french) $query = $query . "french = '$french'";
	if (substr($query, -1) == ',') $query = substr($query, 0, -1);
	$query = $query . " WHERE id = $id";

	// Run the query
	$results = mysqli_query($conn, $query);

	// Return an error if question not updated
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => $query));
		exit();
	}

	// If question was updated, return a success
	echo json_encode(array('success' => TRUE));

	break;

case 'deleteQuestion':

	// Get and validate the question id
	$question = mysqli_real_escape_string($conn, $_GET['question']);
	if (!$question) {
		echo json_encode(array('success' => FALSE, 'error' => 'No question id provided'));
		exit();
	}

	// Create and run query
	$query = "DELETE FROM questions WHERE id = $question";
	$results = mysqli_query($conn, $query);

	// If question is not deleted return an error
	if (!$results) {
		echo json_encode(array('success' => FALSE, 'error' => 'Question not deleted.'));
		exit();
	}

	// If question is deleted return a success
	echo json_encode(array('success' => TRUE));
	break;

default: echo 'whatchu doin in here?'; break;
}
