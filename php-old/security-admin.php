<?php
include('config.php');
session_start();
// Redirect to login page if not authenticated
if($_SESSION['secure']!=md5($salt.$_SESSION['userid'])){header('Location: index.php');}
// Redirect to home page if not an admin
if($_SESSION['role']!='admin'){header('Location: home.php');}