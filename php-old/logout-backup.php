<?php

require('config.php');

session_start();

define("TOOLKIT_PATH", '/var/www/flowportal/php-saml-2.4.0/');
require(TOOLKIT_PATH . '_toolkit_loader.php');

$auth = new OneLogin_Saml2_Auth();

$auth->logout('https://sso.clearcable.ca/index.php');            // Process the Logout Request & Logout Response
$errors = $auth->getErrors(); // Retrieves possible validation errors
if (empty($errors)) {
  session_destroy();
  header('Location: /index.php');
} else {
  print_r('<p>'.implode(', ', $errors).'</p>');
}
