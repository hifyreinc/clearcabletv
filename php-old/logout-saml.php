<?php

require('config.php');

session_start();

define("TOOLKIT_PATH", '/var/www/flowportal/php-saml-2.4.0/');
require(TOOLKIT_PATH . '_toolkit_loader.php');

$auth = new OneLogin_Saml2_Auth();

$paramters = array();
$nameId = null;
$sessionIndex = null;
if (isset($_SESSION['samlNameId'])) {
  $nameId = $_SESSION['samlNameId'];
}
if (isset($_SESSION['samlSessionIndex'])) {
  $sessionIndex = $_SESSION['samlSessionIndex'];
}

$auth->logout('https://ccapgo.com/index.php', $paramters, $nameId, $sessionIndex);
/*
$errors = $auth->getErrors(); // Retrieves possible validation errors
if (empty($errors)) {
  session_destroy();
  header('Location: /index.php');
} else {
  print_r('<p>'.implode(', ', $errors).'</p>');
}
*/
