#!/bin/bash
# Patrick Maille - patrickm@clearcable.ca
# 2015 - Clearcable Networks

# set -x

FTPDIR="/srv/pump_data/CABLE14"
UPLOADDIR="${FTPDIR}/uploads"
PUBLICDIR="${FTPDIR}/public"
PORTALDIR="${FTPDIR}/toportal"
POSTERSDIR="${FTPDIR}/posters"
AVCONV="/usr/bin/avconv"
DBPW="TesTing4231"
MYSQL="/usr/bin/mysql"
PORTAL="flow2.localnet"
REMOTEUSER="hifyre"


if ! [ -x ${AVCONV} ]
        then exit 1
fi

cd ${UPLOADDIR} || exit 2

## Grab the video file, parse filename and dates, and generate poster image
## filename-11112233445566-11112233445566.mp4
for file in *.mp4
        do
        test -f "${file}" || continue
        NAME=`echo ${file} | cut -d- -f1`
        PUBLISHSTART=`echo ${file} | cut -d- -f2`
        PUBLISHEND=`echo ${file} | cut -d- -f3 | cut -d\. -f1`
        # echo "NAME: ${NAME} - STARTDATE: ${PUBLISHSTART} - ENDDATE: ${PUBLISHEND}"
        ${AVCONV} -i ${file} -r 1 -vframes 1 -ss 00:02:00 -s vga ${POSTERSDIR}/${NAME}.png
        test -f "${POSTERSDIR}/${NAME}.png" || continue
        mv ${file} ${PUBLICDIR}/${NAME}.mp4 && chown root:root ${PUBLICDIR}/${NAME}.mp4 && chmod 400 ${PUBLICDIR}/${NAME}.mp4 && chmod 644 ${POSTERSDIR}/${NAME}.png

## Create mysql insert statement for the portal

        rsync -p -e ssh ${POSTERSDIR}/${NAME}.png ${REMOTEUSER}@${PORTAL}:/var/www/flowportal/uploads/images/ && rm ${POSTERSDIR}/${NAME}.png

        ${MYSQL} -h ${PORTAL} -p${DBPW} cable14 << EOF
update videos set position=position+1;
insert into videos (position,name,description,location,section,thumbnail,poster,url,m3u8) values (0,"${NAME}","${NAME}","Hamilton, ON",6,"${NAME}.png","${NAME}.png","rtmp://pump.cdn.clearcable.net:1935/c14vod/${NAME}.mp4","http://pump.cdn.clearcable.net:1935/c14vod/${NAME}.mp4/playlist.m3u8");
EOF

        if [ "${PUBLISHSTART}" != "00000000000000" ]
        then
        ${MYSQL} -h ${PORTAL} -p${DBPW} cable14 << EOF1
update videos set publishstart=${PUBLISHSTART} where position=0 and name="${NAME}";
EOF1
        fi

        if [ "${PUBLISHEND}" != "00000000000000" ]
        then
${MYSQL} -h ${PORTAL} -p${DBPW} cable14 << EOF2
update videos set publishend=${PUBLISHEND} where position=0 and name="${NAME}";
EOF2
        fi

done

exit 0
