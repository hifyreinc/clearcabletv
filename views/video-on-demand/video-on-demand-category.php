<div ng-controller="CategoryController"
  ng-init="typesection = 2"
  >

  <?php

    // Set section for include settings
    $section = 'video-on-demand';
    
    require_once(realpath(__DIR__ . '/../_includes/_banner-empty.php'));
    require_once(realpath(__DIR__ . '/../_includes/_submenu-video-on-demand-new.php'));

  ?>

  <div class="main-content-area"
    ng-cloak
    >

    <?php

      // Define title
      $title  = '<span ng-if="lang == \'en\'">{{categoryTitle}}</span>';
      $title .= '<span ng-if="lang == \'fr\'">{{categoryFenchTitle}}</span>';
      $title .= '<span ng-if="lang == \'es\'">{{categorySpanishTitle}}</span>';
      require_once(realpath(__DIR__ . '/../_includes/strip-title.php'));

      // Define Loop Variables
      $loop['index'] = 'video';
      $loop['array'] = 'videos';
      require_once(realpath(__DIR__ . '/../_includes/category-strip.php'));
    ?>

  </div>
</div>