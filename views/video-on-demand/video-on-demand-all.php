<div ng-controller="CategoriesController"
  ng-init="typesection = 2"
  >
  <?php

    // Set section for include settings
    $section = 'video-on-demand';

    require_once(realpath(__DIR__ . '/../_includes/_banner-live-streams.php'));
    require_once(realpath(__DIR__ . '/../_includes/_submenu-video-on-demand-new.php'));

  ?>
  <div class="main-content-area">

    <!-- top 10 videos -->
    <div id="popularVideos">

      <?php

        // Define Title Variables
        $title  = $tr->__('Most Popular');
        require(realpath(__DIR__ . '/../_includes/strip-title.php'));
        
        // Define Loop Variables
        $loop['index'] = 'popular';
        $loop['array'] = 'popularVideos';

        // Include Loop template
        require(realpath(__DIR__ . '/../_includes/filmstrip.php'));

      ?>
      

    </div>

    <!-- All section categories -->
    <div ng-repeat="stream in streamSections"
      ng-cloak
      >

      <?php

        // Define Title Variables
        $title  = '<span ng-if="lang == \'en\'">{{ stream.name }}</span>';
        $title .= '<span ng-if="lang == \'fr\'">{{ stream.french_name }}</span>';
        $title .= '<span ng-if="lang == \'es\'">{{ stream.spanish_name }}</span>';
        $seeAll = '/'. $section .'/category/?categoryId={{ stream.id }}';

        // Include Title template
        require(realpath(__DIR__ . '/../_includes/strip-title.php'));

        // Define Loop Variables
        $loop['index'] = 'video';
        $loop['array'] = 'stream.videos';

        // Include Loop template
        require(realpath(__DIR__ . '/../_includes/filmstrip.php'));

      ?>
    </div>
  </div>
</div>