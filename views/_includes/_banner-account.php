<div class="featured-banner banner-empty banner-transparent banner-gradient">
	<div class="container-fluid">

		<div class="banner-link">
			<?php echo $tr->__('My Account'); ?>&nbsp; <a href="/assets/php/logout.php" class="btn btn-primary btn-sm"><?php echo $tr->__('Sign Out'); ?></a>
		</div>
		<br/>

	</div>
</div>