<div class="modal fade" id="account-modal-email">
<div class="modal-dialog" role="document">
<div class="modal-content">
	<div class="account-fields-email">

		<div class="modal-header">
			<h4 class="modal-title">
				<?php echo $tr->__('Change Email'); ?>
			</h4>
		</div>

		<div class="modal-body">

			<div class="alert alert-danger animated fadeIn"
				ng-if="editAccount.error"
				ng-cloak
				>
				{{editAccount.error}}
			</div>

			<div class="form-group">
				<label>
					<?php echo $tr->__('New Email Address'); ?>
				</label>
				<input type="text"
					class="form-control"
					ng-model="editAccount.email"
				/>
			</div>

		</div>

		<div class="modal-footer">

			<button type="button"
				class="btn btn-link"
				data-dismiss="modal"
				>
				<?php echo $tr->__('Cancel'); ?>
			</button>

			<button type="button"
				class="btn btn-primary"
				ng-click="accountChangeEmail($event)"
				>
				<?php echo $tr->__('Save'); ?>
			</button>

		</div>

	</div>
</div>
</div>
</div>


<div class="modal fade" id="account-modal-password">
<div class="modal-dialog" role="document">
<div class="modal-content">
	<div class="account-fields-password">

		<div class="modal-header">
			<h4 class="modal-title">
				<?php echo $tr->__('Change Password'); ?>
			</h4>
		</div>

		<div class="modal-body">

			<div class="alert alert-danger animated fadeIn"
				ng-if="editAccount.error"
				ng-cloak
				>
				{{editAccount.error}}
			</div>

			<div class="form-group">
				<label>
					<?php echo $tr->__('New Password'); ?>
				</label>
				<input type="password"
					class="form-control"
					ng-model="editAccount.password"
				/>
			</div>

			<div class="form-group">
				<label>
					<?php echo $tr->__('Confirm New Password'); ?>
				</label>
				<input type="password"
					class="form-control"
					ng-model="editAccount.passwordConfirm"
				/>
			</div>

		</div>

		<div class="modal-footer">

			<button type="button"
				class="btn btn-link"
				data-dismiss="modal"
				>
				<?php echo $tr->__('Cancel'); ?>
			</button>

			<button type="button"
				class="btn btn-primary"
				ng-click="accountChangePassword($event)"
				>
				<?php echo $tr->__('Save'); ?>
			</button>

		</div>

	</div>
</div>
</div>
</div>


<div class="modal fade" id="account-modal-card">
<div class="modal-dialog" role="document">
<div class="modal-content">
	<div class="account-fields-card">

		<div class="modal-header">
			<h4 class="modal-title">
				<?php echo $tr->__('Update Card'); ?>
			</h4>
		</div>

		<div class="modal-body">

			<div class="alert alert-danger animated fadeIn"
				ng-if="editAccount.error"
				ng-cloak
				>
				{{editAccount.error}}
			</div>

			<div class="form-group">
				<label>
					<?php echo $tr->__('Card Number'); ?>
				</label>
				<input type="text"
					class="form-control"
					size="20"
					maxlength="20"
					data-stripe="number"
					placeholder="{{user.card.brand}} ending in {{user.card.last4}}"
				/>
			</div>

			<div class="row">
				<div class="col-xs-7">

					<div class="form-group">
						<label>
							<?php echo $tr->__("Expiration Date"); ?>
						</label>
						<div class="row">
						<div class="col-xs-6">

								<input type="text"
									class="form-control"
									size="2"
									maxlength="2"
									data-stripe="exp-month"
									placeholder="MM"
									value="{{user.card.exp_month}}"
								/>

						</div>
						<div class="col-xs-6 p-l-0">

								<input type="text"
									class="form-control"
									size="4"
									maxlength="4"
									data-stripe="exp-year"
									placeholder="YYYY"
									value="{{user.card.exp_year}}"
								/>

						</div>
						</div>
					</div>

				</div>
				<div class="col-xs-5 p-l-0">
					<div class="form-group">

						<label>
							<?php echo $tr->__("Security Code"); ?>
						</label>

						<input type="text"
							class="form-control"
							size="4"
							maxlength="4"
							data-stripe="cvc"
							placeholder="CVC"
							value=""
						/>
					</div>
				</div>
			</div>

		</div>

		<div class="modal-footer">

			<button type="button"
				class="btn btn-link"
				data-dismiss="modal"
				>
				<?php echo $tr->__('Cancel'); ?>
			</button>

			<button type="button"
				class="btn btn-primary"
				ng-click="accountChangeCard($event)"
				>
				<?php echo $tr->__('Save'); ?>
			</button>

		</div>

	</div>
</div>
</div>
</div>


<div class="modal fade" id="account-modal-plan">
<div class="modal-dialog" role="document">
<div class="modal-content">
	<div class="account-fields-plan">

		<div class="modal-header">
			<h4 class="modal-title">
				<?php echo $tr->__('Change Plan'); ?>
			</h4>
		</div>

		<div class="modal-body">

			<div class="form-group m-b-0">

				<label>
					<?php echo $tr->__("Choose your Package"); ?>
				</label>

				<div class="btn-group-vertical btn-block btn-group-plan">
					<?php foreach( $stripe_plans as $sp ): ?>
            <label class="btn btn-lg btn-secondary-outline"
              ng-class="{'active': editAccount.plan.id == '<?php echo $sp->id; ?>'}"
              >
              <input type="radio"
                ng-model="editAccount.plan.id"
                value="<?php echo $sp->id; ?>"
                />
              <span class="price">
                <?php echo number_format(($sp->amount /100), 2, '.', ' '); ?>
              </span>
              <?php echo $sp->name; ?>
            </label>
          <?php endforeach; ?>
				</div>

				<!-- <div class="btn-group-vertical btn-block btn-group-plan"
					data-toggle="buttons"
					>
					<label class="btn btn-lg btn-secondary-outline"
						ng-class="{'active': editAccount.plan.id == 'daily'}"
						>
						<input type="radio"
							autocomplete="off"
							name="changePlan"
							ng-value="daily"
							ng-model="editAccount.plan.id"
						> <?php echo $tr->__("Daily"); ?>
						<span class="price">$2</span>
					</label>
					<label class="btn btn-lg btn-secondary-outline"
						ng-class="{'active': editAccount.plan.id == 'weekly'}"
						>
						<input type="radio"
							autocomplete="off"
							name="changePlan"
							ng-value="weekly"
							ng-model="editAccount.plan.id"
						> <?php echo $tr->__("Weekly"); ?>
						<span class="price">$10</span>
					</label>
					<label class="btn btn-lg btn-secondary-outline"
						ng-class="{'active': editAccount.plan.id == 'monthly'}"
						>
						<input type="radio"
							autocomplete="off"
							name="changePlan"
							ng-value="monthly"
							ng-model="editAccount.plan.id"
						> <?php echo $tr->__("Monthly"); ?>
						<span class="price">$30</span>
					</label>
				</div> -->

			</div>

		</div>

		<div class="modal-footer">

			<button type="button"
				class="btn btn-link"
				data-dismiss="modal"
				>
				<?php echo $tr->__('Cancel'); ?>
			</button>

			<button type="button"
				class="btn btn-primary"
				ng-click="accountChangePlan($event)"
				>
				<?php echo $tr->__('Save'); ?>
			</button>

		</div>

	</div>
</div>
</div>
</div>