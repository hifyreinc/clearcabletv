<div class="container-fluid">
<ul class="category-list list-unstyled gradient">
	<li>
		<a href="/account/"
			class="category-link
			<?php if($pagename == 'account-all'){ ?>
			active-tab
			<?php } ?>
			">
			<?php echo $tr->__('Account Settings'); ?>
		</a>
	</li>
	<li>
		<a href="/account/password/"
			class="category-link
			<?php if($pagename == 'account-password'){ ?>
			active-tab
			<?php } ?>
			">
			<?php echo $tr->__('Change Your Password'); ?>
		</a>
	</li>
	<li class="dropdown hidden-lg-up">
		<a href="" class="category-link dropdown-toggle center-text-xs"
			data-toggle="dropdown"
			>
			<span>
				<?php echo $tr->__('Account Settings'); ?>
			</span>
		</a>
		<div class="dropdown-menu p-t-0 p-b-0">
			<div class="dropdown-columns">
				<a href="/account/"
					class="dropdown-item"
					>
					<?php echo $tr->__('Account Settings'); ?>
				</a>
				<a href="/account/password/"
					class="dropdown-item"
					>
					<?php echo $tr->__('Change Your Password'); ?>
				</a>
			</div>
		</div>
	</li>
</ul>
</div>