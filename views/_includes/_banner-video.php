<?php

  // Check whether the user has liked the video before.
  $user = $_SESSION['userid'];
  $video = $_GET['videoId'];
  $userLike = getVideoLike($user, $video);
  $likeCount = getVideoLikeCount($video, 1);
  $dislikeCount = getVideoLikeCount($video, 0);
  $viewCount = getVideoViewCount($video);
  
?>

<div class="featured-banner banner-video">
  <div class="container-fluid w-100">
    <div class="row">
      <div class="col-sm-7"
        ng-cloak
        >
        <div class="video-wrapper"
          id="video-wrapper"
          data-video="<?php echo $video; ?>"
          data-user="<?php echo $user; ?>"
          >
        </div>
        <div class="row">
          <div class="col-xs-6">

            <a href="#" title="<?php echo $tr->__('Like this Video'); ?>"
              class="likeable <?php echo !empty($userLike) && $userLike[0]['liked'] == 1 ? 'liked' : ''; ?>"
              id="like"
              data-video="<?php echo $video; ?>"
              data-user="<?php echo $user; ?>"
              data-action="true"
              >
            </a>
            <span class="like-count"
              id="likeCount"
              >
              <?php echo $likeCount; ?>
            </span>
            <a href="#" title="<?php echo $tr->__('Dislike this Video'); ?>"
              class="likeable <?php echo !empty($userLike) && $userLike[0]['liked'] == 0 ? 'disliked' : ''; ?>"
              id="dislike"
              data-video="<?php echo $video; ?>"
              data-user="<?php echo $user; ?>"
              data-action="false"
              >
            </a>
            <span class="like-count"
              id="dislikeCount"
              >
              <?php echo $dislikeCount; ?>
            </span>
          </div>
          <div class="col-xs-6 text-xs-right">
            <span id="viewCount">
              <?php echo $viewCount; ?>
            </span>
            views
          </div>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="video-details"
          ng-cloak
          >
          <p class="video-title">
            <span ng-if="lang == 'en'">
              {{ video.name }}
            </span>
            <span ng-if="lang == 'fr'">
              {{ video.french_name }}
            </span>
            <span ng-if="lang == 'es'">
              {{ video.spanish_name }}
            </span>
          </p>
          <p class="video-category">
            <span ng-repeat="category in categories">
              {{ lang == 'en' ? category.name + (!$last ? ', ' : '') : '' }}
              {{ lang == 'fr' ? category.nameFr + (!$last ? ', ' : '') : '' }}
              {{ lang == 'es' ? category.nameEs  + (!$last ? ', ' : ''): '' }}
            </span>
          </p>
          <p class="video-description">
            <span ng-if="lang == 'en'">
              {{ video.description }}
            </span>
            <span ng-if="lang == 'fr'">
              {{ video.french_description }}
            </span>
            <span ng-if="lang == 'es'">
              {{ video.spanish_description }}
            </span>
          </p>
          <?php
            // Compile the current url for social media sharing
            $pageURL = 'http'
              . (isset($_SERVER['HTTPS']) ? 's' : '')
              . '://' . $_SERVER['HTTP_HOST']
              . '/'
              . $_SERVER['REQUEST_URI'];
          ?>

           <style>
            .stArrow,
            .stHBubble,
            .stMainServices {
              height: 22px !important;
            }
          </style>
 
          <span class='st_facebook_hcount' displayText='Facebook'></span>
          <span class='st_twitter_hcount' displayText='Tweet' st_title='<?php echo $settings[7]['value']; ?>'></span>

        </div>
      </div>
    </div>
  </div>
</div>