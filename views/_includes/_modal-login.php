<div ng-controller="LoginController">

<div class="modal fade" id="login-modal">
<div class="modal-dialog" role="document">
<div class="modal-content">

	<div class="login-fields">

		<div class="modal-header">
			<h4 class="modal-title">
				<?php echo $tr->__('Sign In to '. $settings[0]['value']); ?>
			</h4>
		</div>

		<div class="modal-body">

			<div class="alert alert-danger animated fadeIn"
				ng-if="loginAccount.error"
				ng-cloak
				>
				{{loginAccount.error}}
			</div>

			<div class="form-group">
				<label>
					<?php echo $tr->__('Client Number or Email Address'); ?>
				</label>
				<input type="text"
					class="form-control"
					ng-model="loginAccount.email"
					tabindex="1"
					ng-keydown="$event.which === 13 &amp;&amp; accountLogin($event)"
				/>
			</div>

			<div class="form-group">
				<label class="center-block">
					<?php echo $tr->__('Password'); ?>
					<a href="" class="pull-right">
						<?php echo $tr->__("forgot?"); ?>
					</a>
				</label>
				<input type="password"
					class="form-control"
					ng-model="loginAccount.password"
					tabindex="2"
					ng-keydown="$event.which === 13 &amp;&amp; accountLogin($event)"
				/>
			</div>

			<div class="form-group m-b-0">
				<label>
					<?php echo $tr->__("Don't have an account?"); ?>
					<a href="#signup-modal"
						data-swapmodal="modal"
						>
						<?php echo $tr->__('Sign Up'); ?>
					</a>
				</label>
			</div>

		</div>

		<div class="modal-footer">

			<button type="button"
				class="btn btn-link"
				data-dismiss="modal"
				>
				<?php echo $tr->__('Cancel'); ?>
			</button>

			<button type="button"
				class="btn btn-primary"
				ng-click="accountLogin($event)"
				tabindex="3"
				>
				<?php echo $tr->__('Sign In'); ?>
			</button>

		</div>

	</div>

</div>
</div>
</div>