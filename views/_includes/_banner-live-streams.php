<div class="featured-banner <?php echo !isset($_SESSION['secure']) ? 'banner-loggedout' : ''; ?>"
  ng-class="{
    'banner-empty': !featuredVideo
  }"
  ng-style="{
    'background-image': featuredVideo ? 'url(/uploads/images/' + featuredVideo.thumbnail + ')' : ''
  }"
  >
  <div class="container-fluid">

    <div class="banner-link"
      ng-if="featuredVideo !== null"
      ng-cloak
      >
      <?php
      // Toggle Content based on whether they're logged in or out.
      if(!isset($_SESSION['secure'])):
      ?>

      <span>
        <?php echo $tr->__($settings[5]['value']); ?>
      </span>
        <?php
        // Toggle button based on whether they've been logged in before.
        if(isset($_COOKIE['returningVisit']) && $_COOKIE['returningVisit']):
        ?>
        <a class="btn btn-primary btn-sm"
          href="#login-modal"
          data-toggle="modal"
          >
          <?php echo $tr->__('Sign In'); ?>
        </a>
        <?php else: ?>
        <a class="btn btn-primary btn-sm"
          ng-attr-href="{{featuredVideo.private == 0 ? '/live-streams/video/?videoId='+featuredVideo.id : '#signup-modal'}}"
          ng-attr-data-toggle="{{featuredVideo.private == 0 ? '' : 'modal'}}"
          >
          <?php echo $tr->__('Sign Up'); ?>
        </a>
        <?php endif; ?>
      <?php else: ?>
      <span>{{::featuredVideo.name}}</span>
      <?php echo '&nbsp'; ?>
      <?php echo $tr->__('Streaming Live');?>
      <a href="/live-streams/video/?videoId={{::featuredVideo.id}}" class="btn btn-primary btn-sm">
        <?php echo $tr->__('Watch Now'); ?>
      </a>
      <?php endif; ?>
    </div>

  </div>
</div>