<div ng-controller="SignupController">
  <div class="modal fade"
    id="signup-modal"
    >
    <div class="modal-dialog"
      role="document"
      >
      <div class="modal-content">
        <div class="signup-fields">

          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo $tr->__('Create an Account'); ?>
            </h4>
            <p class="text-muted m-b-0">
              <?php echo $tr->__('Daily, Weekly, or Monthly plans available'); ?>
            </p>
          </div>

          <!-- Step 1 -->
          <div class="step"
            ng-if="createAccount.step == 1"
            >

            <div class="modal-body">

              <div class="alert alert-danger animated fadeIn"
                ng-if="createAccount.error"
                ng-cloak
                >
                {{ createAccount.error }}
              </div>

              <input hidden />
              <input type="password" hidden />

              <div class="form-group">
                <label>
                  <?php echo $tr->__('Email Address'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="createAccount.email"
                  />
              </div>

              <div class="form-group">
                <label>
                  <?php echo $tr->__('Password'); ?>
                </label>
                <input type="password"
                  class="form-control"
                  ng-model="createAccount.password"
                  />
              </div>

              <div class="form-group m-b-0">
                <label>
                  <?php echo $tr->__("Already have an account?"); ?>
                  <a href="#login-modal"
                    data-swapmodal="modal"
                    >
                    <?php echo $tr->__('Sign In'); ?>
                  </a>
                </label>
              </div>

            </div>

            <div class="modal-footer">

              <button type="button"
                class="btn btn-link"
                data-dismiss="modal"
                >
                <?php echo $tr->__('Cancel'); ?>
              </button>

              <button type="button"
                class="btn btn-primary"
                ng-click="<?php echo $settings[2]['value'] === '2' ? 'accountStep(2)' : 'accountCreate($event)'; ?>"
                >
                <?php echo $settings[2]['value'] === '2' ? $tr->__('Next') : $tr->__('Save'); ?>
              </button>

            </div>

          </div>

          <!-- Step 2 -->
          <div class="step"
            ng-if="createAccount.step == 2"
            >
            <div class="modal-body">
              <div class="alert alert-danger animated fadeIn"
                ng-if="createAccount.error"
                ng-cloak
                >
                {{ createAccount.error }}
              </div>

              <div class="form-group m-b-0">
                <label>
                  <?php echo $tr->__("Choose your Package"); ?>
                </label>
                <div class="btn-group-vertical btn-block btn-group-plan">
                  <?php foreach( $stripe_plans as $sp ): ?>
                    <label class="btn btn-lg btn-secondary-outline"
                      ng-class="{'active': createAccount.plan.id == '<?php echo $sp->id; ?>'}"
                      >
                      <input type="radio"
                        ng-model="createAccount.plan.id"
                        value="<?php echo $sp->id; ?>"
                        />
                      <span class="price">
                        <?php echo '$' . number_format(($sp->amount /100), 2, '.', ','); ?>
                      </span>
                      <?php echo $sp->name; ?>
                    </label>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button"
                class="btn btn-secondary pull-left"
                ng-click="accountStep(1)"
                >
                <?php echo $tr->__('Back'); ?>
              </button>

              <button type="button"
                class="btn btn-link"
                data-dismiss="modal"
                >
                <?php echo $tr->__('Cancel'); ?>
              </button>

              <button type="button"
                class="btn btn-primary"
                ng-click="accountStep(3)"
                >
                <?php echo $tr->__('Next'); ?>
              </button>
            </div>
          </div>

          <!-- Step 3 -->
          <div class="step"
            ng-if="createAccount.step == 3"
            >
            <div class="modal-body">
              <div class="alert alert-danger animated fadeIn"
                ng-if="createAccount.error"
                ng-cloak
                >
                {{ createAccount.error }}
              </div>
              <div class="form-group">
                <label>
                  <?php echo $tr->__("Card Number"); ?>
                </label>
                <input type="text"
                  class="form-control"
                  size="16"
                  maxlength="16"
                  data-stripe="number"
                  placeholder="----------------"
                  ng-model="createAccount.stripe.card"
                />
              </div>
              <div class="row">
                <div class="col-xs-7">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__("Expiration Date"); ?>
                    </label>
                    <div class="row">
                      <div class="col-xs-6">
                        <input type="text"
                          class="form-control"
                          size="2"
                          maxlength="2"
                          data-stripe="exp-month"
                          placeholder="MM"
                          ng-model="createAccount.stripe.exp_month"
                          />
                      </div>
                      <div class="col-xs-6 p-l-0">
                        <input type="text"
                          class="form-control"
                          size="4"
                          maxlength="4"
                          data-stripe="exp-year"
                          placeholder="YYYY"
                          ng-model="createAccount.stripe.exp_year"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-5 p-l-0">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__("Security Code"); ?>
                    </label>
                    <input type="text"
                      class="form-control"
                      size="4"
                      maxlength="4"
                      data-stripe="cvc"
                      placeholder="CVC"
                      ng-model="createAccount.stripe.cvc"
                      />
                  </div>
                </div>
              </div>
              <div class="form-group clearfix m-b-0">
                <img src="/build/img/powered-by-stripe.png"
                  class="img-responsive pull-right"
                  width="95"
                  height="21"
                  alt="Powered by Stripe"
                  />
                <label class="text-muted small">
                  <i class="fa fa-lock"></i>&nbsp; <?php echo $tr->__("Secure Server"); ?>
                </label>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button"
                class="btn btn-secondary pull-left"
                ng-click="accountStep(2)"
                >
                <?php echo $tr->__('Back'); ?>
              </button>
              <button type="button"
                class="btn btn-link"
                data-dismiss="modal"
                >
                <?php echo $tr->__('Cancel'); ?>
              </button>
              <button type="button"
                class="btn btn-primary"
                ng-click="accountCreate($event)"
                >
                <?php echo $tr->__('Save'); ?>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>