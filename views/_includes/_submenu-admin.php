<div class="container-fluid">
<ul class="category-list list-unstyled gradient">
  <li>
    <a href="/admin/live-streams/"
      class="category-link
      <?php if($pagesubtype == 'live-streams'){ ?>
      active-tab
      <?php } ?>
      ">
      <?php echo $tr->__('Live Streams'); ?>
    </a>
  </li>
  <li>
    <a href="/admin/video-on-demand/"
      class="category-link
      <?php if($pagesubtype == 'video-on-demand'){ ?>
      active-tab
      <?php } ?>
      ">
      <?php echo $tr->__('On Demand'); ?>
    </a>
  </li>
  <li>
    <a href="/admin/advertisements/"
      class="category-link
      <?php if($pagesubtype == 'advertisements'){ ?>
      active-tab
      <?php } ?>
      ">
      <?php echo $tr->__('Advertisements'); ?>
    </a>
  </li>
  <li>
    <a href="/admin/go-services/"
      class="category-link
      <?php if($pagesubtype == 'go-services'){ ?>
      active-tab
      <?php } ?>
      ">
      <?php echo $tr->__('Go Services'); ?>
    </a>
  </li>
  <li>
    <a href="/admin/faq/"
      class="category-link
      <?php if($pagesubtype == 'faq'){ ?>
      active-tab
      <?php } ?>
      ">
      <?php echo $tr->__('FAQ'); ?>
    </a>
  </li>
  <li>
    <a href="/admin/settings/"
      class="category-link
      <?php if($pagesubtype == 'settings'){ ?>
      active-tab
      <?php } ?>
      ">
      <?php echo $tr->__('Settings'); ?>
    </a>
  </li>
  <li class="dropdown hidden-lg-up">
    <a href="" class="category-link dropdown-toggle center-text-xs"
      data-toggle="dropdown"
      >
      <span>
        <?php if($pagesubtype == 'live-streams'){ ?>
        <?php echo $tr->__('Live Streams'); ?>
        <?php } ?>
        <?php if($pagesubtype == 'video-on-demand'){ ?>
        <?php echo $tr->__('On Demand'); ?>
        <?php } ?>
        <?php if($pagesubtype == 'advertisements'){ ?>
        <?php echo $tr->__('Advertisements'); ?>
        <?php } ?>
        <?php if($pagesubtype == 'faq'){ ?>
        <?php echo $tr->__('FAQ'); ?>
        <?php } ?>
        <?php if($pagesubtype == 'faq'){ ?>
        <?php echo $tr->__('Settings'); ?>
        <?php } ?>
      </span>
    </a>
    <div class="dropdown-menu p-t-0 p-b-0">
      <div class="dropdown-columns">
        <a href="/admin/live-streams/"
          class="dropdown-item
          <?php if($pagesubtype == 'live-streams'){ ?>
          active
          <?php } ?>"
          >
          <?php echo $tr->__('Live Streams'); ?>
        </a>
        <a href="/admin/video-on-demand/"
          class="dropdown-item
          <?php if($pagesubtype == 'video-on-demand'){ ?>
          active
          <?php } ?>"
          >
          <?php echo $tr->__('On Demand'); ?>
        </a>
        <a href="/admin/advertisements/"
          class="dropdown-item
          <?php if($pagesubtype == 'advertisements'){ ?>
          active
          <?php } ?>"
          >
          <?php echo $tr->__('Advertisements'); ?>
        </a>
        <a href="/admin/faq/"
          class="dropdown-item
          <?php if($pagesubtype == 'faq'){ ?>
          active
          <?php } ?>"
          >
          <?php echo $tr->__('FAQ'); ?>
        </a>
        <a href="/admin/settings/"
          class="dropdown-item
          <?php if($pagesubtype == 'settings'){ ?>
          active
          <?php } ?>"
          >
          <?php echo $tr->__('Settings'); ?>
        </a>
      </div>
    </div>
  </li>
</ul>
</div>