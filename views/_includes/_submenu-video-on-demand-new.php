<ul class="category-list list-unstyled gradient">

  <?php
    // Set Back button
    if (in_array($GLOBALS['pagename'], array("video-on-demand-category"))) {
      $url = '/video-on-demand/';
    } elseif (in_array($GLOBALS['pagename'], array("video-on-demand-video"))) {
      $url = '/video-on-demand/category/?categoryId={{ video.section }}';
    }
  ?>
  <li ng-cloak>
    <a href="<?php echo $url; ?>"
      class="category-link back-link hidden-lg-up"
      >
      <?php echo $tr->__('Back'); ?>
    </a>
  </li>

  <!-- catch-all link -->
  <li ng-cloak>
    <a href="/video-on-demand/"
      class="category-link"
      ng-class="{ 'active-tab': currentPageType == 'video-on-demand-all' }"
      >
      <span>
        <?php echo $tr->__('All Videos'); ?>
      </span>
    </a>
  </li>

  <!-- loop through categories -->
  <li ng-repeat="stream in streamSections"
    ng-cloak
    >
    <a href="/video-on-demand/category/?categoryId={{ stream.id}}"
      class="category-link"
      ng-class="{ 'active-tab': browsingCategoryId == stream.id }"
      >
      <span ng-if="lang == 'en'">
        {{ stream.name }}
      </span>
      <span ng-if="lang == 'fr'">
        {{ stream.french_name }}
      </span>
      <span ng-if="lang == 'es'">
        {{ stream.spanish_name }}
      </span>
    </a>
  </li>

  <li class="dropdown">
    <a href="" class="category-link dropdown-toggle
      <?php if(!in_array($GLOBALS['pagename'], array("video-on-demand-category","video-on-demand-video"))) { ?>
      center-text-xs
      <?php } ?>"
      data-toggle="dropdown"
      ng-class="{'hidden-lg-up': streamSections.length < 8}"
      ng-cloak
      >
      <span class="hidden-md-down"
        ng-if="streamSections.length > 7"
        ng-cloak
        >
        <?php echo $tr->__('More'); ?>
      </span>
      <span class="hidden-lg-up"
        ng-if="!streamName"
        ng-cloak
        >
        <?php echo $tr->__('All Stations'); ?>
      </span>
      <span class="hidden-lg-up"
        ng-if="streamName"
        ng-cloak
        >
        {{ streamName }}
      </span>
    </a>
    <div class="dropdown-menu p-t-0 p-b-0">
      <div class="dropdown-columns"
        ng-class="{
          'column-count-2': streamSections.length > 5,
          'column-count-3': streamSections.length > 10
        }"
        >
        <a href="/live-streams/category/?categoryId={{stream.id}}"
          class="dropdown-item"
          ng-repeat="stream in streamSections"
          ng-class="{'active': categoryId == stream.id}"
          >
          <span ng-if="lang == 'en'">
            {{ stream.name }}
          </span>
          <span ng-if="lang == 'fr'">
            {{ stream.french_name }}
          </span>
          <span ng-if="lang == 'es'">
            {{ stream.spanish_name }}
          </span>
        </a>
      </div>
    </div>
  </li>

</ul>