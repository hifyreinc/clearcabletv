<header ng-controller="HeaderController">

  <a href=""
    class="header-link bars-menu-button hidden-lg-up"
    data-test="{{categoryId}}"
    >
    <span>
      <i class="line-1"></i>
      <i class="line-2"></i>
      <i class="line-3"></i>
    </span>
    <i class="sr-only">
      <?php echo $tr->__('Open Menu'); ?>
    </i>
  </a>

  <a href="/"
    class="header-link header-logo"
    >
    <span class="animated fadeIn">
      <i class="sr-only">
        <?php echo $settings[0]['value']; ?>
      </i>
    </span>
  </a>

  <div class="dropdown hidden-md-down">
    <a href=""
      class="header-link dropdown-toggle"
      data-toggle="dropdown"
      ng-class="{'active-page': currentPage == 'live-streams'}"
      >
      <span>
        <?php echo $tr->__('Live Streams'); ?>
      </span>
    </a>
    <div class="dropdown-menu p-b-0">
      <a href="/live-streams/"
        class="dropdown-item
        <?php echo $GLOBALS['pagename'] == 'live-streams-all' ? 'active' : ''; ?>"
        >
        <?php echo $tr->__('All Live Streams'); ?>
      </a>
      <div class="dropdown-divider m-b-0"></div>
      <div class="dropdown-columns"
        ng-class="{
          'column-count-2': liveStreams.length > 5,
          'column-count-3': liveStreams.length > 10
        }"
        >
        <a href="/live-streams/category/?categoryId={{section.id}}"
          class="dropdown-item"
          ng-class="{'active': section.id == categoryId}"
          ng-repeat="section in liveStreams"
          >
          <span ng-if="lang == 'en'">
            {{section.name}}
          </span>
          <span ng-if="lang == 'fr'">
            {{section.french_name}}
          </span>
          <span ng-if="lang == 'es'">
            {{section.spanish_name}}
          </span>
        </a>
      </div>
    </div>
  </div>

  <div class="dropdown hidden-md-down">
    <a href=""
      class="header-link dropdown-toggle"
      data-toggle="dropdown"
      ng-class="{'active-page': currentPage == 'video-on-demand'}"
      >
      <span>
        <?php echo $tr->__('On Demand'); ?>
      </span>
    </a>
    <div class="dropdown-menu p-b-0">
      <a href="/video-on-demand/"
        class="dropdown-item
        <?php echo $GLOBALS['pagename'] == 'video-on-demand-all' ? 'active' : ''; ?>"
        >
        <?php echo $tr->__('All Videos'); ?>
      </a>
      <div class="dropdown-divider m-b-0"></div>
      <div class="dropdown-columns"
        ng-class="{
          'column-count-2': videoSections.length > 5,
          'column-count-3': videoSections.length > 10
        }"
        >
        <a href="/video-on-demand/category/?categoryId={{section.id}}"
          class="dropdown-item"
          ng-class="{'active': section.id == categoryId}"
          ng-repeat="section in videoSections"
          >
          <span ng-if="lang == 'en'">
            {{section.name}}
          </span>
          <span ng-if="lang == 'fr'">
            {{section.french_name}}
          </span>
          <span ng-if="lang == 'es'">
            {{section.spanish_name}}
          </span>
        </a>
      </div>
    </div>
  </div>

  <div class="dropdown hidden-md-down"
    ng-if="goServices.length > 0"
    ng-cloak
    >
    <a href=""
      class="header-link dropdown-toggle"
      data-toggle="dropdown"
      ng-class="{'active-page': currentPage == 'go-services'}"
      >
      <span>
        <?php echo $tr->__('Go Services'); ?>
      </span>
    </a>
    <div class="dropdown-menu p-b-0">
      <a href="/go-services/"
        class="dropdown-item
        <?php echo $GLOBALS['pagename'] == 'go-services-all' ? 'active' : ''; ?>"
        >
        <?php echo $tr->__('All Go Services'); ?>
      </a>
      <div class="dropdown-divider m-b-0"></div>
      <div class="dropdown-columns"
        ng-class="{
          'column-count-2': goServices.length > 5,
          'column-count-3': goServices.length > 10
        }"
        >
        <?php if( !isset($_SESSION['secure']) || !$_SESSION['secure']): ?>
          <?php
            $modal = isset($_COOKIE['returningVisit']) && $_COOKIE['returningVisit']
              ? '#login-modal' : '#signup-modal';
          ?>
        <a class="dropdown-item"
          ng-attr-href="{{service.private == 0 ? service.url : '<?php echo $modal; ?>'}}"
          ng-attr-data-toggle="{{service.private == 0 ? '' : 'modal'}}"
          target="{{service.private == 0 ? '_blank' : '_self'}}"
          ng-repeat="service in goServices"
          >
          <span ng-if="lang == 'en'">
            {{service.name}}
          </span>
          <span ng-if="lang == 'fr'">
            {{service.french_name}}
          </span>
          <span ng-if="lang == 'es'">
            {{service.spanish_name}}
          </span>
        </a>
        <?php else: ?>
        <a class="dropdown-item"
          href="{{service.url}}"
          target="_blank"
          ng-repeat="service in goServices"
          >
          <span ng-if="lang == 'en'">
            {{service.name}}
          </span>
          <span ng-if="lang == 'fr'">
            {{service.french_name}}
          </span>
          <span ng-if="lang == 'es'">
            {{service.spanish_name}}
          </span>
        </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="dropdown hidden-md-down">
    <a href="//<?php echo $_SERVER['HTTP_HOST']; ?>/guide?lang=<?php echo $_SESSION['lang']; ?>"
      class="header-link dropdown-toggle no-carrot"
      ng-class="{'active-page': currentPage == 'guides'}"
      >
      <span>
        <?php echo $tr->__('Guide'); ?>
      </span>
    </a>
  </div>

  <div class="header-link search-link hidden-md-down">

    <?php include($GLOBALS['dir'].'views/_includes/_search.php'); ?>

  </div>

  <div class="dropdown link-right">
    <a href=""
      class="header-link dropdown-toggle hidden-md-down"
      data-toggle="dropdown"
      >
      <span>
        <small>
          <?php echo $tr->__('Welcome'); ?>,
        </small>
        <br/>
        <?php echo isset($_SESSION['secure']) ? $_SESSION['email'] : $tr->__('Please Sign In'); ?>
      </span>
    </a>
    <a href=""
      class="header-link dots-menu-button hidden-lg-up"
      data-toggle="dropdown"
      >
      <span>
        <i class="line-1"></i>
        <i class="line-2"></i>
        <i class="line-3"></i>
      </span>
      <i class="sr-only">
        <?php echo $tr->__('Menu'); ?>
      </i>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
      
      <!-- dynamic link now -->
      <?php if(isset($settings)): ?>
      <a href="<?php echo $settings[1]['value']; ?>"
        class="dropdown-item"
        title="<?php echo $settings[0]['value']; ?>"
        >
        <?php echo $settings[0]['value']; ?>
      </a>
      <?php endif; ?>

      <?php if(in_array('en', $enabled_languages)): ?>
        <a href="<?php echo switchlangurl('en'); ?>"
          class="dropdown-item"
          >
          English
        </a>
      <?php endif; ?>

      <?php if(in_array('fr', $enabled_languages)): ?>
        <a href="<?php echo switchlangurl('fr'); ?>"
          class="dropdown-item"
          >
          Fran&ccedil;ais
        </a>
      <?php endif; ?>

      <?php if (in_array('es', $enabled_languages)): ?>
        <a href="<?php echo switchlangurl('es'); ?>"
          class="dropdown-item"
          >
          Espa&ntilde;ol
        </a>
      <?php endif; ?>

      <?php if (isset($_SESSION['role']) && $_SESSION['role'] == 'admin'): ?>
        <a href="/admin/"
          class="dropdown-item"
          >
          <?php echo $tr->__('Admin'); ?>
        </a>
      <?php endif; ?>

      <?php if(isset($_SESSION['secure'])): ?>
        <a href="/account/"
          class="dropdown-item"
          >
          <?php echo $tr->__('My Account'); ?>
        </a>
      <?php endif; ?>

      <a href="/faq/" class="dropdown-item">
        <?php echo $tr->__('FAQ'); ?>
      </a>

      <?php if(!isset($_SESSION['secure'])): ?>
        <a href="#login-modal"
          data-toggle="modal"
          class="dropdown-item"
          >
          <?php echo $tr->__('Sign In'); ?>
        </a>
        <a href="#signup-modal"
          data-toggle="modal"
          class="dropdown-item"
          >
          <?php echo $tr->__('Sign Up'); ?>
        </a>
      <?php else: ?>
        <a href="/assets/php/logout.php"
          class="dropdown-item"
          >
          <?php echo $tr->__('Sign Out'); ?>
        </a>
      <?php endif; ?>
      
    </div>
  </div>

</header>
