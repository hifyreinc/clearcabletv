<div class="featured-banner <?php if(!isset($_SESSION['secure'])) { ?>banner-loggedout<?php } ?>"
	ng-class="{
		'banner-empty': !featuredVideo
	}"
	ng-style="{
		'background-image': featuredVideo ? 'url(/uploads/images/' + featuredVideo.thumbnail + ')' : ''
	}"
	>
	<div class="container-fluid">

		<div class="banner-link"
			ng-if="featuredVideo !== null"
			ng-cloak
			>
			<!-- <img src="<?php echo $GLOBALS['dir']; ?>assets/img/logos/oln.png" alt="OLN" class="banner-logo img-responsive" /> -->
			<span>{{::featuredVideo.name}}</span>
			<?php echo '&nbsp'; ?>
			<?php if(!isset($_SESSION['secure'])) { ?>
			<a class="btn btn-primary btn-sm"
				ng-attr-href="{{featuredVideo.private == 0 ? '/video-on-demand/video/?videoId='+featuredVideo.id : '#signup-modal'}}"
				ng-attr-data-toggle="{{featuredVideo.private == 0 ? '' : 'modal'}}"
				data-govideo="{{featuredVideo.id}}"
				>
				<?php echo $tr->__('Sign Up'); ?>
			</a>
			<?php } else { ?>
			<a href="/video-on-demand/video/?videoId={{::featuredVideo.id}}" class="btn btn-primary btn-sm">
				<?php echo $tr->__('Watch Now'); ?>
			</a>
			<?php } ?>
		</div>

	</div>
</div>