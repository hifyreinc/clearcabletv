<nav ng-controller="HeaderController">

  <a href="" class="header-link bars-menu-button hidden-lg-up">
    <span>
      <i class="line-1"></i>
      <i class="line-2"></i>
      <i class="line-3"></i>
    </span>
    <i class="sr-only"><?php echo $tr->__('Close Menu'); ?></i>
  </a>

  <div class="side-nav-header">

    <?php include($GLOBALS['dir'].'views/_includes/_search.php'); ?>

  </div>

  <div class="swiper-container swiper-side-nav"
    ng-cloak
    >
  <div class="swiper-wrapper">
  <div class="swiper-slide">

  <div class="side-nav-scroll-list">

    <ul class="list-unstyled">

      <li>
        <div class="dropdown"
          ng-class="{'open': currentPage == 'live-streams'}"
          >
          <a href="" class="side-nav-link dropdown-toggle"
            ng-class="{'active-page': currentPage == 'live-streams'}"
            >
            <?php echo $tr->__('Live Streams'); ?>
          </a>
          <div class="dropdown-menu p-b-0">
            <ul class="list-unstyled">
              <li>
                <a href="/live-streams/"
                  class="side-nav-link
                  <?php if($GLOBALS['pagename'] == 'live-streams-all'){ ?>
                  active-page
                  <?php } ?>"
                  >
                  <span><i class="fa fa-angle-double-right"></i> <?php echo $tr->__('All Live Streams'); ?></span>
                </a>
              </li>
              <li>
                <a href="/live-streams/category/?categoryId={{section.id}}"
                  class="side-nav-link"
                  ng-class="{'active-page': section.id == categoryId}"
                  ng-repeat="section in streamSections"
                  >
                  <span ng-if="lang == 'en'">
                    <i class="fa fa-angle-double-right"></i> {{section.name}}
                  </span>
                  <span ng-if="lang == 'fr'">
                    <i class="fa fa-angle-double-right"></i> {{section.french_name}}
                  </span>
                  <span ng-if="lang == 'es'">
                    <i class="fa fa-angle-double-right"></i> {{section.spanish_name}}
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </li>

      <li>
        <div class="dropdown"
          ng-class="{'open': currentPage == 'video-on-demand'}"
          >
          <a href="" class="side-nav-link dropdown-toggle"
            ng-class="{'active-page': currentPage == 'video-on-demand'}"
            >
            <?php echo $tr->__('On Demand'); ?>
          </a>
          <div class="dropdown-menu p-b-0">
            <ul class="list-unstyled">
              <li>
                <a href="/video-on-demand/"
                  class="side-nav-link
                  <?php if($GLOBALS['pagename'] == 'video-on-demand-all'){ ?>
                  active-page
                  <?php } ?>"
                  >
                  <span><i class="fa fa-angle-double-right"></i> <?php echo $tr->__('All Videos'); ?></span>
                </a>
              </li>
              <li>
                <a href="/video-on-demand/category/?categoryId={{section.id}}"
                  class="side-nav-link"
                  ng-class="{'active-page': section.id == categoryId}"
                  ng-repeat="section in videoSections"
                  >
                  <span ng-if="lang == 'en'">
                    <i class="fa fa-angle-double-right"></i> {{section.name}}
                  </span>
                  <span ng-if="lang == 'fr'">
                    <i class="fa fa-angle-double-right"></i> {{section.french_name}}
                  </span>
                  <span ng-if="lang == 'es'">
                    <i class="fa fa-angle-double-right"></i> {{section.spanish_name}}
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </li>

      <li>
        <div class="dropdown"
          ng-if="goServices.length > 0"
          ng-class="{'open': currentPage == 'go-services'}"
          ng-cloak
          >
          <a href=""
            class="side-nav-link dropdown-toggle"
            ng-class="{'active-page': currentPage == 'go-services'}"
            >
            <span><?php echo $tr->__('Go Services'); ?></span>
          </a>
          <div class="dropdown-menu p-b-0">
            <ul class="list-unstyled">
              <li>
                <a href="/go-services/"
                  class="side-nav-link
                  <?php if($GLOBALS['pagename'] == 'go-services-all'){ ?>
                  active-page
                  <?php } ?>"
                  >
                  <span><i class="fa fa-angle-double-right"></i> <?php echo $tr->__('All Go Services'); ?></span>
                </a>
              </li>
              <li ng-repeat="service in goServices">
                <?php if(isset($_SESSION['secure']) && !$_SESSION['secure']) { ?>
                <a class="side-nav-link"
                  ng-attr-href="{{service.private == 0 ? service.url : '#signup-modal'}}"
                  ng-attr-data-toggle="{{service.private == 0 ? '' : 'modal'}}"
                  target="{{service.private == 0 ? '_blank' : '_self'}}"
                  >
                  <span ng-if="lang == 'en'">
                    <i class="fa fa-angle-double-right"></i> {{service.name}}
                  </span>
                  <span ng-if="lang == 'fr'">
                    <i class="fa fa-angle-double-right"></i> {{service.french_name}}
                  </span>
                  <span ng-if="lang == 'es'">
                    <i class="fa fa-angle-double-right"></i> {{service.spanish_name}}
                  </span>
                </a>
                <?php } else { ?>
                <a class="side-nav-link"
                  href="{{service.url}}"
                  target="_blank"
                  >
                  <span ng-if="lang == 'en'">
                    <i class="fa fa-angle-double-right"></i> {{service.name}}
                  </span>
                  <span ng-if="lang == 'fr'">
                    <i class="fa fa-angle-double-right"></i> {{service.french_name}}
                  </span>
                  <span ng-if="lang == 'es'">
                    <i class="fa fa-angle-double-right"></i> {{service.spanish_name}}
                  </span>
                </a>
                <?php } ?>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <!-- Guide --> 
      <li>
        <div 
          ng-class="{'open': currentPage == 'guide'}"
          ng-cloak
          >
          <a 
            href="/guide?lang=<?php echo $_SESSION['lang']  ?>"
            class="side-nav-link"
            ng-class="{'active-page': currentPage == 'guide'}"
            >
            <span><?php echo $tr->__('Guide'); ?></span>
          </a>
      </li>
    </ul>
  </div>

  </div>
  </div>
  </div>

</nav>

<div class="side-nav-cover">
  <div class="swiper-container swiper-side-nav-cover">
  <div class="swiper-wrapper">
  <div class="swiper-slide">

  </div>
  </div>
  </div>
</div>

<div class="main-page-container page-type-<?php echo $GLOBALS['pagetype']; ?> page-<?php echo $GLOBALS['pagename']; ?>">
<div class="main-page">