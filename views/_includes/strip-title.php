<!-- channel name -->
<div class="container-fluid <?php echo isset($wrapperClass) && !empty($wrapperClass) ? $wrapperClass : ''; ?>">
  <h2 class="h5 with-btn">
    <?php echo $title; ?>
    
    <?php if(isset($seeAll) && $seeAll == true): ?>
    <a href="<?php echo $seeAll; ?>">
      <?php echo $tr->__('See All'); ?>
    </a>
    
    <?php endif; ?>
  </h2>
</div>