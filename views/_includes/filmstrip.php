<!-- filmstrip -->
<div class="swiper-container swiper-scroll-horz">
  <div class="swiper-wrapper">
    <div class="swiper-slide">
      <div class="horz-scroll-list container-fluid">
        <div class="row row-alt">

          <?php require(dirname(__FILE__) . '/video-thumb.php'); ?>

        </div>
      </div>
    </div>
  </div>

  <!-- scroll arrows -->
  <div class="hover-div gradient scroll-left">
    <i class="fa fa-caret-left"></i>
  </div>
  <div class="hover-div gradient scroll-right">
    <i class="fa fa-caret-right"></i>
  </div>

</div>