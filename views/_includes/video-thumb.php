<!-- thumbnail -->
<?php if(!isset($_SESSION['secure'])) : ?>
  <?php
    $modal = isset($_COOKIE['returningVisit']) && $_COOKIE['returningVisit']
      ? '#login-modal' : '#signup-modal';
  ?>
<a class="video-block"
  ng-attr-href="{{ video.private == 0 ? '/<?php echo $section; ?>/video/?videoId='+video.id : '<?php echo $modal; ?>' }}"
  ng-attr-data-toggle="{{video.private == 0 ? '' : 'modal'}}"
  data-govideo="{{ video.id }}"
  ng-repeat="<?php echo $loop['index']; ?> in <?php echo $loop['array']; ?>"
  >
<?php else : ?>
<a class="video-block"
  href="/<?php echo $section; ?>/video/?videoId={{ <?php echo $loop['index']; ?>.id }}"
  ng-repeat="<?php echo $loop['index']; ?> in <?php echo $loop['array']; ?>"
  >
<?php endif; ?>
  <div class="video-thumb">
    <div class="thumbnail-image"
      ng-if="<?php echo $loop['index']; ?>.thumbnail != ''"
      ng-style="{ 'backgroundImage': 'url(/uploads/images/{{<?php echo $loop['index']; ?>.thumbnail}}?m={{<?php echo $loop['index']; ?>.filemtime}})' }"
      >
    </div>
    <div class="thumbnail-image"
      ng-if="<?php echo $loop['index']; ?>.thumbnail == ''"
      ng-style="{ 'backgroundImage': 'url(<?php echo $GLOBALS['noimage']; ?>)' }"
    >
    </div>
    <?php if(!isset($_SESSION['secure'])) : ?>
    <span class="private-lock"
      ng-if="<?php echo $loop['index']; ?>.private == 1"
      >
      <i class="fa fa-lock"></i>
    </span>
    <?php endif; ?>
  </div>

  <!-- name and description -->
  <div class="video-details">
    <p class="video-title">
      <span ng-if="lang == 'en'">
        {{ <?php echo $loop['index']; ?>.name }}
      </span>
      <span ng-if="lang == 'fr'">
        {{ <?php echo $loop['index']; ?>.french_name }}
      </span>
      <span ng-if="lang == 'es'">
        {{ <?php echo $loop['index']; ?>.spanish_name }}
      </span>
    </p>
    <p class="video-description">
      <span ng-if="lang == 'en'">
        {{ <?php echo $loop['index']; ?>.description | truncate:75 }}
      </span>
      <span ng-if="lang == 'fr'">
        {{ <?php echo $loop['index']; ?>.french_description | truncate:75 }}
      </span>
      <span ng-if="lang == 'es' &amp;&amp; <?php echo $loop['index']; ?>.spanish_description">
        {{ <?php echo $loop['index']; ?>.spanish_description | truncate:75 }}
      </span>
    </p>
  </div>

  </a>