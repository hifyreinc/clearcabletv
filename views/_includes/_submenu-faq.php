<div class="container-fluid hidden-md-up">
<ul class="category-list list-unstyled gradient">
	<li ng-repeat="category in faq.categories track by category.id"
		ng-if="faq.categories.length"
		>
		<a href="/faq/category/?categoryId={{category.id}}"
			class="category-link"
			ng-class="{'active-tab': faq.categoryId == category.id}"
			ng-cloak
			>
			<span ng-if="lang == 'en'">
				{{category.english_category}}
			</span>
			<span ng-if="lang == 'fr'">
				{{category.french_category}}
			</span>
			<span ng-if="lang == 'es'">
				{{category.spanish_category}}
			</span>
		</a>
	</li>
	<li class="dropdown hidden-lg-up"
		>
		<a href="" class="category-link dropdown-toggle center-text-xs"
			data-toggle="dropdown"
			ng-cloak
			>
			<span ng-if="lang == 'en'">
				{{faq.category.english_category}}
			</span>
			<span ng-if="lang == 'fr'">
				{{faq.category.french_category}}
			</span>
			<span ng-if="lang == 'es'">
				{{faq.category.spanish_category}}
			</span>
		</a>
		<div class="dropdown-menu p-t-0 p-b-0">
			<div class="dropdown-columns">
				<a href="/faq/category/?categoryId={{category.id}}"
					class="dropdown-item"
					ng-repeat="category in faq.categories track by category.id"
					ng-cloak
					>
					<span ng-if="lang == 'en'">
						{{category.english_category}}
					</span>
					<span ng-if="lang == 'fr'">
						{{category.french_category}}
					</span>
					<span ng-if="lang == 'es'">
						{{category.spanish_category}}
					</span>
				</a>
			</div>
		</div>
	</li>
</ul>
</div>