<form action="/search/">
	<div class="form-group search-group">
		<label>
			<span><i class="fa fa-search"></i>&nbsp;<?php echo $tr->__('Search'); ?></span>
		</label>
		<input type="text" class="form-control" name="search" autocomplete="off" />
		<a href="" class="clear-input">
			<?php echo $tr->__('cancel'); ?>
		</a>
	</div>
</form>