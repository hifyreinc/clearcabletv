<div ng-controller="AccountController">

<?php include($GLOBALS['dir'].'views/_includes/_banner-account.php'); ?>
<?php include($GLOBALS['dir'].'views/_includes/_submenu-account.php'); ?>

<div class="main-content-area">
	
	<div class="container-fluid">
	
		<div class="row">
		<div class="col-xs-12 col-md-6">
				
		<form method="post" action="/assets/php/functions.php?type=saveAccountPassword">
			
			<?php if (isset($_SESSION['accountsuccess'])) { ?>
			<div class="response">
				<div class="alert alert-success">
					<i class="fa fa-check"></i> <?php echo $_SESSION['accountsuccess'] ?>
				</div>
			</div>
			<?php unset($_SESSION['accountsuccess']); ?>
			<?php } ?>

			<?php if (isset($_SESSION['accounterror'])) { ?>
			<div class="response">
				<div class="alert alert-danger">
					<i class="fa fa-warning"></i> <?php echo $_SESSION['accounterror'] ?>
				</div>
			</div>
			<?php unset($_SESSION['accounterror']); ?>
			<?php } ?>
			
			<div class="form-group">
				<label><?php echo $tr->__('New Password'); ?></label>
				<input type="password" name="password" class="form-control" />
			</div>
			
			<div class="form-group">
				<label><?php echo $tr->__('Confirm New Password'); ?></label>
				<input type="password" name="passconf" class="form-control" />
			</div>
			
			<div class="form-group button-group text-right">
				<a href="/account/" class="btn btn-secondary btn-sm pull-left"><?php echo $tr->__('Cancel'); ?></a>
				<button type="submit" class="btn btn-primary btn-sm"><?php echo $tr->__('Save'); ?></button>
			</div>
			
		</form>
		
		</div>
		</div>
		
	</div>
	
</div>

</div>