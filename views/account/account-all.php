<div ng-controller="AccountController">
  <div class="main-content-area">
    <div class="container">
      <br class="hidden-sm-down" />
      <!-- title -->
      <h1 class="h3 m-b-1">
        <?php echo $tr->__('My Account'); ?>
      </h1>
      <div class="clearfix"></div>

      <?php if(isset($_SESSION['subscription_error'])): ?>
      <div class="alert alert-danger animated fadeIn"
        ng-if="!editAccount.success"
        ng-cloak
        >
        <?php echo $tr->__('There is an error with your subscription. Please update your billing information.'); ?>
      </div>
      <?php endif; ?>

      <div class="alert alert-success animated fadeIn"
        ng-if="editAccount.success"
        ng-cloak
        >
        {{editAccount.success}}
      </div>

      <hr />

      <!-- Account Details -->
      <fieldset>
        <div class="row">
          <div class="col-md-4">
            <legend class="h5 text-muted">
              <?php echo $tr->__('Account Details'); ?>
            </legend>
            <a href=""
              class="btn btn-secondary btn-sm m-b-1"
              ng-click="deleteAccount()"
              >
              <?php echo $tr->__('Delete my Account'); ?>
            </a>
          </div>
          <!-- email -->
          <div class="col-md-8">
            <div class="row m-b-1">
              <div class="col-sm-7 col-lg-8">
                <span ng-cloak>
                  {{ userEmail }}
                </span>
              </div>
              <div class="col-sm-5 col-lg-4 text-right text-left-xs">
                <a href="#account-modal-email" data-toggle="modal">
                  <?php echo $tr->__('Change email'); ?>
                </a>
              </div>
            </div>
            <!-- password -->
            <div class="row m-b-1">
              <div class="col-sm-7 col-lg-8">
                <span class="text-muted">
                  Password: ********
                </span>
              </div>
              <div class="col-sm-5 col-lg-4 text-right text-left-xs">
                <a href="#account-modal-password" data-toggle="modal">
                  <?php echo $tr->__('Change password'); ?>
                </a>
              </div>
            </div>
          </div>
        </div>
      </fieldset>

      <?php
        // BILLING: Only if stripe is set as payment gateway
        if($settings[2]['value'] == '2'):
      ?>
      <hr/>
      <fieldset>
        <div class="row">
          <div class="col-md-4">
            <legend class="h5 text-muted m-b-1">
              <?php echo $tr->__('Billing Information'); ?>
            </legend>
          </div>
          <div class="col-md-8">
            <div class="row m-b-1">
              <div class="col-sm-7 col-lg-8">
                <div ng-cloak>
                  <span>
                    <strong>
                      {{ user.card.brand }}
                    </strong>
                    ending in {{ user.card.last4 }}
                  </span>
                </div>
                <span ng-cloak class="text-muted">
                  Expiration Date: {{user.card.exp_month}}/{{user.card.exp_year}}
                </span>
              </div>
              <div class="col-sm-5 col-lg-4 text-right text-left-xs">
                <a href="#account-modal-card"
                  data-toggle="modal"
                  >
                  <?php echo $tr->__('Update card'); ?>
                </a>
              </div>
            </div>
          </div>
        </div>
      </fieldset>
      <?php endif; ?>

      <?php
        // PLANS: Only if stripe is set as payment gateway
        if($settings[2]['value'] == '2'):
      ?>
      <hr />
      <fieldset>
        <div class="row">
          <div class="col-md-4">
            <legend class="h5 text-muted m-b-1">
              <?php echo $tr->__('Plan Details'); ?>
            </legend>
          </div>
          <div class="col-md-8">
            <div class="row m-b-1">
              <div class="col-sm-7 col-lg-8">
                <div ng-cloak>
                  <span>
                    {{ user.plan.name }}
                  </span>
                </div>
              </div>
              <div class="col-sm-5 col-lg-4 text-right text-left-xs">
                <a href="#account-modal-plan"
                  data-toggle="modal"
                  >
                  <?php echo $tr->__('Change plan'); ?>
                </a>
              </div>
            </div>
          </div>
        </div>
      </fieldset>
      <?php endif; ?>

    </div>
  </div>

  <?php include($GLOBALS['dir'].'views/_includes/_modal-account.php'); ?>

</div>