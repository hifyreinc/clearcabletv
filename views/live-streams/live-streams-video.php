<div ng-controller="VideoController"
  ng-init="typesection = 1"
  >
  <div class="rearrange-order">
    <?php

      // Set section for include settings
      $section = 'live-streams';

      require_once(realpath(__DIR__ . '/../_includes/_banner-video.php'));
      require_once(realpath(__DIR__ . '/../_includes/_submenu-live-streams-new.php'));

    ?>
  </div>

  <!-- related videos -->
  <div class="main-content-area">
    <!-- related videos -->
    <?php

      // Define Title Variables
      $title  = $tr->__('Related Videos');
      
      // Define Loop Variables
      $loop['index'] = 'video';
      $loop['array'] = 'videos';

      // Include Loop template
      echo '<div ng-if="'. $loop['array'] . '.length > 1">';
      require( realpath( __DIR__ . '/../_includes/strip-title.php' ) );
      require(realpath(__DIR__ . '/../_includes/filmstrip.php'));
      echo '</div>';

    ?>

    <!-- top 10 videos -->
    <div id="popularVideos">

      <?php

        // Define Title Variables
        $title  = $tr->__('Most Popular');
        require(realpath(__DIR__ . '/../_includes/strip-title.php'));
        
        // Define Loop Variables
        $loop['index'] = 'popular';
        $loop['array'] = 'popularVideos';

        // Include Loop template
        require(realpath(__DIR__ . '/../_includes/filmstrip.php'));

      ?>
      

    </div>
  </div>
</div>