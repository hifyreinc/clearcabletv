<div ng-controller="FaqCategoryController as faq">

<?php include($GLOBALS['dir'].'views/_includes/_banner-faq.php'); ?>
<?php include($GLOBALS['dir'].'views/_includes/_submenu-faq.php'); ?>

<div class="main-content-area">

	<div class="container">

		<br class="hidden-sm-down" />
		<h1 class="h3 hidden-sm-down">
			<?php echo $tr->__('Frequently Asked Questions'); ?>
		</h1>
		<br class="hidden-sm-down" />

		<div class="row">
			<div class="col-xs-12 col-md-4 col-lg-3 hidden-sm-down">

				<ul class="nav nav-pills nav-stacked custom-nav-pills-stacked">
					<li ng-repeat="category in faq.categories track by category.id"
						ng-if="faq.categories.length"
						class="nav-item"
						>
						<a href="/faq/category/?categoryId={{category.id}}"
							class="nav-link"
							ng-class="{'active': faq.categoryId == category.id}"
							ng-cloak
							>
							<span ng-if="lang == 'en'">{{category.english_category}}</span>
							<span ng-if="lang == 'fr'">{{category.french_category}}</span>
							<span ng-if="lang == 'es'">{{category.spanish_category}}</span>
						</a>
					</li>
				</ul>

			</div>
			<div class="col-xs-12 col-md-8 col-lg-9">

		<div class="panel panel-default"
			ng-repeat="question in faq.questions track by question.id"
			ng-cloak
			>
			<div class="panel-heading">
				<h6 class="panel-title">
					<a class="dropdown-toggle"
						data-toggle="collapse"
						href="#question-{{question.id}}"
						>
						<span ng-if="lang == 'en'">{{question.english_question}}</span>
						<span ng-if="lang == 'fr'">{{question.french_question}}</span>
						<span ng-if="lang == 'es'">{{question.spanish_question}}</span>
					</a>
				</h6>
			</div>
			<div id="question-{{question.id}}" class="panel-collapse collapse">
				<div class="panel-body">
					<div ng-if="lang == 'en'">{{question.english}}</div>
					<div ng-if="lang == 'fr'">{{question.french}}</div>
					<div ng-if="lang == 'es'">{{question.spanish}}</div>
				</div>
			</div>
		</div>

			</div>
		</div>

	</div>

</div>

</div>