<div ng-controller="SignupController">

<div class="main-content-area">
<div class="container">

	<br class="hidden-md-down" />
	<br class="hidden-md-down" />

	<h1 class="h3 text-center">
		<?php echo $tr->__("Watch all of your favourite channels"); ?>
	</h1>

	<br/>

	<form action="" method="POST" id="payment-form" autocomplete="off">

		<input hidden>
		<input type="password" hidden>

	<div class="form-signup">

	<div class="response"></div>

	<div class="row">
		<div class="col-md-6">

			<h2 class="h6 text-center text-uppercase m-b-1">
				<?php echo $tr->__("Choose your Package"); ?>
			</h2>

			<div class="form-group">

				<div class="btn-group-vertical btn-block btn-group-plan" data-toggle="buttons">
					<label class="btn btn-lg btn-secondary-outline active">
						<input type="radio"
							name="subscription_plan"
							autocomplete="off"
							value="daily"
							checked
						> <?php echo $tr->__("Daily"); ?>
						<span class="price">$2</span>
					</label>
					<label class="btn btn-lg btn-secondary-outline">
						<input type="radio"
							name="subscription_plan"
							autocomplete="off"
							value="weekly"
						> <?php echo $tr->__("Weekly"); ?>
						<span class="price">$10</span>
					</label>
					<label class="btn btn-lg btn-secondary-outline">
						<input type="radio"
							name="subscription_plan"
							autocomplete="off"
							value="monthly"
						> <?php echo $tr->__("Monthly"); ?>
						<span class="price">$30</span>
					</label>
				</div>

			</div>

		</div>
		<div class="col-md-6">

			<br class="hidden-md-up" />

			<h2 class="h6 text-center text-uppercase m-b-1">
				<?php echo $tr->__("Complete Your Account Details"); ?>
			</h2>

			<div class="form-group">
				<input type="email"
					class="form-control form-control-outline"
					name="subscription_email"
					placeholder="Email Address"
					autocomplete="off"
				/>
			</div>

			<div class="form-group">
				<input type="password"
					class="form-control form-control-outline"
					name="subscription_password"
					placeholder="Password"
					autocomplete="off"
				/>
			</div>

			<div class="form-group">
				<label>
					<?php echo $tr->__("Card Number"); ?>
				</label>
				<input type="text"
					class="form-control form-control-outline"
					size="20"
					maxlength="20"
					data-stripe="number"
					value="4242 4242 4242 4242"
				/>
			</div>

			<div class="row">
				<div class="col-xs-4">
					<div class="form-group">
						<input type="text"
							class="form-control form-control-outline"
							size="2"
							maxlength="2"
							data-stripe="exp-month"
							placeholder="MM"
							value="02"
						/>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="form-group">
						<input type="text"
							class="form-control form-control-outline"
							size="4"
							maxlength="4"
							data-stripe="exp-year"
							placeholder="YYYY"
							value="2017"
						/>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="form-group">
						<input type="text"
							class="form-control form-control-outline"
							size="4"
							maxlength="4"
							data-stripe="cvc"
							placeholder="CVC"
							value="123"
						/>
					</div>
				</div>
			</div>

		</div>
	</div>
	</div>

	<br />

	<div class="form-group text-center">

		<button class="btn btn-lg btn-primary"
			ng-click="submitSubsciption()"
			>
			<?php echo $tr->__("Sign Up"); ?>
		</button>

	</div>

	</form>


	</div>

</div>
</div>

</div>