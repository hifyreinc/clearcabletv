<div ng-controller="AdminEditController"
	ng-init="section = 2"
	>
  <?php

    // Set column width
    $cols = ( 12 / count($enabled_languages) );

    // include template
    require_once(realpath(__DIR__ . '/_includes/admin-edit.php'));

  ?>
</div>