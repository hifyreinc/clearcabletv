<?php

  require_once(realpath(__DIR__ . '/../../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../../_includes/_submenu-admin.php'));

?>

<div class="main-content-area">
  <div class="container-fluid">

    <!-- top title -->
    <h1 class="h4"
      ng-cloak
      >
      <span ng-if="section == 1">
        <?php echo $tr->__('Add New Live Stream Item'); ?> &nbsp;
      </span>
      <span ng-if="section == 2">
        <?php echo $tr->__('Add New Video-On-Demand Item'); ?> &nbsp;
      </span>
      <span ng-if="section == 3">
        <?php echo $tr->__('Add New Advertisement Item'); ?> &nbsp;
      </span>
      <span ng-if="section == 4">
        <?php echo $tr->__('Add New Go Service Item'); ?> &nbsp;
      </span>
      <a href=""
        class="btn btn-danger btn-sm"
        ng-click="cancel()"
        >
        <?php echo $tr->__('Cancel'); ?>
      </a>
    </h1>

    <br/>

    <div ng-if="section == 2 || section == 3"
      ng-cloak
      >

      <h2 class="h5 with-em">
        <?php echo $tr->__('Drag and Drop video below'); ?>
        <em>
          0%
        </em>
      </h2>

      <div class="dropzone-container">
        <div class="dropzone"
           nv-file-drop
           uploader="videoUploader"
           >
          <i class="fa fa-cloud-upload"></i>
        </div>
      </div>

    </div>

    <div ng-if="section == 1"
      ng-cloak
      >
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Protocol for rtmp/mp4'); ?>
            </label>
            <br/>
            <select class="form-control c-select"
              ng-model="stream.url"
              >
              <option value="rtmp">
                <?php echo $tr->__('rtmp'); ?>
              </option>
              <option value="rtmps">
                <?php echo $tr->__('rtmps'); ?>
              </option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-3">
          <div class="form-group">
            <label><?php echo $tr->__('Protocol for m3u8'); ?></label><br/>
            <select class="form-control c-select"
              ng-model="stream.m3u8"
              >
              <option value="http">
                <?php echo $tr->__('http'); ?>
              </option>
              <option value="https">
                <?php echo $tr->__('https'); ?>
              </option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-4">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Hostname'); ?>
            </label>
            <input type="text"
              class="form-control"
              ng-model="stream.hostname"
              />
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-2">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Port'); ?>
            </label>
            <input type="text"
              class="form-control"
              ng-model="stream.port"
              />
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-6">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Application'); ?>
            </label>
            <input type="text"
              class="form-control"
              ng-model="stream.application"
              />
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-6">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Streamname'); ?>
            </label>
            <input type="text"
              class="form-control"
              ng-model="stream.streamname"
              />
          </div>
        </div>
      </div>
      <div class="form-group button-group text-right">
        <a href=""
          class="btn btn-primary btn-sm"
          ng-click="addStream()"
          >
          <?php echo $tr->__('Add Live Stream'); ?>
        </a>
      </div>
    </div>

    <div ng-if="section == 4"
      ng-cloak
      >
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Service Link'); ?>
            </label>
            <input type="text"
              class="form-control"
              ng-model="stream.url"
              />
          </div>
        </div>
      </div>
      <div class="form-group button-group text-right">
        <a href=""
          class="btn btn-primary btn-sm"
          ng-click="addGoService()"
          >
          <?php echo $tr->__('Add Service'); ?>
        </a>
      </div>
    </div>

    <br/>

    <div class="video-upload-list">
      <div ng-repeat="(index, item) in videoUploader.queue"
        ng-init="itemInit(item, index)"
        id="item{{index}}"
        ng-cloak
        >
        <div class="row">
          <div class="col-xs-12 col-md-9 col-md-push-3">

          <progress class="progress"
            value="{{item.progress}}"
            max="100"
            ng-if="!item.isUploaded"
            ng-class="item.progress < 100 ? 'progress-success' : 'progress-warning'"
            >
            <div class="progress">
              <span class="progress-bar"
                ng-style="{'width':item.progress+'%'}"
                >
                <span ng-show="item.progress < 100 &amp;&amp; item.isUploading">
                  {{item.progress}}%
                </span>
                <span ng-show="item.progress == 100 &amp;&amp; item.isUploading">
                  <i class="fa fa-refresh fa-pulse"></i>
                  <?php echo $tr->__('Processing'); ?>
                  ...
                </span>
                <span ng-show="item.progress == 0 &amp;&amp; !item.isUploading"
                  class="text-muted"
                  >
                  <?php echo $tr->__('In Queue'); ?>
                  ...
                </span>
              </span>
            </div>
          </progress>

          <ul class="nav nav-pills custom-nav-pills">
            <li class="nav-item">
              <a href="#basic-{{index}}"
                data-toggle="tab"
                class="nav-link active"
                >
                <?php echo $tr->__('Basic Info'); ?>
              </a>
            </li>
            <li class="nav-item">
              <a href="#advanced-{{index}}"
                data-toggle="tab"
                class="nav-link"
                >
                <?php echo $tr->__('Advanced Settings'); ?>
              </a>
            </li>
          </ul>

          <div class="tab-content">
            <div class="tab-pane active" id="basic-{{index}}">
              <div class="row">

                <?php if (in_array('en', $enabled_languages)): ?>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('English Title'); ?>
                    </label>
                    <input type="text"
                      class="form-control"
                      ng-model="item.formData[0].name"
                      />
                  </div>
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('English Description'); ?>
                    </label>
                    <textarea class="form-control"
                      ng-model="item.formData[0].description"
                      rows="6"
                      >
                    </textarea>
                  </div>
                </div>
                <?php endif; ?>

                <?php if (in_array('fr', $enabled_languages)): ?>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('French Title'); ?>
                    </label>
                    <input type="text"
                      class="form-control"
                      ng-model="item.formData[0].french_name"
                      />
                  </div>
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('French Description'); ?>
                    </label>
                    <textarea class="form-control"
                      ng-model="item.formData[0].french_description"
                      rows="6"
                      >
                    </textarea>
                  </div>
                </div>
                <?php endif; ?>

                <?php if (in_array('es', $enabled_languages)): ?>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Spanish Title'); ?>
                    </label>
                    <input type="text"
                      class="form-control"
                      ng-model="item.formData[0].spanish_name"
                      />
                  </div>
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Spanish Description'); ?>
                    </label>
                    <textarea class="form-control"
                      ng-model="item.formData[0].spanish_description"
                      rows="6"
                      >
                    </textarea>
                  </div>
                </div>
                <?php endif; ?>
              </div>

              <div class="row"
                ng-if="section != 3"
                >
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Category'); ?>
                    </label>
                    <select class="form-control c-select"
                      ng-model="item.formData[0].categories"
                      multiple
                      >
                      <option value="{{section.id}}"
                        ng-repeat="section in sections"
                        ng-selected="compareCategories(section.id)"
                        >
                        {{ lang == 'en' ? section.name : '' }}
                        {{ lang == 'fr' ? section.french_name : '' }}
                        {{ lang == 'es' ? section.spanish_name : '' }}
                      </option>
                    </select>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Type'); ?>
                    </label>
                    <select class="form-control c-select"
                      ng-model="item.formData[0].type"
                      disabled
                      >
                      <option value="stream">
                        <?php echo $tr->__('Live Stream'); ?>
                      </option>
                      <option value="video">
                        <?php echo $tr->__('Video'); ?>
                      </option>
                    </select>
                  </div>
                </div>
              </div>

              <!-- *** SHOW IF STREAM *** -->
              <div class="row"
                ng-show="item.formData[0].type == 'stream'"
                >
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label><?php echo $tr->__('Stream Link'); ?></label>
                    <input type="text"
                      class="form-control"
                      ng-model="item.formData[0].url"
                      />
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('m3u8 Link'); ?>
                    </label>
                    <input type="text"
                      class="form-control"
                      ng-model="item.formData[0].m3u8"
                      />
                  </div>
                </div>
              </div>
              <!-- *** SHOW IF STREAM *** -->

              <!-- *** SHOW IF VIDEO *** -->
              <div class="row"
                ng-show="item.formData[0].type == 'video' || item.formData[0].type == 'advertisement'"
                >
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('MP4 Video'); ?>
                      <span ng-if="item.isUploaded">
                        (<?php echo $tr->__('Drag and drop video below'); ?>)
                      </span>
                    </label>
                    <div class="form-control dropzone"
                      nv-file-drop
                      uploader="videoUploader"
                      ng-class="{ 'dropzone': item.isUploaded }"
                      >
                      <span ng-bind="item.file.name"></span>
                      <span ng-if="item.isUploading" >
                        &nbsp;&nbsp;
                        <i class="fa fa-refresh fa-pulse"
                          ng-class="item.progress < 100 ? 'text-success' : 'text-warning'"
                          >
                        </i>
                      </span>
                      <span ng-if="item.isSuccess || item.isUploaded">
                        &nbsp;&nbsp;
                        <i class="fa fa-check-circle text-success"></i>
                      </span>
                      <input type="hidden"
                        ng-model="item.file.name"
                        />
                      <i class="fa fa-cloud-upload"></i>
                    </div>
                  </div>
                </div>
              </div>
              <!-- *** SHOW IF VIDEO *** -->
            </div>

            <div class="tab-pane" id="advanced-{{index}}">
              <div class="row">

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Publish Start'); ?>
                    </label>
                    <input type="text"
                      class="form-control publish-start-toggle{{index}}"
                      value="{{item.formData[0].publishstart | date:'yyyy-MM-dd HH:mm:ss'}}"
                      data-toggle="dropdown"
                      />
                    <div class="dropdown-menu datetimepicker-dropdown dropdown{{index}}">
                      <datetimepicker data-ng-model="item.formData[0].publishstart"
                        data-on-set-time="onSetTime(newDate, oldDate, index)"
                        data-datetimepicker-config="{ dropdownSelector: '.publish-start-toggle{{index}}', minView: 'hour' }"
                        >
                      </datetimepicker>
                    </div>
                    <a href=""
                      class="btn btn-sm btn-link p-l-0"
                      ng-click="clearPublishDates(item.formData[0])"
                      >
                      <?php echo $tr->__('Clear Dates'); ?>
                    </a>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Publish End'); ?>
                    </label>
                    <input type="text"
                      class="form-control publish-end-toggle{{index}}"
                      value="{{item.formData[0].publishend | date:'yyyy-MM-dd HH:mm:ss'}}"
                      data-toggle="dropdown"
                      ng-disabled="item.formData[0].publishstart == null || item.formData[0].publishstart == ''"
                      />
                    <div class="dropdown-menu datetimepicker-dropdown dropdown{{index}}">
                      <datetimepicker data-ng-model="item.formData[0].publishend"
                        data-on-set-time="onSetTime(newDate, oldDate, index)"
                        data-datetimepicker-config="{ dropdownSelector: '.publish-end-toggle{{index}}', minView: 'hour' }"
                        >
                      </datetimepicker>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label><?php echo $tr->__('Tags (separate tags with a comma)'); ?></label>
                    <textarea class="form-control"
                      ng-model="item.formData[0].tags"
                      >
                    </textarea>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6"
                  ng-if="section != 3"
                  >
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Preroll Advertisement'); ?>
                    </label>
                    <br/>
                    <label class="c-input c-radio">
                      <input name="radio-0"
                        type="radio"
                        />
                      <span class="c-indicator"></span>
                      <?php echo $tr->__('Yes'); ?>
                    </label>
                    <label class="c-input c-radio">
                      <input name="radio-0"
                        type="radio"
                        />
                      <span class="c-indicator"></span>
                      <?php echo $tr->__('No'); ?>
                    </label>
                    <div ng-form="adForm">
                      <select class="form-control c-select m-t-10"
                        ng-model="$parent.selectedAd"
                        ng-init="$parent.selectedAd = -1"
                        ng-options="myAd.id as myAd.name for myAd in advertisements"
                        >
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"
                ng-if="section != 3"
                >
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Set as Featured Video?'); ?>
                    </label>
                    <br/>
                    <label class="c-input c-radio">
                      <input type="radio"
                        name="featured{{index}}"
                        ng-model="item.formData[0].featured"
                        ng-value="true"
                        />
                      <span class="c-indicator"></span>
                      <?php echo $tr->__('Yes'); ?>
                    </label>
                    <label class="c-input c-radio">
                      <input type="radio"
                        name="featured{{index}}"
                        ng-model="item.formData[0].featured"
                        ng-value="false"
                        />
                      <span class="c-indicator"></span>
                      <?php echo $tr->__('No'); ?>
                    </label>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>
                      <?php echo $tr->__('Set as Private Video?'); ?>
                    </label>
                    <br/>
                    <label class="c-input c-radio">
                      <input type="radio"
                        name="private{{index}}"
                        ng-model="item.formData[0].private"
                        ng-value="true"
                        />
                      <span class="c-indicator"></span>
                      <?php echo $tr->__('Yes'); ?>
                    </label>
                    <label class="c-input c-radio">
                      <input type="radio"
                        name="private{{index}}"
                        ng-model="item.formData[0].private"
                        ng-value="false"
                        />
                      <span class="c-indicator"></span>
                      <?php echo $tr->__('No'); ?>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
          <div class="col-xs-12 col-md-3 col-md-pull-9">
            <div class="video-info">
              <label class="hidden-md-up">
                <?php echo $tr->__('Thumbnail'); ?>
              </label>
              <div class="video-thumb">
                <div class="thumbnail-image"
                  nv-file-drop
                  uploader="thumbUploader"
                  options="{formData:[{videoIndex: index}]}"
                  ng-style="item.formData[0].thumbStyle"
                  >
                </div>
                <input type="hidden"
                  ng-model="item.formData[0].thumbImageName"
                  />
              </div>
              <div class="video-details text-left"
                ng-if="item.isUploaded"
                >
                <p class="video-description">
                  <?php echo $tr->__('Drag and drop an image on the thumbnail to replace.'); ?>
                  <strong>
                    <?php echo $tr->__('For best results, image should be 16:9 aspect ratio (eg. 800 x 450)'); ?>
                  </strong>
                </p>
              </div>

              <div class="video-details text-left m-t-10"
                ng-show="'video' == item.formData[0].type || 'advertisement' == item.formData[0].type"
                >
                <p class="video-title">
                  <strong>
                    <?php echo $tr->__('Video Status'); ?>:
                  </strong>
                  <span ng-show="item.progress == 0 &amp;&amp; !item.isUploading">
                    <?php echo $tr->__('In Queue'); ?>
                    ...
                  </span>
                  <span ng-show="item.progress < 100 &amp;&amp; item.isUploading"
                    class="text-success"
                    >
                    <?php echo $tr->__('Uploading'); ?>
                    ...
                  </span>
                  <span ng-show="item.progress == 100 &amp;&amp; item.isUploading"
                    class="text-warning"
                    >
                    <?php echo $tr->__('Processing'); ?>
                    ...
                  </span>
                  <span ng-show="item.isUploaded"
                    class="text-success"
                    >
                    <?php echo $tr->__('Uploaded'); ?>
                  </span>
                </p>
                <p class="video-title">
                  <strong>
                    <?php echo $tr->__('File Name'); ?>:
                  </strong>
                  <span ng-bind="item.file.name"></span>
                </p>
                <p class="video-title">
                  <strong>
                    <?php echo $tr->__('File Size'); ?>:
                  </strong>
                  <span ng-bind="(item.file.size/1000/1000|number:2) + ' MB'"></span>
                </p>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group button-group text-right">
          <a href=""
            class="btn btn-link btn-sm"
            ng-click="removeVideo(item.formData[0], index)"
            >
            <?php echo $tr->__('Delete'); ?>
          </a>
          <a href=""
            class="btn btn-primary btn-sm"
            ng-class="{
              disabled: !item.formData[0].categories.length &amp;&amp; item.formData[0].categoryId !== '113'
            }"
            ng-click="updateVideo(item.formData[0], index)"
            ng-if="item.isUploaded"
            >
            <?php echo $tr->__('Save Changes'); ?>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>