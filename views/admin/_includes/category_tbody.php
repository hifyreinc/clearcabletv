<li id="video{{index}}"
  ng-repeat="(index, video) in videos | orderBy: 'position'"
  data-video="{{ video.id }}"
  ng-if="sectionVideoCount > 0"
  ng-cloak
  >

  <!-- sorting icon -->
  <div class="sort-handle">
    <i class="fa fa-sort"></i>
  </div>

  <div class="video-info">
    <div class="row display">
      <div class="col-xs-2 p-r-0">

        <!-- thumbnail -->
        <div ng-if="video.thumbnail != ''"
          class="thumbnail-image small"
          ng-style="{'backgroundImage': 'url(/uploads/images/{{video.thumbnail}}?m={{video.filemtime}})'}"
          >
        </div>
        <div ng-if="video.thumbnail == ''"
          class="thumbnail-image small"
          ng-style="{'backgroundImage': 'url(//placehold.it/480x270/000000&amp;text=Image+Not+Found)'}"
          >
        </div>
      </div>

      <!-- video details -->
      <div class="col-xs-5 video-deets">
        <span class="video-title">
          {{ lang == 'en' ? video.name : '' }}
          {{ lang == 'fr' ? video.french_name : '' }}
          {{ lang == 'es' ? video.spanish_name : '' }}
        </span>
        <span class="video-dscr">
          {{ lang == 'en' ? video.description : '' }}
          {{ lang == 'fr' ? video.french_description : '' }}
          {{ lang == 'es' ? video.spanish_description : '' }}
        </span>
        <div class="actions">
          <a href="/admin/<?php echo $type; ?>/edit/?categoryId={{ categoryId }}&amp;videoId={{ video.id }}"
            class="action-link"
            >
            <?php echo $tr->__('Edit'); ?>
          </a>
          <span class="action-link-divider">|</span>
          <a href=""
            class="action-link action-delete"
            ng-click="deleteVideo(video, $index)"
            >
            <?php echo $tr->__('Delete'); ?>
          </a>
        </div>
      </div>

      <div class="col-xs-1 text-xs-center">
        <span ng-if="video.featured == 1">
          <?php echo $tr->__('Yes'); ?>
        </span>
        <span ng-if="video.featured == 0">
          <?php echo $tr->__('No'); ?>
        </span>
      </div>
      <div class="col-xs-2">
        <span ng-if="video.publishstart">
          <?php echo $tr->__('Start: '); ?>
          {{ video.publishstart }}
          <br />
        </span>
        <span ng-if="video.publishend">
          <?php echo $tr->__('End: '); ?>
          {{ video.publishend }}
        </span>
        <span ng-if="!video.publishstart">
          <?php echo $tr->__('Unplublished'); ?>
        </span>
      </div>
      <div class="col-xs-2">
        <span ng-if="video.private == 1">
          <?php echo $tr->__('Private'); ?>
        </span>
        <span ng-if="video.private == 0">
          <?php echo $tr->__('Public'); ?>
        </span>
      </div>

    </div>

  </div>
</li>