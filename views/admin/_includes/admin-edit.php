<?php

  require_once(realpath(__DIR__ . '/../../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../../_includes/_submenu-admin.php'));

?>

<div class="main-content-area">
  <div class="container-fluid">

    <!-- top title -->
    <h1 class="h4"
      ng-cloak
      >
      <span ng-if="section == 1">
        <?php echo $tr->__('Edit Live Stream Item'); ?> &nbsp;
      </span>
      <span ng-if="section == 2">
        <?php echo $tr->__('Edit Video-On-Demand Item'); ?> &nbsp;
      </span>
      <span ng-if="section == 3">
        <?php echo $tr->__('Edit Advertisement Item'); ?> &nbsp;
      </span>
      <span ng-if="section == 4">
        <?php echo $tr->__('Edit Go Service Item'); ?> &nbsp;
      </span>
      <a href=""
        class="btn btn-secondary btn-sm"
        ng-click="cancel()"
        >
        <?php echo $tr->__('Cancel'); ?>
      </a>
    </h1>

    <br />

    <!-- columns -->
    <div class="row"
      ng-cloak
      >
      <div class="col-xs-12 col-md-9 col-md-push-3">

        <!-- video progress -->
        <progress class="progress"
          value="{{videoUploader.progress}}"
          max="100"
          ng-class="videoUploader.progress < 100 ? 'progress-success' : 'progress-warning'"
          >
          <div class="progress">
            <span class="progress-bar"
              ng-style="{'width':videoUploader.progress+'%'}"
              >
            </span>
          </div>
        </progress>

        <!-- column toggle -->
        <ul class="nav nav-pills custom-nav-pills">
          <li class="nav-item">
            <a href="#basic"
              data-toggle="tab"
              class="nav-link active"
              >
              <?php echo $tr->__('Basic Info'); ?>
            </a>
          </li>
          <li class="nav-item"
            ng-if="section != 4"
            >
            <a href="#advanced"
              data-toggle="tab"
              class="nav-link"
              >
              <?php echo $tr->__('Advanced Settings'); ?>
            </a>
          </li>
        </ul>

        <div class="tab-content">

          <!-- basic info -->
          <div class="tab-pane active"
            id="basic"
            >
            <div class="row">

            <!-- titles and descriptions -->
            <?php if (in_array('en', $enabled_languages)): ?>
            <div class="col-md-<?php echo $cols; ?>">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('English Title'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.name"
                  />
              </div>
              <div class="form-group">
                <label>
                  <?php echo $tr->__('English Description'); ?>
                </label>
                <textarea class="form-control"
                  ng-model="video.description"
                  rows="6"
                  >
                </textarea>
              </div>
            </div>
            <?php endif; ?>

            <?php if (in_array('fr', $enabled_languages)): ?>
            <div class="col-md-<?php echo $cols; ?>">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('French Title'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.french_name"
                  />
              </div>
              <div class="form-group">
                <label>
                  <?php echo $tr->__('French Description'); ?>
                </label>
                <textarea class="form-control"
                  ng-model="video.french_description"
                  rows="6"
                  >
                </textarea>
              </div>
            </div>
            <?php endif; ?>

            <?php if (in_array('es', $enabled_languages)) : ?>
            <div class="col-md-<?php echo $cols; ?>">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Spanish Title'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.spanish_name"
                  />
              </div>
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Spanish Description'); ?>
                </label>
                <textarea class="form-control"
                  ng-model="video.spanish_description"
                  rows="6"
                  >
                </textarea>
              </div>
            </div>
            <?php endif; ?>
          </div>

          <hr />

          <!-- categories and settings -->
          <div class="row"
            ng-if="section == 1 || section == 2"
            >
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Category'); ?>
                </label>
                <select class="form-control c-select"
                  ng-model="video.categories"
                  multiple
                  >
                  <option value="{{ section.id }}"
                    ng-repeat="section in sections"
                    ng-selected="compareCategories(section.id)"
                    >
                    {{ lang == 'en' ? section.name : '' }}
                    {{ lang == 'fr' ? section.french_name : '' }}
                    {{ lang == 'es' ? section.spanish_name : '' }}
                  </option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Type'); ?>
                </label>
                <select class="form-control c-select"
                  ng-model="video.type"
                  disabled
                  >
                  <option value="stream"
                    ng-selected="'stream' == video.type"
                    >
                    <?php echo $tr->__('Live Stream'); ?>
                  </option>
                  <option value="video"
                    ng-selected="'video' == video.type"
                    >
                    <?php echo $tr->__('Video'); ?>
                  </option>
                  <option value="advertisement"
                    ng-selected="'advertisement' == video.type"
                    >
                    <?php echo $tr->__('Advertisement'); ?>
                  </option>
                </select>
              </div>
            </div>
          </div>

          <!-- *** SHOW IF STREAM *** -->
          <div class="row"
            ng-show="'stream' == video.type"
            >
            <div class="col-xs-12 col-sm-6 col-lg-3">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Protocol for rtmp/mp4'); ?>
                </label>
                <br/>
                <select class="form-control c-select"
                  ng-model="video.url"
                  >
                  <option value="rtmp"
                    ng-selected="'rtmp' == video.url"
                    >
                    <?php echo $tr->__('rtmp'); ?>
                  </option>
                  <option value="rtmps"
                    ng-selected="'rtmps' == video.url"
                    >
                    <?php echo $tr->__('rtmps'); ?>
                  </option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-3">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Protocol for m3u8'); ?>
                </label>
                <br/>
                <select class="form-control c-select"
                  ng-model="video.m3u8"
                  >
                  <option value="http"
                    ng-selected="'http' == video.m3u8"
                    >
                    <?php echo $tr->__('http'); ?>
                  </option>
                  <option value="https"
                    ng-selected="'https' == video.m3u8"
                    >
                    <?php echo $tr->__('https'); ?>
                  </option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Hostname'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.hostname"
                  />
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-2">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Port'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.port"
                  />
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-6">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Application'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.application"
                />
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-6">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Streamname'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.streamname"
                  />
              </div>
            </div>
          </div>
          <!-- *** SHOW IF STREAM *** -->

          <!-- *** SHOW IF VIDEO *** -->
          <div class="row"
             ng-show="'video' == video.type || 'advertisement' == video.type"
             >
            <div class="col-xs-12">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('MP4 Video'); ?>
                  (<?php echo $tr->__('Drag and drop video below'); ?>)
                </label>
                <div class="form-control dropzone"
                  nv-file-drop uploader="videoUploader"
                  >
                  <span ng-bind="video.mp4"></span>
                  <span ng-if="item.isUploading">
                    &nbsp;&nbsp;
                    <i class="fa fa-refresh fa-pulse text-danger"></i>
                  </span>
                  <span ng-if="item.isSuccess || item.isUploaded">
                    &nbsp;&nbsp;
                    <i class="fa fa-check-circle text-success"></i>
                  </span>
                  <i class="fa fa-cloud-upload"></i>
                </div>
              </div>
            </div>
          </div>
          <!-- *** SHOW IF VIDEO *** -->

          <!-- *** SHOW IF GO SERVICE *** -->
          <div class="row"
            ng-show="'go' == video.type"
            >
            <div class="col-xs-12">
              <div class="form-group">
                <label>
                  <?php echo $tr->__('Stream Link'); ?>
                </label>
                <input type="text"
                  class="form-control"
                  ng-model="video.url"
                  />
              </div>
            </div>
          </div>
          <!-- *** SHOW IF GO SERVICE *** -->

        </div>

        <!-- advanced settings -->
          <div class="tab-pane"
            id="advanced"
            ng-if="section != 4"
            >
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Publish Start'); ?>
                  </label>
                  <input type="text"
                    class="form-control publish-start-toggle"
                    value="{{video.publishstart | date:'yyyy-MM-dd HH:mm:ss'}}"
                    data-toggle="dropdown"
                    />
                  <div class="dropdown-menu datetimepicker-dropdown">
                    <datetimepicker data-ng-model="video.publishstart"
                      data-on-set-time="onSetTime(newDate, oldDate)"
                      data-datetimepicker-config="{ dropdownSelector: '.publish-start-toggle', minView: 'hour' }"
                      >
                    </datetimepicker>
                  </div>
                  <a href=""
                    class="btn btn-sm btn-link p-l-0"
                    ng-click="clearPublishDates()"
                    >
                    <?php echo $tr->__('Clear Dates'); ?>
                  </a>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Publish End'); ?>
                  </label>
                  <input type="text"
                    class="form-control publish-end-toggle"
                    value="{{video.publishend | date:'yyyy-MM-dd HH:mm:ss'}}"
                    ng-disabled="video.publishstart == null || video.publishstart == ''"
                    data-toggle="dropdown"
                    />
                  <div class="dropdown-menu datetimepicker-dropdown">
                    <datetimepicker data-ng-model="video.publishend"
                      data-on-set-time="onSetTime(newDate, oldDate)"
                      data-datetimepicker-config="{ dropdownSelector: '.publish-end-toggle', minView: 'hour' }"
                      >
                    </datetimepicker>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Tags (separate tags with a comma)'); ?>
                  </label>
                  <textarea class="form-control"
                    ng-model="video.tags"
                    >
                  </textarea>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6"
                ng-if="section != 3"
                >
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Preroll Advertisement'); ?>
                  </label>
                  <br/>
                  <select class="form-control c-select"
                    ng-model="video.preroll"
                    >
                    <option value="{{myAd.id}}"
                      ng-repeat="myAd in advertisements"
                      ng-selected="myAd.id == video.preroll"
                      >
                      {{ lang == 'en' ? myAd.name : '' }}
                      {{ lang == 'fr' ? myAd.french_name : '' }}
                      {{ lang == 'es' ? myAd.spanish_name : '' }}
                    </option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row"
              ng-if="section != 3"
              >
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Set as Featured Video?'); ?>
                  </label>
                  <br/>
                  <label class="c-input c-radio">
                    <input type="radio"
                      name="radio-1"
                      ng-model="video.featured"
                      ng-value="true"
                      />
                    <span class="c-indicator"></span>
                    <?php echo $tr->__('Yes'); ?>
                  </label>
                  <label class="c-input c-radio">
                    <input type="radio"
                      name="radio-1"
                      ng-model="video.featured"
                      ng-value="false"
                      />
                    <span class="c-indicator"></span>
                    <?php echo $tr->__('No'); ?>
                  </label>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Set as Private Video?'); ?>
                  </label>
                  <br/>
                  <label class="c-input c-radio">
                    <input type="radio"
                      name="radio-2"
                      ng-model="video.private"
                      ng-value="true"
                      />
                    <span class="c-indicator"></span>
                    <?php echo $tr->__('Yes'); ?>
                  </label>
                  <label class="c-input c-radio">
                    <input type="radio"
                      name="radio-2"
                      ng-model="video.private"
                      ng-value="false"
                      />
                    <span class="c-indicator"></span>
                    <?php echo $tr->__('No'); ?>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-3 col-md-pull-9">
        <div class="video-info">
          <label class="hidden-md-up">
            <?php echo $tr->__('Thumbnail'); ?>
          </label>
          <div class="video-thumb">
            <div class="thumbnail-image"
              nv-file-drop
              uploader="thumbUploader"
              ng-style="video.thumbStyle"
              >
            </div>
          </div>
          <div class="video-details text-left">
            <p class="video-description">
              <?php echo $tr->__('Drag and drop an image on the thumbnail to replace.'); ?>
              <strong>
                <?php echo $tr->__('For best results, image should be 16:9 aspect ratio (eg. 800 x 450)'); ?>
              </strong>
            </p>
          </div>

          <div class="video-details text-left m-t-10"
            ng-show="'video' == video.type || 'advertisement' == video.type"
            >
            <p class="video-title"
              ng-if="videoUploader.queue[videoUploader.queue.length-1].isUploading || videoUploader.queue[videoUploader.queue.length-1].isUploaded"
              >
              <strong><?php echo $tr->__('Video Status'); ?>:</strong>
              <span ng-if="videoUploader.queue[videoUploader.queue.length-1].isUploading">
                <?php echo $tr->__('Uploading'); ?>...
              </span>
              <span ng-if="videoUploader.queue[videoUploader.queue.length-1].isUploaded">
                <?php echo $tr->__('Uploaded'); ?>
              </span>
            </p>
            <p class="video-title">
              <strong><?php echo $tr->__('File Name'); ?>:</strong> <span ng-bind="video.mp4"></span>
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group button-group text-right">
      <a href=""
        class="btn btn-link btn-sm"
        ng-click="deleteVideo(video)"
        >
        <?php echo $tr->__('Delete'); ?>
      </a>
      <a href=""
        class="btn btn-primary btn-sm"
        ng-click="updateVideo(video)"
        >
        <?php echo $tr->__('Save Changes'); ?>
      </a>
    </div>

  </div>

</div>