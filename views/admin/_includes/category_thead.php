<h2 class="h5 with-em">
  <?php echo $tr->__('All Items in'); ?>
  <span ng-cloak>
    &ldquo;{{ categoryTitle }}&rdquo;
  </span>
  <em ng-cloak>
    {{ sectionVideoCount }}
  </em>
</h2>

<ul class="list-unstyled admin-category-list title-bar">
  <li class="title-bar">
    <div class="video-info">
      <div class="row">
        <div class="col-xs-2 p-r-0">
          <span class="video-count">
            &nbsp;
          </span>
        </div>
        <div class="col-xs-5">
          <?php echo $tr->__('Title'); ?>
        </div>
        <div class="col-xs-1 text-xs-center">
          <?php echo $tr->__('Featured'); ?>
        </div>
        <div class="col-xs-2">
          <?php echo $tr->__('Published'); ?>
        </div>
        <div class="col-xs-2">
          <?php echo $tr->__('Private'); ?>
        </div>
      </div>
    </div>
  </li>
</ul>