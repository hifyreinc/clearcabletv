<li class="editing"
  ng-if="addNew<?php echo $context; ?>"
  ng-cloak
  >
  <form name="addSectionForm">
    <div class="category-info">
      <div class="edit">

        <!-- action heading -->
        <p>
          <strong>
            <?php echo $tr->__('Add New ' . $context); ?>
          </strong>
        </p>
        <div class="response"
          ng-show="newSection.error"
          ng-cloak
          >
          <div class="alert alert-danger">
            <i class="fa fa-warning"></i>
            <?php echo $tr->__($context . ' name is required'); ?>.
          </div>
        </div>

        <!-- fields -->
        <div class="row">
          
          <?php if (in_array('en', $enabled_languages)): ?>
          <div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                  <label>
                    <?php echo $tr->__('English Name'); ?>
                  </label>
                </div>
                <div class="col-xs-8">
                  <input type="text"
                    class="form-control"
                    ng-model="newSection.name"
                    />
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>

          <?php if (in_array('fr', $enabled_languages)): ?>
          <div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                  <label>
                    <?php echo $tr->__('French Name'); ?>
                  </label>
                </div>
                <div class="col-xs-8">
                  <input type="text"
                    class="form-control"
                    ng-model="newSection.french_name"
                    />
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>

          <?php if (in_array('es', $enabled_languages)): ?>
          <div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                  <label>
                    <?php echo $tr->__('Spanish Name'); ?>
                  </label>
                </div>
                <div class="col-xs-8">
                  <input type="text"
                    class="form-control"
                    ng-model="newSection.spanish_name"
                  />
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>

        </div>

        <!-- action -->
        <div class="form-group button-group text-right">
          <a href=""
            class="btn btn-secondary btn-sm pull-left"
              ng-click="$parent.addNew<?php echo $context; ?> = false;"
            >
            <?php echo $tr->__('Cancel'); ?>
          </a>
          &nbsp;
          <a href=""
            class="btn btn-primary btn-sm"
            ng-click="addSection()"
            >
            <?php echo $tr->__('Add ' . $context); ?>
          </a>
        </div>
        
      </div>
    </div>
  </form>
</li>