<li ng-class="{editing: section.editing}"
  ng-repeat="section in sections"
  data-section="{{section.id}}"
  ng-cloak
  >

  <!-- sorting handle -->
  <div class="sort-handle">
    <i class="fa fa-sort"></i>
  </div>

  <div class="category-info">
    <div class="row display">

      <!-- name -->
      <div class="col-xs-6">
        <strong class="category-title"
          ng-if="lang == 'en'"
          >
          {{ lang == 'en' ? section.name : '' }}
          {{ lang == 'fr' ? section.french_name : '' }}
          {{ lang == 'es' ? section.spanish_name : '' }}
        </strong>

        <!-- actions -->
        <div class="actions">
          <a href=""
            class="action-link action-edit"
            ng-click="initEditSection(section)"
            >
            <?php echo $tr->__('Edit'); ?>
          </a>
          <span class="action-link-divider">
            |
          </span>
          <a href=""
            class="action-link action-delete"
            ng-click="deleteSection(section)"
            >
            <?php echo $tr->__('Delete'); ?>
          </a>
          <span class="action-link-divider">
            |
          </span>
          <a href="/admin/<?php echo $type; ?>/category/?categoryId={{section.id}}">
            <?php echo $tr->__('View Items'); ?>
          </a>
          <span class="action-link-divider">
            |
          </span>
          <a href="/admin/<?php echo $type; ?>/add-new/?categoryId={{section.id}}">
            <?php echo $tr->__('Add Item'); ?>
          </a>
        </div>

      </div>

      <!-- video count -->
      <div class="col-xs-6 text-xs-center">
        <span class="category-count">
          {{ section.videoCount.count }}
        </span>
      </div>

    </div>

    <!-- edit row -->
    <div class="edit">

      <!-- heading -->
      <p>
        <strong>
          <?php echo $tr->__('Edit ' . $context); ?>
        </strong>
      </p>

      <!-- fields -->
      <div class="row">

        <?php if (in_array('en', $enabled_languages)) : ?>
        <div class="col-xs-12 col-md-<?php echo $cols; ?>">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label><?php echo $tr->__('English Name'); ?></label>
              </div>
              <div class="col-xs-9">
                <input type="text"
                  class="form-control"
                  ng-model="editSection.name"
                  />
              </div>
            </div>
          </div>
        </div>
        <?php endif; ?>

        <?php if (in_array('fr', $enabled_languages)): ?>
        <div class="col-xs-12 col-md-<?php echo $cols; ?>">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label>
                  <?php echo $tr->__('French Name'); ?>
                </label>
              </div>
              <div class="col-xs-9">
                <input type="text"
                  class="form-control"
                  ng-model="editSection.french_name"
                />
              </div>
            </div>
          </div>
        </div>
        <?php endif; ?>

        <?php if (in_array('es', $enabled_languages)): ?>
        <div class="col-xs-12 col-md-<?php echo $cols; ?>">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label>
                  <?php echo $tr->__('Spanish Name'); ?>
                </label>
              </div>
              <div class="col-xs-9">
                <input type="text"
                  class="form-control"
                  ng-model="editSection.spanish_name"
                  />
              </div>
            </div>
          </div>
        </div>
        <?php endif; ?>

      </div>


        <div class="form-group button-group text-right">
          <a href=""
            class="btn btn-secondary btn-sm pull-left"
            ng-click="cancelEditSection(section)"
            >
            <?php echo $tr->__('Cancel'); ?>
          </a>
          &nbsp;
          <a href=""
            class="btn btn-primary btn-sm"
            ng-click="saveSection(section)"
            >
            <?php echo $tr->__('Update Category'); ?>
          </a>
        </div>

    </div>
  </div>
</li>