<ul class="list-unstyled admin-category-list title-bar">
  <li class="title-bar">
    <div class="category-info">
      <div class="row">

        <!-- channel name -->
        <div class="col-xs-6">
          <span class="category-title">
            <?php echo $tr->__('Name'); ?>
          </span>
        </div>

        <!-- video count -->
        <div class="col-xs-6 text-xs-center">
          <span class="category-count">
            <?php echo $tr->__('Video Count'); ?>
          </span>
        </div>
        
      </div>
    </div>
  </li>
</ul>