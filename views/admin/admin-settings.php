<div ng-controller="AdminController">

  <?php

  require_once(realpath(__DIR__ . '/../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../_includes/_submenu-admin.php'));

  ?>

  <div class="main-content-area">
    <div class="container-fluid">

      <!-- page heading -->
      <h1 class="h4">
        <?php echo $tr->__('Edit Website Settings'); ?> &nbsp;
      </h1>

      <br />

      <!-- basic settings -->
      <div class="row">

        <!-- website label -->
        <div class="col-md-4">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Website Label'); ?>
            </label>
            <input type="text"
              class="form-control"
              id="websiteLabel"
              rel="<?php echo $settings[0]['ID']; ?>"
              value="<?php echo $settings[0]['value']; ?>"
              />
          </div>
        </div>

        <!-- website URL -->
        <div class="col-md-4">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Website URL'); ?>
            </label>
            <input type="text"
              class="form-control"
              id="websiteURL"
              rel="<?php echo $settings[1]['ID']; ?>"
              value="<?php echo $settings[1]['value']; ?>"
              />
          </div>
        </div>

        <!-- website URL -->
        <div class="col-md-4">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Homepage Message'); ?>
            </label>
            <input type="text"
              class="form-control"
              id="homepageMsg"
              rel="<?php echo $settings[5]['ID']; ?>"
              value="<?php echo $settings[5]['value']; ?>"
              />
          </div>
        </div>

        <!-- languages -->
        <div class="col-md-4">
          <div class="form-group">
            <label class="block">
              <?php echo $tr->__('Enabled Languages'); ?>
            </label>
            <?php $languages = json_decode($settings[8]['value']); ?>
            <select class="form-control c-select"
              id="languages"
              name="languages"
              rel="<?php echo $settings[8]['ID']; ?>"
              multiple
              >
              <option value="en"
                <?php echo in_array('en', $languages) ? 'selected' : ''; ?>
                >
                English
              </option>
              <option value="fr"
                <?php echo in_array('fr', $languages) ? 'selected' : ''; ?>
                >
                French
              </option>
              <option value="es"
                <?php echo in_array('es', $languages) ? 'selected' : ''; ?>
                >
                Spanish
              </option>
            </select>
          </div>
        </div>

      </div>

      <hr />

      <!-- payment settings -->
      <div class="row">

        <!-- website label -->
        <div class="col-md-6">
          <div class="form-group">
            <label class="block">
              <?php echo $tr->__('Payment Gateway'); ?>
            </label>
            <label class="c-input c-radio">
              <input type="radio"
                name="payment"
                class="gateway-radio"
                id="saml"
                rel="<?php echo $settings[2]['ID']; ?>"
                value="1"
                <?php
                  echo $settings[2]['value'] == 1
                    ? 'checked' : '';
                ?>
                data-key="stripeAPIWrapper"
                />
              <span class="c-indicator"></span>
              <?php echo $tr->__('SAML'); ?>
            </label>
            <label class="c-input c-radio">
              <input type="radio"
                name="payment"
                class="gateway-radio"
                id="stripe"
                rel="<?php echo $settings[2]['ID']; ?>"
                value="2"
                <?php
                  echo $settings[2]['value'] == 2
                    ? 'checked' : '';
                ?>
                data-key="stripeAPIWrapper"
                />
              <span class="c-indicator"></span>
              <?php echo $tr->__('Stripe'); ?>
            </label>
            <label class="c-input c-radio">
              <input type="radio"
                name="payment"
                class="gateway-radio"
                id="no_auth"
                rel="<?php echo $settings[2]['ID']; ?>"
                value="0"
                <?php
                  echo $settings[2]['value'] == 0
                    ? 'checked' : '';
                ?>
                data-key="stripeAPIWrapper"
                />
              <span class="c-indicator"></span>
              <?php echo $tr->__('No Authentication'); ?>
            </label>
          </div>
        </div>

        <!-- website URL -->
        <div class="col-md-6">
          <div id="stripeAPIWrapper"
            class="<?php
                  echo $settings[2]['value'] != 2
                    ? 'hidden' : '';
                ?>"
            >
            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Public API Key'); ?>
                  </label>
                  <input type="text"
                    class="form-control"
                    id="stripeAPI"
                    rel="<?php echo $settings[3]['ID']; ?>"
                    value="<?php echo $settings[3]['value']; ?>"
                    />
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label>
                    <?php echo $tr->__('Private API Key'); ?>
                  </label>
                  <input type="text"
                    class="form-control"
                    id="privateAPI"
                    rel="<?php echo $settings[4]['ID']; ?>"
                    value="<?php echo $settings[4]['value']; ?>"
                    />
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <hr />

      <!-- payment settings -->
      <div class="row">
        <div class="col-xs-6">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Twitter Share Message'); ?>
            </label>
            <textarea class="form-control"
              id="twitterMsg"
              rel="<?php echo $settings[7]['ID']; ?>"><?php echo $settings[7]['value']; ?></textarea>
          </div>
        </div>
      <!--  <div class="col-xs-6">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Facebook Share Message'); ?>
            </label>
            <textarea class="form-control"
              id="facebookMsg"
              rel="<?php echo $settings[6]['ID']; ?>"><?php echo $settings[6]['value']; ?></textarea>
          </div>
        </div> -->
      </div>

      <hr />

      <div class="form-group button-group text-right">
        <button type="button"
          id="adminSettings"
          class="btn btn-primary btn-sm"
          >
          <?php echo $tr->__('Save Settings'); ?>
        </button>
      </div>

    </div>
  </div>
  
</div>