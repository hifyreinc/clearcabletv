<div ng-controller="AddQuestionController as question">

  <?php

  require_once(realpath(__DIR__ . '/../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../_includes/_submenu-admin.php'));

  ?>

  <div class="main-content-area">
    <div class="container-fluid">
      <h1 class="h4">
        <?php echo $tr->__('Add New FAQ Item'); ?>
        &nbsp;
        <a href=""
          ng-click="question.cancel()"
          class="btn btn-secondary btn-sm"
          >
          <?php echo $tr->__('Cancel'); ?>
        </a>
      </h1>
    </div>

    <br/>

    <div class="container-fluid">

      <h2 class="h5">
        <?php echo $tr->__('Adding Item to'); ?>
        <span ng-cloak>
          &ldquo;{{question.english ? question.category.english_category : question.category.french_category}}&rdquo;
        </span>
      </h2>

      <div class="row">

        <?php if (in_array('en', $enabled_languages)): ?>
        <div class="col-xs-12 col-md-4">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Question (English)'); ?>
            </label>
            <input type="text"
              name="question_english"
              ng-model="question.question.english"
              placeholder="<?php echo $tr->__('Enter a question in English'); ?>"
              class="form-control"
              required
              />
          </div>
          <div class="form-group">
            <label>
              <?php echo $tr->__('Answer (English)'); ?>
            </label>
            <textarea
              name="answer_english"
              ng-model="question.answer.english"
              placeholder="<?php echo $tr->__('Enter an answer in English'); ?>"
              class="form-control"
              required
              >
            </textarea>
          </div>
        </div>
        <?php endif; ?>

        <?php if (in_array('fr', $enabled_languages)): ?>
        <div class="col-xs-12 col-md-4">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Question (French)'); ?>
            </label>
            <input type="text"
              name="question_french"
              ng-model="question.question.french"
              placeholder="<?php echo $tr->__('Enter a question in French'); ?>"
              class="form-control"
              required
              />
          </div>
          <div class="form-group">
            <label>
              <?php echo $tr->__('Answer (French)'); ?>
            </label>
            <textarea
              name="answer_french"
              ng-model="question.answer.french"
              placeholder="<?php echo $tr->__('Enter an answer in French'); ?>"
              class="form-control"
              required
              >
            </textarea>
          </div>
        </div>
        <?php endif; ?>

        <?php if (in_array('es', $enabled_languages)): ?>
        <div class="col-xs-12 col-md-4">
          <div class="form-group">
            <label>
              <?php echo $tr->__('Question (Spanish)'); ?>
            </label>
            <input type="text"
              name="question_spanish"
              ng-model="question.question.spanish"
              placeholder="<?php echo $tr->__('Enter a question in Spanish'); ?>"
              class="form-control"
              required
              />
          </div>
          <div class="form-group">
            <label>
              <?php echo $tr->__('Answer (Spanish)'); ?>
            </label>
            <textarea
              name="answer_spanish"
              ng-model="question.answer.spanish"
              placeholder="<?php echo $tr->__('Enter an answer in Spanish'); ?>"
              class="form-control"
              required
              >
            </textarea>
          </div>
        </div>
        <?php endif; ?>

      </div>

      <div class="form-group button-group text-right">

        <a href=""
          class="btn btn-link btn-sm"
          ng-click="question.cancel()"
          >
          <?php echo $tr->__('Cancel'); ?>
        </a>

        <button class="btn btn-primary btn-sm"
          ng-click="question.addQuestion()"
          ng-disabled="question.questionForm.$invalid"
          >
          <?php echo $tr->__('Save FAQ'); ?>
        </button>
      </div>
    </div>
  </div>
</div>