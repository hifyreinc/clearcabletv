<div ng-controller="AdminController">
  <?php

  require_once(realpath(__DIR__ . '/../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../_includes/_submenu-admin.php'));

  $cols = ( 12 / count($enabled_languages) );

  ?>
  <div class="main-content-area">
    <div class="container-fluid">

      <!-- Top title and New button -->
      <h1 class="h4">
        <?php echo $tr->__('FAQ Categories'); ?> &nbsp;
        <a class="btn btn-secondary btn-sm"
          href
          ng-init="$parent.addNewCategory = false;"
          ng-click="$parent.addNewCategory = true;"
          ng-if="!addNewCategory"
          >
          <?php echo $tr->__('Add New'); ?>
        </a>
      </h1>

      <br />

      <!-- table -->
      <div class="row">
        <div class="col-xs-12">

          <!-- table heading -->
          <h2 class="h5 with-em">
            <?php echo $tr->__('All Categories'); ?>
            <em ng-cloak>
              {{ categoryCount }}
            </em>
          </h2>

          <!-- thead -->
          <ul class="list-unstyled admin-category-list title-bar">
            <li class="title-bar">
              <div class="category-info">
                <div class="row">
                  <div class="col-xs-6">
                    <span class="category-title">
                      <?php echo $tr->__('Name'); ?>
                    </span>
                  </div>
                  <div class="col-xs-6 text-xs-center">
                    <span class="category-count">
                      <?php echo $tr->__('Question Count'); ?>
                    </span>
                  </div>
                </div>
              </div>
            </li>
          </ul>

          <!-- tbody -->
          <ul class="list-unstyled admin-category-list sortable sortable-sections ui-sortable">
            <li class="editing"
              ng-show="addNewCategory"
              ng-cloak
              >
              <form name="addCategoryForm">
                <div class="category-info">

                  <div class="edit">
                    
                    <!-- action heading -->
                    <p>
                      <strong>
                        <?php echo $tr->__('Add New Category'); ?>
                      </strong>
                    </p>
                    <div class="response"
                      ng-show="newCategory.error"
                      ng-cloak
                      >
                      <div class="alert alert-danger">
                        <i class="fa fa-warning"></i>
                        <?php echo $tr->__('Category name is required'); ?>.
                      </div>
                    </div>

                    <!-- fields -->
                    <div class="row">

                      <?php if (in_array('en', $enabled_languages)) : ?>
                      <div class="col-xs-12 col-md-<?php echo $cols; ?>">
                        <div class="form-group">
                          <div class="row">
                            <div class="col-xs-3">
                              <label>
                                <?php echo $tr->__('English Name'); ?>
                              </label>
                            </div>
                            <div class="col-xs-9">
                              <input type="text"
                                class="form-control"
                                ng-model="newCategory.english"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php endif; ?>

                      <?php if (in_array('fr', $enabled_languages)) : ?>
                      <div class="col-xs-12 col-md-<?php echo $cols; ?>">
                        <div class="form-group">
                          <div class="row">
                            <div class="col-xs-3">
                              <label>
                                <?php echo $tr->__('French Name'); ?>
                              </label>
                            </div>
                            <div class="col-xs-9">
                              <input type="text"
                                class="form-control"
                                ng-model="newCategory.french"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php endif; ?>

                      <?php if (in_array('es', $enabled_languages)) : ?>
                      <div class="col-xs-12 col-md-<?php echo $cols; ?>">
                        <div class="form-group">
                          <div class="row">
                            <div class="col-xs-3">
                              <label>
                                <?php echo $tr->__('Spanish Name'); ?>
                              </label>
                            </div>
                            <div class="col-xs-9">
                              <input type="text"
                                class="form-control"
                                ng-model="newCategory.spanish"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php endif; ?>

                    </div>

                    <!-- add actions -->
                    <div class="form-group button-group text-right">
                      <a href=""
                        class="btn btn-secondary btn-sm pull-left"
                        ng-click="addNewCategory = false;"
                        >
                        <?php echo $tr->__('Cancel'); ?>
                      </a>
                      &nbsp;
                      <a href=""
                        class="btn btn-primary btn-sm"
                        ng-click="addCategory()"
                        >
                        <?php echo $tr->__('Add Category'); ?>
                      </a>
                    </div>

                  </div>
                </div>
              </form>
            </li>

            <!-- loop -->
            <li ng-class="{editing: category.editing}"
              ng-repeat="category in categories track by category.id"
              data-section="{{category.id}}"
              ng-cloak
              >

              <!-- sort handle -->
              <div class="sort-handle">
                <i class="fa fa-sort"></i>
              </div>

              <div class="category-info">
                <div class="row display">
                  <div class="col-xs-6">
                    <strong class="category-title"
                    >
                    {{ lang == 'en' ? category.english_category : '' }}
                    {{ lang == 'fr' ? category.french_category : '' }}
                    {{ lang == 'es' ? category.spanish_category : '' }}
                  </strong>

                  <!-- actions -->
                  <div class="actions">
                    <a href=""
                      class="action-link action-edit"
                      ng-click="startCategoryEdit(category)"
                      >
                      <?php echo $tr->__('Edit'); ?>
                    </a>
                    <span class="action-link-divider">
                      |
                    </span>
                    <a href=""
                      class="action-link action-delete"
                      ng-click="deleteCategory(category)"
                      >
                      <?php echo $tr->__('Delete'); ?>
                    </a>
                    <span class="action-link-divider">
                      |
                    </span>
                    <a href="/admin/faq/category/?categoryId={{category.id}}">
                      <?php echo $tr->__('View Items'); ?>
                    </a>
                    <span class="action-link-divider">
                      |
                    </span>
                    <a href="/admin/faq/add-new/?categoryId={{category.id}}">
                      <?php echo $tr->__('Add Item'); ?>
                    </a>
                  </div>
                </div>

                <div class="col-xs-6 text-xs-center">
                  <span class="category-count">
                    {{ category.count }}
                  </span>
                </div>
            </div>

            <div class="edit">
              
              <!-- heading -->
              <p>
                <strong>
                  <?php echo $tr->__('Edit Category'); ?>
                </strong>
              </p>

              <!-- fields -->
              <div class="row">

                <?php if (in_array('en', $enabled_languages)) : ?>
                <div class="col-xs-12 col-md-<?php echo $cols; ?>">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-xs-4">
                        <label>
                          <?php echo $tr->__('English Name'); ?>
                        </label>
                      </div>
                      <div class="col-xs-8">
                        <input type="text"
                          class="form-control"
                          ng-model="editCategory.english_category"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <?php endif; ?>

                <?php if (in_array('fr', $enabled_languages)) : ?>
                <div class="col-xs-12 col-md-<?php echo $cols; ?>">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-xs-4">
                        <label>
                          <?php echo $tr->__('French Name'); ?>
                        </label>
                      </div>
                      <div class="col-xs-8">
                        <input type="text"
                          class="form-control"
                          ng-model="editCategory.french_category"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <?php endif; ?>

                <?php if (in_array('es', $enabled_languages)) : ?>
                <div class="col-xs-12 col-md-<?php echo $cols; ?>">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-xs-4">
                        <label>
                          <?php echo $tr->__('Spanish Name'); ?>
                        </label>
                      </div>
                      <div class="col-xs-8">
                        <input type="text"
                          class="form-control"
                          ng-model="editCategory.spanish_category"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <?php endif; ?>

              </div>

              <!-- actions -->
              <div class="form-group button-group text-right">
                <a href=""
                  class="btn btn-secondary btn-sm pull-left"
                  ng-click="cancelEditCategory(category)"
                  >
                  <?php echo $tr->__('Cancel'); ?>
                </a>
                &nbsp;
                <a href=""
                  class="btn btn-primary btn-sm"
                  ng-click="updateCategory(category)"
                  >
                  <?php echo $tr->__('Update Category'); ?>
                </a>
              </div>

            </div>

          </div>
          
        </li>
      </ul>

      </div>
    </div>

  </div>

</div>

</div>