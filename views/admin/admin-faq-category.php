<div ng-controller="ViewFaqController as faq">
  <?php

  require_once(realpath(__DIR__ . '/../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../_includes/_submenu-admin.php'));

  ?>
  <div class="main-content-area">
    <div class="container-fluid">

      <!-- heading -->
      <h1 class="h4">
        <?php echo $tr->__('FAQ Items'); ?> &nbsp;
        <a href="/admin/faq/add-new/?categoryId={{faq.category.id}}"
          class="btn btn-secondary btn-sm"
          >
          <?php echo $tr->__('Add New'); ?>
        </a>
      </h1>
    
      <br/>

      <!-- table -->
      <div class="row">
        <div class="col-xs-12">

          <!-- table heading -->
          <h2 class="h5 with-em">
            <?php echo $tr->__('All Items in'); ?>
            <span ng-cloak>
              &ldquo;
              {{faq.english ? faq.category.english_category : faq.category.french_category}}
              &rdquo;
            </span>
            <em ng-bind="faq.category.count"></em>
          </h2>

          <!-- thead -->
          <ul class="list-unstyled admin-category-list title-bar">
            <li class="title-bar">
              <div class="video-info">
                <div class="row">
                  <div class="col-xs-12">
                    <span class="video-title">
                      <?php echo $tr->__('Title'); ?>
                    </span>
                  </div>
                </div>
              </div>
            </li>
          </ul>

          <!-- tbody -->
          <ul class="list-unstyled admin-category-list sortable sortable-questions sortable-sections ui-sortable">

            <!-- no questions -->
            <li ng-cloak
              ng-if="!faq.questions.length"
              >
              <?php echo $tr->__('No Questions Found'); ?>
            </li>

            <!-- loop -->
            <li
              ng-if="faq.questions &amp;&amp; faq.questions.length"
              ng-repeat="question in faq.questions track by question.id"
              data-question="{{question.id}}"
              ng-cloak
              >

              <!-- sort handle -->
              <div class="sort-handle">
                <i class="fa fa-sort"></i>
              </div>

              <!-- question -->
              <div class="category-info faq">

                <strong ng-if="faq.english"
                  ng-bind="question.english_question"
                  >
                </strong>
                <strong ng-if="faq.french"
                  ng-bind="question.french_question"
                  >
                </strong>
                <strong ng-if="faq.spanish"
                  ng-bind="question.spanish_question"
                  >
                </strong>

                <!-- actions -->
                <div class="actions">
                  <a href="/admin/faq/edit/?categoryId={{faq.category.id}}&amp;questionId={{question.id}}"
                    class="action-link"
                    >
                    <?php echo $tr->__('Edit'); ?>
                  </a>
                  <span class="action-link-divider">|</span>
                  <a href=""
                    class="action-link action-delete"
                    ng-click="faq.deleteQuestion(question)"
                    >
                    <?php echo $tr->__('Delete'); ?>
                  </a>
                </div>
              </div>

            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>