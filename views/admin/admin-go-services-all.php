<div ng-controller="AdminController"
  ng-init="section = 4"
  >
  <?php

  require_once(realpath(__DIR__ . '/../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../_includes/_submenu-admin.php'));

  ?>
  <div class="main-content-area">
    <div class="container-fluid">

      <!-- Top title and New button -->
      <h1 class="h4">
        <?php echo $tr->__('Go Services Items'); ?> &nbsp;
        <a href="/admin/go-services/add-new/?categoryId=70"
          class="btn btn-secondary btn-sm"
          >
          <?php echo $tr->__('Add New'); ?>
        </a>
      </h1>

      <br />

      <!-- table -->
      <div class="row">
        <div class="col-xs-12">

          <!-- table heading -->
          <?php require_once(realpath(__DIR__ . '/_includes/category_thead.php')); ?>

          <!-- table body -->
          <ul class="list-unstyled admin-category-list sortable sortable-videos ui-sortable">
            <?php

              // Set Variables
              $context = 'Service';
              $type = 'go-services';

              require_once(realpath(__DIR__ . '/_includes/no_videos.php'));
              require_once(realpath(__DIR__ . '/_includes/category_tbody.php'));

            ?>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>