<div ng-controller="AdminController"
  ng-init="section = 2"
  >
  <?php

  require_once(realpath(__DIR__ . '/../_includes/_banner-admin.php'));
  require_once(realpath(__DIR__ . '/../_includes/_submenu-admin.php'));

  ?>
  <div class="main-content-area">
    <div class="container-fluid">

      <!-- Top title and New button -->
      <h1 class="h4">
        <?php echo $tr->__('Video-On-Demand Categories'); ?> &nbsp;
        <a class="btn btn-secondary btn-sm"
          href
          ng-init="$parent.addNewCategory = false;"
          ng-click="$parent.addNewCategory = true;"
          ng-if="!addNewCategory"
          >
          <?php echo $tr->__('Add New'); ?>
        </a>
      </h1>

      <br />

      <!-- table -->
      <div class="row">
        <div class="col-xs-12">

          <!-- table heading -->
          <h2 class="h5 with-em">
            <?php echo $tr->__('All Categories'); ?>
            <em ng-bind="sectionCount"></em>
          </h2>

          <!-- thead -->
          <?php require_once(realpath(__DIR__ . '/_includes/channel_thead.php')); ?>
          
          <!-- tbody -->
          <ul class="list-unstyled admin-category-list sortable sortable-sections ui-sortable">
            <?php

              // Set Variables
              $context = 'Category';
              $type = 'video-on-demand';
              $cols = ( 12 / count($enabled_languages) );

              require_once(realpath(__DIR__ . '/_includes/channel_add.php'));
              require_once(realpath(__DIR__ . '/_includes/channel_tbody.php'));

            ?>
            
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>