<div ng-controller="ServicesController"
  ng-init="typesection = 4"
  >

  <?php require_once(realpath(__DIR__ . '/../_includes/_banner-empty.php')); ?>

  <div class="main-content-area">
    <div class="container-fluid static-list">

      <h1 class="h3">
        <?php echo $tr->__('Go Services'); ?>
      </h1>

      <br />

      <div class="row row-alt">

        <?php if (!$_SESSION['secure']): ?>
        <a class="video-block"
          ng-attr-href="{{ video.private == 0 ? video.url : '#signup-modal' }}"
          ng-attr-data-toggle="{{ video.private == 0 ? '' : 'modal' }}"
          data-govideo="{{ video.id }}"
          ng-repeat="video in videos"
          ng-cloak
          >
        <?php else: ?>
        <a class="video-block"
          ng-href="{{video.url}}"
          ng-repeat="video in videos"
          ng-cloak
          target="_blank"
          >
        <?php endif; ?>

          <div class="video-thumb">
            <div class="thumbnail-image"
              ng-if="video.thumbnail != ''"
              ng-style="{ 'backgroundImage': 'url(/uploads/images/{{video.thumbnail}}?m={{ video.filemtime }})' }"
              >
            </div>
            <div class="thumbnail-image"
              ng-if="video.thumbnail == ''"
              ng-style="{ 'backgroundImage': 'url(<?php echo $GLOBALS['noimage']; ?>)' }"
            >
            </div>
            <?php if (!$_SESSION['secure']): ?>
            <span class="private-lock"
              ng-if="video.private == 1"
              >
              <i class="fa fa-lock"></i>
            </span>
            <?php endif; ?>
          </div>

          <div class="video-details">
            <p class="video-title">
              <span ng-if="lang == 'en'">
                {{ video.name }}
              </span>
              <span ng-if="lang == 'fr'">
                {{ video.french_name }}
              </span>
              <span ng-if="lang == 'es'">
                {{ video.spanish_name }}
              </span>
            </p>
            <p class="video-description">
              <span ng-if="lang == 'en'">
                {{ video.description | truncate:75 }}
              </span>
              <span ng-if="lang == 'fr'">
                {{ video.french_description | truncate:75 }}
              </span>
              <span ng-if="lang == 'es' &amp;&amp; video.spanish_description">
                {{ video.spanish_description | truncate:75 }}
              </span>
            </p>
          </div>
        </a>

        <div class="video-block no-videos"
          ng-if="videos.length < 1"
          ng-cloak
          >
          <p>
            <?php echo $tr->__('No Videos Found'); ?>
          </p>
        </div>

      </div>

  </div>

</div>