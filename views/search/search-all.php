<div ng-controller="SearchController">

<div class="main-content-area">

	<div class="container-fluid">

		<?php if(isset($_GET['search']) && !empty($_GET['search'])){ ?>
		<h2 class="h5 with-btn"><?php echo $tr->__('Searching for'); ?> "<?php echo $_GET['search']; ?>" <a href="/search/"><?php echo $tr->__('Clear'); ?></a></h2>
		<?php } else { ?>
		<h2 class="h5 with-btn"><?php echo $tr->__('Enter something to search.'); ?> <a href="/search/"><?php echo $tr->__('Clear'); ?></a></h2>
		<?php } ?>

		<?php include($GLOBALS['dir'].'views/_includes/_search.php'); ?>

	</div>

	<hr/>

	<div class="container-fluid static-list">
		<div class="row row-alt">

			<?php if(!$_SESSION['secure']) { ?>
			<a class="video-block"
				ng-attr-href="{{
					video.private == 1 ? '#signup-modal' : (
						video.type === 'stream' ? '/live-streams/video/?videoId=' + video.id :
							'/video-on-demand/video/?videoId=' + video.id
					)
				}}"
				ng-attr-data-toggle="{{video.private == 0 ? '' : 'modal'}}"
				data-govideo="{{video.id}}"
				ng-repeat="video in videos"
				>
			<?php } else { ?>
			<a class="video-block"
				href="/{{video.type === 'stream' ? 'live-streams' : 'video-on-demand'}}/video/?videoId={{video.id}}"
				ng-repeat="video in videos"
				>
			<?php } ?>

				<div class="video-thumb">
					<div class="thumbnail-image"
						ng-if="video.poster != ''"
						ng-style="{'backgroundImage': 'url({{video.poster}}?m={{video.filemtime}})'}"
						>
					</div>
					<div class="thumbnail-image"
						ng-if="video.poster == ''"
						ng-style="{'backgroundImage': 'url(//placehold.it/480x270/000000&text=Image+Not+Found)'}"
					>
					</div>
					<?php if(!$_SESSION['secure']) { ?>
					<span class="private-lock"
						ng-if="video.private == 1"
						>
						<i class="fa fa-lock"></i>
					</span>
					<?php } ?>
				</div>

				<div class="video-details">
					<p class="video-title">
						<span ng-if="lang == 'en'">{{video.name}}</span>
						<span ng-if="lang == 'fr'">{{video.french_name}}</span>
						<span ng-if="lang == 'es'">{{video.spanish_name}}</span>
					</p>
					<p class="video-category">
						<span ng-if="lang == 'en'">{{video.sectionName}}</span>
						<span ng-if="lang == 'fr'">{{video.sectionFrenchName}}</span>
						<span ng-if="lang == 'es'">{{video.sectionSpanishName}}</span>
					</p>
					<p class="video-description">
						<span ng-if="lang == 'en'">{{video.description | truncate:75}}</span>
						<span ng-if="lang == 'fr'">{{video.french_description | truncate:75}}</span>
						<span ng-if="lang == 'es' && video.spanish_description">{{video.spanish_description | truncate:75}}</span>
					</p>
				</div>

			</a>

			<div class="video-block no-videos"
				ng-if="videos.length < 1"
				>
				<p><?php echo $tr->__('No Videos Found'); ?></p>
			</div>

		</div>
	</div>

</div>

</div>